<?php
/**
 * @param $x
 * @param $length
 */
function custom_echo($x, $length){
    if(strlen($x)<=$length) {
        echo $x;
    }
    else {
        $y=substr($x,0,$length) . '...';
        echo $y;
    }
}
?>
<div class="pull-left image">
    <img src="../images/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
         class="img-circle" alt="><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?>" />
</div>
<div class="pull-left info">
    <p><?php custom_echo( $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'],20) ?></p>
    <!-- Status -->
    <a href="#"><i class="fa fa-circle text-success"></i>
        <?php echo $_SESSION['datosLogin']['NombreRol']; ?></a>
</div>
