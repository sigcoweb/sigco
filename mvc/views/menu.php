<?php
include_once '../facades/FacadeEmpleado.php';
include_once'../facades/ClienteFacade.php';
$inac=new ClienteFacade();
$facade=new FacadeEmpleado();
?>
<!-- Sidebar Menu -->
<ul class="sidebar-menu">
    <li class="header">Menu</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="index.php"><i class="fa fa-desktop"></i> <span>Inicio</span></a></li>
    <?php
    $titulos=$facade->obtenerMenu($_SESSION['datosLogin']['IdRol']);
    $nombre_archivo = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    //verificamos si en la ruta nos han indicado el directorio en el que se encuentra
    if ( strpos($nombre_archivo, '/') !== FALSE )
        //de ser asi, lo eliminamos, y solamente nos quedamos con el nombre y su extension
        $chart=explode('/', $nombre_archivo);
        $nombre_archivo = array_pop($chart);
    foreach ($titulos as $menu ) {
        $i=0;
        $subtitulos=$facade->obtenerSubMenu($menu['IdCategoria'],$_SESSION['datosLogin']['IdRol']);
        foreach ($subtitulos as $submenu ) {
            if($nombre_archivo==$submenu['Url']){
                $i++;
            };
        }
        ?>
        <li class="treeview <?php if($i>=1){echo 'active';}; ?>">
            <a href="#"><i class="<?php echo $menu['Icono']?>"></i> <span><?php echo $menu['Nombre']?>
                            </span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <?php
                $subtitulos=$facade->obtenerSubMenu($menu['IdCategoria'],$_SESSION['datosLogin']['IdRol']);
                foreach ($subtitulos as $submenu ) { ?>
                    <li class="<?php if($nombre_archivo==$submenu['Url']){echo 'active';}; ?>">
                        <a href="<?php echo $submenu['Url']?>">
                            <i class="fa fa-circle-o"></i>
                            <?php echo $submenu['NombrePagina'];?>
                            <?php if($submenu['Url']=='activarClientes.php'){
                                $cant=$inac->contarClientesInactivos();
                                echo'<span class="label bg-blue-gradient pull-right">'.$cant['inactivos'].'</span>';
                            };
                            ?></a>

                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>

        <?php
    }
    ?>
</ul><!-- /.sidebar-menu -->

