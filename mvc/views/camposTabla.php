<?php
include_once '../facades/ImportarFacade.php';
$impor = new ImportarFacade();
$tab=$impor->listarCampos($_POST['nombreTabla']);
$html='
<table class="table table-hover table-striped table-bordered table-condensed">
        <thead>
        ';
foreach($tab as $camp) {
    $html .= '
            <th class="info">';
    $html .= $camp['Field'] . '<br>' . $camp['Type'];
    $html .= '
</th>';
};
$html.='
        </thead>
        <tbody>';
for($i=0;$i<3;$i++) {
    $html .= '
        <tr>
            ';
    foreach ($tab as $camp) {
        if (isset($camp['Extra']) && $camp['Extra'] == 'auto_increment') {
            $clase = 'warning';
        } else {
            $clase = '';
        };
        $html .= '
                <td class="' . $clase . '">
                ';
        if(isset($camp['Extra']) && $camp['Extra'] == 'auto_increment'){
            $html .='(Dejar en blanco, campo automático)';
        }else{
            $html .= $impor->ejemploDato($camp['Type']);
        }
        $html .= '
   </td>
   ';
    };
    $html .= '
        </tr>';
};
$html.='
        </tbody>
    </table>';
echo $html;
/*?>
    <table border="1">
        <thead>
        <?php foreach($tab as $camp){ ?>
            <th><?php echo $camp['Field'].'<br>'.$camp['Type']; ?></th>
        <?php } ?>
        </thead>
        <tbody>
        <tr>
            <?php foreach($tab as $camp){ ?>
                <td><?php echo $impor->ejemploDato($camp['Type']);?></td>
            <?php } ?>
        </tr>
        </tbody>
    </table>






<?php
/*
$html.='<option value="0" disabled selected>Seleccione un lugar...</option>';
foreach($facs as $lug){
    $html.='<option value="'.$lug['IdLugar'].'">'.$lug['NombreLugar'].'</option>';
}
echo $html;
*/