<!DOCTYPE html> <?php session_start(); ?>

<?php
if ($_SESSION['datosLogin']['EstadoPersona'] == "Inactivo" or !isset($_SESSION['datosLogin'])) {
    header('location: Invalido.php');

}
?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Proximas Gestiónes</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- fullCalendar 2.2.5-->
    <link href="../../plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="../../plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="../../favicon.ico">
    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <link href="../../plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">
                    <?php include_once 'header.php' ?>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- Sidebar iterator panel (optional) -->
                    <div class="user-panel">
                        <?php include_once 'userPanel.php'; ?>
                    </div>
                    <?php include_once 'menu.php' ?>
                </section>
                <!-- /.sidebar -->
            </aside>
        <!-- sidebar: style can be found in sidebar.less -->


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Calendario
            <small>Actividades</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active"> Gestiónes</li>
              <li class="active">Proximas gestiónes</li>
          </ol>


        </section>

        <!-- Main content -->
          <section class="content">
              <div class="row">
                  <div class="col-md-3">
                      <div class="box box-solid box-primary">
                          <div class="box-header with-border">
                              <h4 class="box-title">Arrastre una etiqueta para crear un evento</h4>
                          </div>
                          <div class="box-body">
                              <!-- the events -->
                              <div id="external-events">
                                  <div class="external-event bg-green">Asesoria Técnica</div>
                                  <div class="external-event bg-yellow">Capacitación</div>
                              </div>
                          </div><!-- /.box-body -->
                      </div><!-- /. box -->
                      <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {?>
                      <div class="box box-solid box-info">
                          <div class="box-header with-border">
                              <h5 class="box-title">Seleccione a un asesor para ver  sus actividades</h5>
                          </div>
                          <div class="box-body">
                              <!-- the events -->
                              <div id="external-events">
                                  <div class="form-group">
                                      <select class="form-control select2" name="" style="width: 100%" id="document" required autofocus>
                                          <option value="" disabled selected>Seleccione..</option>
                                          <?php include_once '../models/EmpleadoDao.php';
                                          $facadeEmleado=new FacadeEmpleado();
                                          foreach($facadeEmleado->listarEmpleadosAsesores() as $data){ ?>
                                          <option value="<?php print $data['CedulaEmpleado'] ?>"><?php print $data['CedulaEmpleado'].' | '.$data['Nombres'].' '.$data['Apellidos']; if($data['EstadoPersona']=='Inactivo'){echo ' | Inactivo';} ?></option>
                                          <?php } ?>
                                      </select>

                                  </div>
                                  <h4>Ver las actividades </h4>
                                  <button class="btn btn-info  btn-block me" value="<?php print $_SESSION['datosLogin']['id'];?>" data-toggle="tooltip" title="Ver mis Actividades"><i class="fa fa-male"></i></button>
                              </div>
                          </div><!-- /.box-body -->
                      </div><!-- /. box -->
                      <?php }?>
                  </div><!-- /.col -->
                  <div class="col-md-9">
                      <div class="box box-primary">
                          <div class="box-body no-padding">
                              <!-- THE CALENDAR -->
                              <div id="calendar"></div>
                          </div><!-- /.box-body -->
                      </div><!-- /. box -->
          </section><!-- /.content -->
      </div>
            <!-- /.content-wrapper -->


            <!-- Main Footer -->
            <?php include_once 'footer.php'; ?>

            <!-- Control Sidebar -->

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../plugins/messajes/jquery.noty.packaged.min.js"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="../../plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="../../plugins/fullcalendar/lang-all.js"></script>
   <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- Page specific script -->
    <script type="text/javascript">

        var firstdata;
        $(function () {

        $('.select2').select2();
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
    title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
            $.post("../controllers/ControladorGestion.php",

                {
                    events: 1
                },
                function (data) {
                    firstdata =JSON.parse(data);
                    var nowDate = new Date();
                    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);


        $('#calendar').fullCalendar({

          lang: 'es',
          header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          buttonText: {
            today: 'Hoy',
            month: 'Mes',
            week: 'Semana',
            day: 'Hoy'

          },
                dayRender: function(date, cell){
                    if (date.toLocaleString() < today){
                        console.log(date.toLocaleString());
                        $(cell).addClass('disabled');
                    }

            },
          //Random default events

          events: firstdata,
          editable: false,
          droppable: true,
          timeFormat: 'h:mm a',// this allows things to be dropped onto the calendar !!!
          drop: function (date, allDay) { // this function is called when something is dropped

    // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

    // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            var dateFormated= moment(date).format('YYYY-MM-DD h:mm A');
             if(dateFormated<moment().format('YYYY-MM-DD')){
                 var n = noty({
                     text: '<h4>La fecha debe ser posterior</h4>',
                     theme: 'relax',
                     layout: 'center',
                     closeWith: ['click', 'hover'],
                     buttons: [
                         {
                             addClass: 'btn btn-success', text: 'Ok', onClick: function ($noty) {
                             $noty.close();

                         }
                         }

                     ],
                     type: 'confirm',
                     animation: {
                         open: 'animated wobble', // Animate.css class names
                         close: 'animated flipOutX', // Animate.css class names
                     }

                 });
             }else{
                 window.location='RegistrarGestion.php?date='+dateFormated+"&type="+copiedEventObject.title;
                 copiedEventObject.backgroundColor = $(this).css("background-color");
                 copiedEventObject.borderColor = $(this).css("border-color");
                 $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

             }


            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)


            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

          }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
          event.html(val);
          $('#external-events').prepend(event);

          //Add draggable funtionality
          ini_events(event);

          //Remove event from text input
          $("#new-event").val("");

        });
      });
  });
    $('#document').on('change',function(){

        $.post("../controllers/ControladorGestion.php",

            {
                searchBy: $(this).val()
            },
            function (data) {
                var json =JSON.parse(data);
                $('#calendar').fullCalendar( 'removeEvents');
                $('#calendar').fullCalendar( 'addEventSource', json);
            });

    });

        $('.me').on('click',function(){
            $.post("../controllers/ControladorGestion.php",

                {
                    searchBy: $(this).val()
                },
                function (data) {
                    var json =JSON.parse(data);
                    $('#calendar').fullCalendar( 'removeEvents');
                    $('#calendar').fullCalendar( 'addEventSource', json);
                });
        });

    </script>
  </body>
</html>
