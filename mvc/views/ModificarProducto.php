<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
include_once '../facades/FacadeProducto.php';
session_start();
$productoTo= new FacadeProducto();
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar producto</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../plugins/select2/select2.css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>
    <script src="../../plugins/select2/select2.min.js"></script>


    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <link rel="stylesheet" href="../../css/cirular-buttons.css">
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

       <?php include_once 'header.php' ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <?php include_once 'userPanel.php'; ?>
            </div>
            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar-menu -->

        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Productos
                <small> Formulario de Creación</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Productos</a></li>
                <li class="active">Registrar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">
                    <form id="defaultForm" action="../controllers/ControladorProducto.php" method="post"
                          enctype="multipart/form-data">

                        <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones de registro</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar un nuevo
                                        producto.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <div class="box box-default" id="formulario1">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registrar Producto</h3>
                            </div>
                            <?php $productData=$productoTo->obtenerProducto($_GET['id'])?>                             <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="placeind">
                                            <label for="names">Código del producto*</label>
                                            <input class="form-control validation" required name="idProducto"
                                                   id="codigo" type="text" placeholder="MAT001" value="<?php print $productData['IdProducto']?>" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="names">Nombre de producto*</label>
                                            <input class="form-control" name="nombreProducto" id="names"
                                                   type="text" placeholder="Neutralizador de malos olores" required value="<?php print $productData['NombreProducto']?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="apellido">Descripción*</label>
                                    <textarea class="form-control" name="descriptionProducto" aria-required="true"
                                              id="descriptionProducto"  maxlength="200" placeholder=""
                                              rows="5"><?php print $productData['DescripcionProducto']?></textarea>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label for="categoriaProducto">Categoria producto*</label>
                                            <select class="form-control select2" style="width: 100%" required name="categoriaProducto"
                                                    >
                                                <option value="">Seleccionar</option>
                                                <?php
                                                include_once '../facades/FacadeProducto.php';
                                                $producto = new FacadeProducto();
                                                $Productos = $producto->obtenerCategoriaProducto();
                                                foreach ($Productos as $iterator) { ?>
                                                    <option
                                                        value="<?php echo $iterator['IdCategoria']; ?>" <?php if($productData['IdCategoriaProductos']==$iterator['IdCategoria']){print 'selected';} ?>><?php echo $iterator['NombreCategoria']; ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cargo">IVA*</label>
                                            <select class="form-control select2" style="width: 100%" required name="ivaProducto" >
                                                <option value="">Seleccionar</option>
                                                <?php

                                                $producto = new FacadeProducto();
                                                $Productos = $producto->obtenerImpuestosProducto();
                                                foreach ($Productos as $iterator) { ?>
                                                    <option
                                                        value="<?php echo $iterator['IdIva'];?>" <?php if($productData['IdIvaProductos']==$iterator['IdIva']){print ' selected';} ?>><?php echo $iterator['PorcentajeIva']; ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="imagen">Imágen</label> <br>
                                        <span class="btn bg-blue-gradient btn-file btn-block">
                                            Buscar imagen...<input name="ImagenProducto" id="imagen" type="file"
                                                                    class="file"
                                                                   accept="image/gif, image/jpeg, image/png  title="
                                                                  >
                                        </span>
                                        </div>

                                    </div>
                                    <div class="col-lg-6" hidden>
                                        <div class="form-group">
                                            <label for="imagen">Ficha tecnica</label> <br>
                                        <span class="">
                                            <input name="fichaTec"  type="file"
                                                                    class="file"
                                                                   accept="image/gif, image/jpeg, image/png  title="
                                                                  />
                                        </span>
                                        </div>

                                    </div>
                                    <div class="col-lg-6" hidden>
                                        <div class="form-group">
                                            <label for="imagen">Ficha de seguridad</label> <br>
                                        <span class="">
                                            <input name="FichaSeg"  type="file"
                                                                    class="file"
                                                                   accept="image/gif, image/jpeg, image/png  title="
                                                                  />
                                        </span>
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group" id="img" style="margin: 0 auto;" ><img id="blah"
                                                                                                     style="border-radius: 5px;height: auto;max-height: 300px;min-width: 150px;min-height: 100px;"
                                                                                                     src="<?php print $productData['rutaImagen'] ?>"
                                                                                                     alt="producto"/>
                                </div>

                                <div class="box-footer">

                                </div>

                            </div>

                        </div>


                        <!-- general form elements disabled -->


                        <div class="box box-default" id="formulario2">
                            <div class="box-header with-border">
                                <h4>Propiedades Especificas</h4>
                            </div>

                            <div class="box-body">
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="color">Color*</label>
                                                <select class="form-control select2" style="width: 100%" multiple="multiple"
                                                        name="color[]"
                                                        id="">
                                                    <?php
                                                    $producto = new FacadeProducto();
                                                    $Productos = $producto->obtenerColores();
                                                    foreach ($Productos as $iterator) {

                                                        ?>

                                                        <option
                                                            value="<?php print $iterator['idColor']; ?>"
                                                            <?php
                                                            foreach ($productoTo->obtenerColoresById($_GET['id']) as $color){
                                                            if($color['idColorColoresProd']==$iterator['idColor']){print 'selected';}
                                                            } ?> ><?php echo $iterator['color'];?> </option>
                                                        <?php

                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="concentracion">Concentración*</label>
                                                <select class="form-control select2" style="width: 100%" multiple="multiple"
                                                        name="concentracion[]" id="">
                                                    <?php
                                                    $producto = new FacadeProducto();
                                                    $Productos = $producto->obtenerConcetraciones();
                                                    foreach ($Productos as $iterator) { ?>
                                                        <option
                                                            value="<?php echo $iterator['idConcentracion']; ?>"
                                                            <?php foreach ($productoTo->obtenerConcetracionesById($_GET['id']) as $aroma )
                                                            {   if($aroma['idConcentracionConcProd']==$iterator['idConcentracion']) {
                                                                print 'selected';
                                                            }
                                                            }?>
                                                            ><?php echo $iterator['concentracion']; ?></option>
                                                        <?php
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="col-lg-12">

                                            <div class="form-group">
                                                <label for="aroma">Aroma*</label>
                                                <select class="form-control select2" style="width: 100%" multiple="multiple"
                                                        name="aroma[]"
                                                        id="">
                                                    <?php
                                                    $producto = new FacadeProducto();
                                                    $Productos = $producto->obtenerAromas();
                                                    foreach ($Productos as $iterator) { ?>
                                                        <option
                                                            <?php foreach ($productoTo->obtenerAromasById($_GET['id']) as $aroma )
                                                            {   if($aroma['idAromaAromasProd']==$iterator['idAroma']) {
                                                                print 'selected';
                                                            }
                                                            }?>
                                                            value="<?php echo $iterator['idAroma']; ?>"><?php echo $iterator['aroma']; ?></option>
                                                        <?php
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php

                                        $temp=$productoTo->obtenerPresentacionesPrecios($_GET['id']);
                                        foreach ($temp as  $res){?>
                                        <div class="col-lg-12">
                                        <div class="form-group" id="optionTemplate">
                                            <div class="col-lg-12" style="margin-top: 2%">

                                                <div class="form-group" id="placeind">
                                                    <label class="col-xs-2">Presentación</label>
                                                    <div class="col-xs-4">
                                                        <select  class="form-control" style="width: 100%" required name="clonpresentacion">
                                                            <option value="">Seleccionar</option>
                                                            <?php

                                                            $producto = new FacadeProducto();
                                                            $Productos = $producto->obtenerPresentacionProducto();

                                                            foreach ($Productos as $iterator) { ?>
                                                                <option
                                                                    value="<?php echo $iterator['IdPresentacion']; ?>" <?php if($res['idpresentacion']==$iterator['IdPresentacion']) {print 'selected';} ?>><?php echo $iterator['NombrePresentacion']; ?></option>
                                                                <?php
                                                            }?>

                                                        </select>
                                                    </div>

                                                    <div class="col-xs-4">

                                                        <input  type="text" class="form-control" placeholder="$ precio" value="<?php print $res['valorPresentacion'] ?>" name="clonprecio" />
                                                    </div>

                                                    <div class="col-xs-2">
                                                        <button type="button" class="btn bg-light-blue-gradient removeButton pull-right"><i class="fa fa-minus"></i></button>                                            </div>
                                                </div>
                                            </div>

                                    </div>

                                        <?php }?>
                                        <div class="box-footer">
                                        <button type="submit" class="btn btn-success pull-right" style="margin-top: 5%" tabindex="14"
                                                value="guardar" name="guardar" id="guardar">Guardar producto
                                        </button>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>

                <!-- /.box -->
            </div>


            <!--/.col (right) -->

            <!-- /.row -->
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function () {
        var titleValidators = {
                row: '.col-xs-4',   // The title is placed inside a <div class="col-xs-4"> element
                validators: {
                    notEmpty: {
                        message: 'selecione una presentacion'
                    }
                }
            },
            priceValidators = {
                row: '.col-xs-2',
                validators: {
                    notEmpty: {
                        message: 'el precio es requerido'
                    },
                    numeric: {
                        message: 'el precio debe ser un numero'
                    }
                }
            },
            bookIndex = 0;
        $('#defaultForm').formValidation({
            message: 'ingrese ingrese dato valido',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },


            fields: {
                nombreProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                descriptionProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                ivaProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                valorProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },

                presentacionProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                categoriaProducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                'presentacion[]':{
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                'precio[]':priceValidators,

                'aroma[]': {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                'color[]': {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                'concentracion[]': {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },

                idProducto: {
                    validator: {
                        notEmpty: {
                            message: 'Verifique el numero ingresado'
                        }
                    }
                }

            }

        }).on('success.validator.fv', function (e, data) {
            // data.field     --> The field name
            // data.element   --> The field element
            // data.result    --> The result returned by the validator
            // data.validator --> The validator name

            if (data.field === 'idProducto'
                && data.validator === 'notEmpty' && validate()) {
                // The userName field passes the remote validator
                data.element                    // Get the field element
                    .closest('.form-group')     // Get the field parent
                    // Add has-warning class
                    .removeClass('has-success')
                    .addClass('has-error')

                    // Show message
                    .find('small[data-fv-validator="notEmpty"][data-fv-for="idProducto"]')
                    .show();
                data.fv.disableSubmitButtons(true);
                data.fv.updateMessage('idProducto', 'notEmpty', 'Este código ya se encuetra Registrado');

            }
        })

            .on('click', '.addButton', function() {
                bookIndex++;
                var $template = $('#optionTemplate'),
                    $clone    = $template
                        .clone()
                        .removeClass('hide')
                        .removeAttr('id')
                        .attr('data-book-index', bookIndex)
                        .insertBefore($template);


                // Update the name attributes
                $clone
                    .find('[name="clonpresentacion"]').attr('name', 'presentacion[]').end()
                    .find('[name="clonprecio"]').attr('name', 'precio[]').end();


                // Add new fields
                // Note that we also pass the validator rules for new field as the third parameter
                $('#defaultForm')
                    .formValidation('addField', 'option[' + bookIndex + '].presentacion', titleValidators)
                    .formValidation('addField', 'option[' + bookIndex + '].precio', priceValidators);
               // addoptions();
            })

                // Remove button click handler
                .on('click', '.removeButton', function() {
                    var $row  = $(this).parents('.form-group'),
                        index = $row.attr('data-book-index');

                    // Remove fields
                    $('#defaultForm')
                        .formValidation('addField', 'option[' + bookIndex + '].presentacion', titleValidators)
                        .formValidation('addField', 'option[' + bookIndex + '].precio', priceValidators);

                    // Remove element containing the fields
                    $row.remove();
                });
    });

        $('.select2').select2();

        $('#imagen').on('change', function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)


                };
                $('#img').show();
                reader.readAsDataURL(this.files[0]);
            }

        });

        function validate() {
            var succeed = false;

            $.ajax({
                url: '../controllers/ControladorProducto.php',
                type: "POST",
                async: false,
                data: {data: $('#codigo').val()},
                success: function (r) {
                    if (r == '1') {
                        succeed = true;
                    }
                }

            });
            return succeed;


        }




</script>
</html>
