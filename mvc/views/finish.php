<?php
include_once '../utilities/installDto.php';
include_once '../utilities/config.php';
$dtoinstall=new installDto();
if($installed=='1'&&$dtoinstall->existsConection($host,$baseName,$userName,$userPass)==1){ ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Finalización</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="../../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="register-page">
    <div class="register-box">
      <div class="register-logo">
        <b>Iniciar</b>SIGCO</a>
      </div>

      <div class="register-box-body">
        <h3 class="login-box-msg">Felicitaciones</h3>
        <p class="login-box-msg">Ha instalado y configurado correctamente <b>SIGCO</b>. Recuerde los datos utilizados en el registro:</p>
          <div class="form-group has-feedback">
            <label for="">Usuario aministrador:</label>
            <input type="text" class="form-control" disabled value="<?php echo $_GET['user']?>" />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <label for="">Contraseña:</label>
            <input type="text" disabled class="form-control" value="<?php echo '******'.$_GET['pass'];?>" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <label for="">Correo:</label>
            <input type="text" disabled class="form-control" value="<?php echo $_GET['email']?>" />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="row">
            <p class="login-box-msg">Ahora puede iniciar sesión con los datos registrados por medio del  siguiente link:</p>
            <div class="col-xs-12 text-center">
              <a href="../../login.php"><i class="fa fa-check-square-o"></i>  Iniciar sesión en <b>SIGCO</b></a>
            </div><!-- /.col -->
          </div>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
  <?php
}else{
  header("location: install.php?error=true&mensaje=No se ha realizado la conexión, verifique los requisitos.");
}
?>