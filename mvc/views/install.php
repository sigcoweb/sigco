<?php
include_once '../utilities/noCache.php';
include_once '../utilities/installDto.php';
include_once '../utilities/config.php';
session_start();
$installDto=new installDto();
$lec='../utilities/configRead';
$esc='../utilities/config.php';
$path='../utilities/';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Instalación</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .modal-body{
            height: 250px;
            overflow-y: auto;
        }

        @media (min-height: 500px) {
            .modal-body { height: 400px; }
        }

        @media (min-height: 800px) {
            .modal-body { height: 600px; }
        }
    </style>
</head>
<body class="register-page">
<div class="register-box">
    <?php if(isset($_GET['error'])||isset($_GET['mensaje'])){ ?>
        <div class="box box-danger box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Error:</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php echo $_GET['mensaje']; ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    <?php };
    if(isset($_SESSION['forzar']['NombreRol'])&&$_SESSION['forzar']['NombreRol']=='Administrador'){
        $pass=1;
    }else{
        $pass=0;
    }
    if($installed=='1'&&($installDto->countTables($host,$baseName,$userName,$userPass))>35&&!$pass&&!isset($_GET['lock'])){ ?>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <i class="fa fa-exclamation-triangle"></i><h3 class="box-title">SIGCO ya está instalado</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <p>
                    Hemos verificado que ya cuenta con una instalación de <strong>SIGCO</strong> en el servidor.
                </p>
                <p>
                    Si procede con una nueva instalación deberá seleccionar otra base de datos y éste sistema
                    se enlazará con la nueva base olvidando la información anterior.
                </p>
                <p>
                    La información de
                    la base de datos anterior no se perderá, pero deberá administrarla con otra herramienta desde su servidor.
                </p>
                <p>
                    Se solicitarán credenciales de acceso como administrador de la <b>versión de SIGCO</b> que está instalada para forzar una nueva instalación.
                </p>
                <p>¿Desea continuar?</p>
                <p align="center">
                    <a href="../../index.php" tabindex="1"><i class="fa fa-undo"></i>   No continuar, ir al inicio del sistema.</a>
                </p>
                <form id="fock0" action="../controllers/installerController.php" method="post">
                    <p align="center">
                        <input type="checkbox" class="input" name="forzar" id="forzar" onclick="showContent()" required tabindex="2">
                        <label for="forzar">Entiendo los riesgos, quiero continuar
                        </label>
                    </p>
                    <div id="fuerzame" style="display: none;">
                        <p>Para continuar se deben validar los datos de acceso a SIGCO de un usuario administrador:</p>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" name="userName" placeholder="Usuario administrador SIGCO*" required data-toggle="popup" title="Usuario administrador" tabindex="3"/>
                            <span id="2" class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="userPass" placeholder="Contraseña del usuario" required data-toggle="popup" title="Usuario administrador" tabindex="4"/>
                            <span id="3" class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn bg-yellow-gradient btn-block btn-flat" tabindex="4">Verificar y forzar la nueva instalación</button>
                            </div><!-- /.col -->
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    <?php }else{
    if(!isset($_GET['lock'])){ ?>
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Requisitos</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                Antes de proceder, asegúrese de tener una <b>base de datos</b> en MySQL en blanco, un <b>usuario</b>
                con privilegios de escritura y su <b>contraseña</b> para realizar la conexión creados previamente en el hosting.
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    <?php }; if(isset($_GET['error'])){ ?>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Verificaciones</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-responsive hover">
                    <thead>
                    <th>Requisito</th>
                    <th>Elemento actual</th>
                    <th>Resultado</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Lectura de archivo</td>
                        <td><span class="label label-default"><?php echo $lec; ?></span></td>
                        <td><?php if(is_readable($lec)){echo '<span class="label label-success badge">Ok</span>';}else{echo '<span class="label label-warning">Revisar</span>';}; ?></td>
                    </tr>
                    <tr>
                        <td>Escritura de archivo</td>
                        <td><span class="label label-default"><?php echo $esc; ?></span></td>
                        <td><?php if(is_writable($esc)){echo '<span class="label label-success badge">Ok</span>';}else{echo '<span class="label label-warning">Revisar</span>';}; ?></td>
                    </tr>
                    <tr>
                        <td>Escritura de carpeta</td>
                        <td><span class="label label-default"><?php echo $path; ?></span></td>
                        <td><?php if(is_writable($path)){echo '<span class="label label-success badge">Ok</span>';}else{echo '<span class="label label-warning">Revisar</span>';}; ?></td>
                    </tr>
                    <tr>
                        <td>Base de datos y usuario con privilegios</td>
                        <td>BD: <span class="label label-default"><?php echo $baseName; ?></span><br>
                            User: <span class="label label-default"><?php echo $userName; ?></span><br>
                            Pass: <span class="label label-default"><?php if($userPass!=""){echo '*****'.(substr($userPass,(strlen($userPass)-3),3));}else{echo 'Sin password';}; ?></span>
                        </td>
                        <td><?php if($installDto->existsConection($host, $baseName, $userName, $userPass)==1&&$installed==1){echo '<span class="label label-success badge">Ok</span>';}else{echo '<span class="label label-warning">Revisar</span>';}; ?></td>
                    </tr>
                    <tr>
                        <td>Versión PHP (>5.2, <=5.6.15) </td>
                        <td><span class="label label-default"><?php echo phpversion(); ?></span></td>
                        <td><?php if(phpversion()>5.2&&phpversion()<=5.6){echo '<span class="label label-success badge">Ok</span>';}else{echo '<span class="label label-warning">Revisar</span>';}; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    <?php } ?>
    <?php if(isset($_SESSION['forzar']['NombreRol'])&&$_SESSION['forzar']['NombreRol']=='Administrador'){ ?>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Advertencia:</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <p>Instalando sobre una instalación existente.</p>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    <?php }; ?>
    <div class="register-logo">
        <b>Instalación</b>SIGCO
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">Paso 1 de 2:</p>
        <div id="oculta">
            <p class="login-box-msg" id="parrafo">Si ya cuenta con los requisitos anteriores (base de datos, usuario y contraseña), diligencie el formulario:</p>
        </div>
        <form id="fock1" action="../controllers/installerController.php?<?php if(isset($_GET['lock'])){echo 'installer=true';}else{echo 'makeFile=true';} ?>" method="post">
            <div class="form-group has-feedback">
                <input type="text" class="form-control host" name="host" value="localhost" placeholder="Nombre del host" data-toggle="tooltip" title="Nombre del host" required/>
                <span id="0" class="glyphicon glyphicon-home form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control 1" name="baseName" placeholder="Base de datos*" <?php if(!isset($_GET['lock'])){echo 'autofocus';} ?> tabindex="2" required data-toggle="tooltip" title="Nombre de la base de datos MySQL"/>
                <span id="1" class="glyphicon glyphicon-hdd form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control 2" name="userName" placeholder="Usuario de la base de datos*" tabindex="3" required data-toggle="tooltip" title="Nombre del usuario con privilegios en la base"/>
                <span id="2" class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control 3" name="userPass" placeholder="Contraseña del usuario" tabindex="4" data-toggle="tooltip" title="Contraseña del usuario para la base de datos (opcional)"/>
                <span id="3" class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <p>
                    <button type="button" class="btn bg-yellow-gradient btn-sm" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-file-text-o"></i>  Ver licencia de uso
                    </button>
                </p>
                <!-- Modal -->
                <div class="modal fade modal-xlg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Licencia de uso SIGCO</h4>
                            </div>
                            <div class="modal-body">
                                <?php
                                $licencia=fopen('../utilities/licence','r');
                                while(!feof($licencia)){
                                    echo fgets($licencia);
                                }
                                fclose($licencia);
                                ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn bg-light-blue-gradient" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="checkbox" name="condiciones" id="condiciones" required tabindex="5">
                <label for="condiciones">Acepto los términos de la licencia.
                </label>

            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" id="sigue" disabled class="btn bg-blue-gradient btn-block btn-flat" tabindex="6"><i class="fa fa-search"></i>  Comprobar la información</button>
                </div>
            </div>
        </form>
    </div><!-- /.form-box -->
</div><!-- /.register-box -->
<?php } ?>
<!-- jQuery 2.1.4 --
<script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function aceptoDeMuyMalaGana() {
        element = document.getElementById("fuerzame");
        check = document.getElementById("condiciones");
        if (check.checked) {
            $('#sigue').removeAttr('disabled').removeClass('disabled');
        }
        else {
            $('#sigue').attr('disabled','disabled').addClass('disabled');
        }
    }
</script>
<?php if(!isset($_GET['lock'])){ ?>
    <script type="text/javascript">
        function showContent() {
            element = document.getElementById("fuerzame");
            check = document.getElementById("forzar");
            if (check.checked) {
                element.style.display='block';
            }
            else {
                element.style.display='none';
            }
        }
        $(document).ready(function () {
            $('#fock1').formValidation({
                message: 'Este valor no es admitido',

                locale: 'es_ES',

                fields: {
                    host: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'Este campo debe tener mínimo 1 carácteres y máximo 30.'
                            }
                        }
                    },
                    baseName: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'Este campo debe tener mínimo 1 carácteres y máximo 30.'
                            }
                        }
                    },
                    userName: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 1,
                                max: 30,
                                message: 'Este campo debe tener mínimo 1 carácteres y máximo 30.'
                            }
                        }
                    },
                    condiciones: {
                        validators: {
                            notEmpty: {
                                message: 'Debe aceptar los términos para continuar.'
                            }
                        }
                    }
                }
            });

        });
    </script>
<?php } ?>
<?php if(isset($_GET['lock'])){ ?>
    <script type="text/javascript">
        $('.form-control').attr('readonly',true);
        $('#parrafo').attr('autofocus',true).text('Se ha verificado exitosamente la información. Ahora puede proceder con la inicialización de la base de datos. Haga clic en el botón inferior para continuar.');
        $('#sigue').removeClass('bg-blue-gradient').addClass('bg-green-gradient').html('<i class="fa fa-check-square-o"></i>   Inicializar la base de datos');
        $('#0').removeClass('glyphicon-home').addClass('glyphicon-check');
        $('#1').removeClass('glyphicon-hdd').addClass('glyphicon-check');
        $('#2').removeClass('glyphicon-user').addClass('glyphicon-check');
        $('#3').removeClass('glyphicon-lock').addClass('glyphicon-check');
        $('.host').removeAttr('value').attr("placeholder","<?php echo $host ?>");
        $('.1').attr("placeholder","<?php echo $baseName ?>");
        $('.2').attr('placeholder','<?php echo $userName ?>');
        $('.3').attr('placeholder','***');
        $('#condiciones').attr('checked','checked').attr('disabled','disabled');
        $('#sigue').removeAttr('disabled').removeClass('disabled');
    </script>
<?php } ?>
</body>
</html>
