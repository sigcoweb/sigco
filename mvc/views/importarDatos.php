<?php
include_once '../facades/ImportarFacade.php';
include_once '../utilities/installDto.php';
include_once '../utilities/config.php';
$tablasBase = new ImportarFacade();
$dataBase=new installDto();
session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Importar datos</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <?php include_once 'userPanel.php'; ?>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Importar
                <small>Datos</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li>Clientes</li>
                <li class="active">Buscar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 'true') {
                            echo 'alert-error';
                        } else {
                            echo 'alert-success';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="fa fa-<?php if($_GET['error']=='true'){echo 'warning';}else{echo 'check';};?>">   </i>    Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 'true') {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>

                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la importación</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Seleccione la tabla y cargue un archivo con la información necesaria para importar los datos(*).
                            </p>
                        </div>
                    </div>

                    <form role="form" id="formValidacion" action="../controllers/ImportarController.php" method="post" enctype="multipart/form-data">
                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Importar archivo con información</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for"Tabla">Seleccione la tabla*</label>
                                                <select class="form-control select2" name="Tabla" id="Tabla" required
                                                        tabindex="8">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    $todasTablas = $tablasBase->listarTablas();
                                                    foreach ($todasTablas as $tabla) {
                                                        ?>
                                                        <option
                                                            value="<?php $nombre='Tables_in_'.$baseName; echo $tabla[$nombre] ?>"><?php echo $tabla[$nombre] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="verEjemplo" id="verEjemplo" style="display: none">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <p>¿Ver ejemplo de los datos?</p>
                                                <label for="check">
                                                    <input class="form-group" type="checkbox" name="check" id="check" value="1" onchange="javascript:showContent()" />Mostrar ejemplo</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-info box-solid" id="boxOculto" style="display: none;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ejemplo</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <p>Para importar información debe diligenciar un archivo con los siguientes campos:</p>
                                <div id="aquiEs" class="table-responsive">
                                </div>
                                <p class="help-block">Si desea puede descargar un archivo de ejemplo con el siguiente botón.
                                    El archivo de ejemplo ya contiene las cabeceras y está listo para diligenciar, de
                                    tal forma que una vez registrada la información en el archivo, éste
                                    puede ser cargado con el botón "Importar archivo" para finalizar el proceso.</p>
                            </div>
                            <div class="box-footer">
                                <a href="" class="btn btn-info" tabindex="15"
                                   name="ejemplo" id="ejemplo">Descargar archivo de ejemplo
                                </a>
                            </div>
                        </div>

                        <div class="box box-primary box-solid" id="boxOculto2" style="display: none;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Archivo con la información</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <p>Seleccione el archivo con las catarcterísticas anteriores</p>
                                <div class="form-group">
                                    <p class="help-block">Seleccione un archivo de texto plano, con valores separados por comas (.csv o .txt)</p>
                                    <input class="btn" type="file" id="exampleInputFile" required name="Archivo">
                                    <p class="help-block">El archivo no debe ser mayor a 5 mb</p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="separador">Separador de los campos*</label><br>
                                            <input id="separador" type="text" name="separador" value=";" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="encabezado">¿La primera fila tiene encabezados?*</label><br>
                                            <input id="encabezado" type="radio" name="encabezado" value="0" required>No
                                            <input id="encabezado" type="radio" name="encabezado" value="1" required>Si
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success" tabindex="15"
                                        value="importar" name="importar" id="importar">Importar archivo
                                </button>
                            </div>
                        </div>
                    </form>
                </div><!--/.col (right) -->


            </div>   <!-- /.row -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php'; ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
    <script type="text/javascript">
        verEjemplo = document.getElementById("verEjemplo");
        ejemplo = document.getElementById("boxOculto");
        ejemplo2 = document.getElementById("boxOculto2");
        function showContent() {
            check = document.getElementById("check");
            if (check.checked) {
                ejemplo.style.display='block';
            }
            else {
                ejemplo.style.display='none';
            }
        }
        $(document).ready(function () {

            $("#Tabla").change(function () {
                ejemplo2.style.display='block';
                verEjemplo.style.display='block';
                $("#Tabla option:selected").each(function () {
                    nombreTabla = $(this).val();
                    $("#ejemplo").attr('href','../utilities/exportarPlantilla.php?tabla='+nombreTabla);
                    //$("#IdLugar").selectedIndex=0;
                    //document.getElementById("#IdLugar").selectedIndex=0;
                    //$('#IdLugar').val('0');
                    //$('#IdLugar').change();
                    $.post("camposTabla.php", { nombreTabla: nombreTabla }, function(data){
                        $("#aquiEs").html(data);
                    });
                });

            });


            $('#formValidacion').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    Nit: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            }
                        }
                    },
                    RazonSocial: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 50.'
                            }
                        }
                    },
                    Direccion: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 50.'
                            }
                        }
                    },
                    Telefono: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            }
                        }
                    },
                    Email2: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            }
                        }
                    },
                    IdLugar: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdTipo: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdActividad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    Cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            }
                        }
                    },
                    Nombres: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Apellidos: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Celular: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            }
                        }
                    },
                    Email1: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            }
                        }
                    }

                }
            });
        });
    </script>
    <!-- Page script-->
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>
</body>
</html>