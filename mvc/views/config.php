<?php
include_once '../utilities/installDto.php';
include_once '../facades/installFacade.php';
$facaInstall=new installFacade();
$dtoinstall=new installDto();
include_once '../utilities/config.php';
if($installed=='1'&&$dtoinstall->existsConection($host,$baseName,$userName,$userPass)==1){ ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Configuración</title>
        <link rel="icon" href="../../favicon.ico" type="image/x-icon">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.4 -->
        <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
        <!-- Theme style -->
        <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="../../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

        <!-- FORMVALIDATION -->
        <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
        <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
        <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
        <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="register-page">
    <div class="register-box">
        <?php if(isset($_GET['error'])||isset($_GET['mensaje'])){ ?>
            <div class="box box-warning box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Error:</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php echo $_GET['mensaje']; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        <?php } ?>
        <div class="register-logo">
            <b>Configuración</b>SIGCO
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Paso 2 de 2:</p>
            <p class="login-box-msg">Se ha inicializado correctamente la base de datos.</p>
            <p class="login-box-msg">Antes del primer uso, debe crear el usuario administrador del sistema con el siguiente formulario:</p>
            <form id="fock1" action="../controllers/installerController.php?config=true" method="post">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="adminName" placeholder="Usuario administrador" />
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="adminPass" placeholder="Contraseña del administrador" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="adminPass2" placeholder="Confirme la contraseña" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" name="adminEmail" placeholder="Correo del administrador" />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn bg-blue-gradient btn-block btn-flat pull-right"><i class="fa fa-user-plus"></i>  Registrar administrador y finalizar</button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 --
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#fock1').formValidation({
                message: 'Este valor no es admitido',

                locale: 'es_ES',

                fields: {
                    adminName: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 5,
                                max: 15,
                                message: 'Este campo debe tener mínimo 5 carácteres y máximo 15.'
                            },
                            between: {
                                min: 10000,
                                max: 100000000000000,
                                message: 'Debe ser un número entero positivo.'
                            }
                        }
                    },
                    adminEmail: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$'
                            }
                        }
                    },
                    adminPass:{
                        validators:{
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    adminPass2:{
                        validators:{
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            identical: {
                                field: 'adminPass',
                                message: 'La contraseña debe ser identica a la anterior.'
                            }

                        }

                    }
                }
            });
        });
    </script>
    </body>
    </html>
    <?php
}else{
    header("location: install.php?error=true&mensaje=No se ha realizado la conexión, verifique los requisitos.");
}
?>
