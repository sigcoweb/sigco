<?php
include_once '../facades/FacadeCotizaciones.php';
include_once '../facades/PuntosEntregaFacade.php';
include_once '../facades/ClienteFacade.php';
$clienteFac= new ClienteFacade();
$puntosFaca=new PuntosEntregaFacade();
$coti = new FacadeCotizaciones();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Registrar pedido</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <?php include_once 'userPanel.php'; ?>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Pedido</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Pedido</a></li>
                <li class="active">Crear pedido</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if($coti->buscarCotizacion($_GET['idcliente'])[0]['diaCierreFacturacion']<date('d',time())) {
                        ?>
                        <div class="box box-warning box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    Advertencia: se ha superado el día de facturación para el cliente</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>A éste cliente sólo debería generarle pedidos/facturas antes del día <span
                                        class="label label-danger badge">
                                    <?php
                                    echo $coti->buscarCotizacion($_GET['idcliente'])[0]['diaCierreFacturacion'];
                                    ?>
                                </span> de cada mes.</p>
                                <p>Es posible que no le acepten la factura debido a que ya efectuaron cierre de
                                    facturación.</p>
                                <p>Recuerde que puede generar una cotización nueva utilizando una ya existente para
                                    agilizar el proceso.</p>
                            </div>
                            <div class="box-footer">
                                <button class="btn bg-green-gradient pull-left" onclick="location.href='index.php'" >
                                    <i class="fa fa-arrow-left"></i>  No deseo continuar, ir al inicio
                                </button>
                                <div class="pull-right">
                                    <input type="checkbox" name="forzar" id="forzar" value="1"
                                           onchange="javascript:showContent()">
                                    <label for="forzar"> Entiendo los riesgos, deseo generar el pedido
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div id="oculto" style="display: <?php
                    if($coti->buscarCotizacion($_GET['idcliente'])[0]['diaCierreFacturacion']<date('d',time())){
                        echo 'none';
                    }else{
                        echo 'block';
                    }
                    ?>;">



                        <?php if (isset($_GET['mensaje'])) { ?>
                            <div class="alert
                      <?php if ($_GET['error'] == 'true') {
                                echo 'alert-error';
                            } else {
                                echo 'alert-success';
                            } ?>
                      alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="fa fa-<?php if ($_GET['error'] == 'true') {
                                        echo 'warning';
                                    } else {
                                        echo 'check';
                                    }; ?>">
                                    </i> Resultado del proceso:</h4>
                                <p>
                                    <?php echo $mensaje = $_GET['mensaje'] ?>
                                </p>
                                <?php if (isset($_GET['activo']) && $_GET['activo'] == 'true') { ?>
                                    <a href="crearCotizacion.php?nit=<?php echo $_GET['nit'] ?>"
                                       class="btn bg-blue-gradient btn-xs">Añadir cotización</a>

                                <?php } ?>
                            </div>

                            <?php if (isset($_GET['detalleerror']) && $_GET['error'] == 'true') { ?>

                                <div class="box box-danger box-solid collapsed-box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ver detalle del error</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-plus"></i>
                                            </button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-remove"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <p>
                                            <?php echo $mensaje = $_GET['detalleerror'] ?>
                                        </p>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                    </div>
                                </div><!-- /.box -->
                                <?php
                            }
                        }
                        if(!isset($_GET['idcliente'])||count($coti->buscarCotizacion($_GET['idcliente']))==0) { ?>
                            <div class="box box-warning box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                        La cotización no es válida o no está asignada a su cuenta</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>Debe seleccionar una cotización válida para generar un pedido.</p>
                                </div>
                                <div class="box-footer">
                                    <input type="button" class="btn bg-green-gradient pull-right" tabindex="16"
                                           onclick="location.href='crearOrden1.php'" value="Seleccionar otra cotización"/>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                        <?php }else { $cotizacion=$coti->buscarCotizacion($_GET['idcliente']); ?>

                            <form id="defaultForm" action="../controllers/ControladorOrdenCompra.php?crear=true"
                                  method="post">
                                <div class="box box-default box-solid collapsed-box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Indicaciones de registro</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-plus"></i>
                                            </button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-remove"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <p>
                                                Por favor diligencie el siguiente formulario para registrar la
                                                información.<br>
                                                Recuerde que este formulario contiene campos obligatorios(*).
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <input name="idcoti" hidden type="text"
                                       value="<?php echo $cotizacion[0]['IdCotizacion'] ?>">
                                <div class="box box-solid box-info">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Registrar Pedido</h3>
                                    </div>

                                    <div class="box-body">


                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="valorcoti">Total a pagar:*</label>
                                                    <input type="text" hidden name="valorpedido" value="<?php echo $cotizacion[0]['ValorTotalCotizacion'] ?>">
                                                    <input class="form-control" name="valorcoti" id="valorcoti" type="text"
                                                           value="$ <?php echo number_format($cotizacion[0]['ValorTotalCotizacion']) ?>"
                                                           readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="buttonOrd">O.C. cliente</label>
                                                    <button type="button" id="mostrarOrden" class="btn btn-gray btn-block off" onclick="orden()">Agregar
                                                        orden
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="form-group" id="ordenCompra">
                                                    <label for="ordenDeCompra">Cód. Orden de compra:*</label>
                                                    <input class="form-control" name="ordenDeCompra" id="ordenDeCompra"
                                                           type="text" placeholder="24312">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="remisionado">Remisionado:*</label>
                                                    <select class="form-control select2" style="width: 100%;"  name="remisionado" id="remisionado"
                                                            required autofocus tabindex="1">
                                                        <option value="" disabled selected>Seleccionar</option>
                                                        <option value="Si">Si</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="facturado">Facturado:*</label>
                                                    <select class="form-control select2" style="width: 100%;"  name="facturado" id="facturado" required tabindex="2">
                                                        <option value="" disabled selected>Seleccionar</option>
                                                        <option value="Si">Si</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="certificado">Certificado:*</label>
                                                    <select class="form-control select2" style="width: 100%;"  name="certificado" id="certificado"
                                                            required tabindex="3">
                                                        <option value="" disabled selected>Seleccionar</option>
                                                        <option value="Si">Si</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="fichaTecnica">Ficha técnica:*</label>
                                                    <select class="form-control select2" style="width: 100%;"  name="fichaTecnica" id="fichaTecnica"
                                                            required tabindex="4">
                                                        <option value="" disabled selected>Seleccionar</option>
                                                        <option value="Si">Si</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="fichaSeguridad">Ficha seguridad:*</label>
                                                    <select class="form-control select2" style="width: 100%;"  name="fichaSeguridad" id="fichaSeguridad"
                                                            required tabindex="5">
                                                        <option value="" disabled selected>Seleccionar</option>
                                                        <option value="Si">Si</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cantidad">Observaciones (opcional)</label>
                                            <input class="form-control" name="observaciones" id="observaciones" type="text"
                                                   placeholder="Por favor entregar en horas de la tarde" tabindex="6">
                                        </div>
                                        <div class="row">
                                            <?php
                                            if(count($puntosFaca->buscarPunto("puntosentrega.nitEmpresaPuntoEntrega",$cotizacion[0]['Nit'],1))>0){ $puntos=$puntosFaca->buscarPunto("puntosentrega.nitEmpresaPuntoEntrega",$cotizacion[0]['Nit'],1); ?>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="direccion">Dirección de entrega:*</label>
                                                        <select class="form-control select2" style="width: 100%;"  name="direccion" id="direccion"
                                                                required tabindex="7">
                                                            <option value="" disabled selected>Seleccionar...</option>
                                                            <?php
                                                            foreach ($puntos as $punto) {?>
                                                                <option value="<?php echo $punto['nombrePuntoEntrega']?>"><?php echo $punto['nombrePuntoEntrega'].' | '.$punto['nombreContactoPuntoEntrega'].' | '.$punto['direccionPuntoEntrega'].' - '.$punto['NombreLugar'].',  '.$punto['nombreDepartamento'].' | '.$punto['observacionesPuntoEntrega'] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-lg-<?php if(!isset($puntos)||count($puntos)==0){echo 12;}else{echo 4;} ?>">
                                                <div class="form-group">
                                                    <label for="otroPunto">
                                                        <?php
                                                        if(!isset($puntos)||count($puntos)==0){ ?>
                                                        <i class="fa fa-exclamation-triangle"></i>  Debe agregar un punto de entrega para registrar el pedido*</label>
                                                    <?php }else{ echo 'Registrar otro punto';} ?>
                                                    <br>
                                                    <label class="btn bg-yellow-gradient click " ><i class="fa fa-plus-square"> </i>   Agregar punto de entrega</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <button type="submit" class="btn bg-green-gradient pull-left <?php if(!isset($puntos)||count($puntos)==0){echo 'hidden';}; ?>" tabindex="8" <?php if(!isset($puntos)||count($puntos)==0){echo 'hidden';}; ?>
                                                    value="guardar" name=" guardar" id="guardar"><i class="fa fa-floppy-o"></i>   Guardar pedido
                                            </button>


                                            <script>
                                                function orden(){
                                                    if( $('#ordenCompra').is(":visible") ){
                                                        $('#ordenCompra').hide();
                                                        document.getElementById('mostrarOrden').textContent="Agregar orden";


                                                    }else{
                                                        $('#ordenCompra').show();
                                                        document.getElementById('mostrarOrden').textContent="Ocultar orden";
                                                    }

                                                }

                                                function comprobar(){
                                                    if ($('#cambiar').prop('checked')){
                                                        document.getElementById('guardar').textContent="Continuar";
                                                        $('#defaultForm').attr('action','../controllers/ControladorOrdenCompra.php?crear=true&change=true');

                                                    }else{
                                                        document.getElementById('guardar').textContent="Guardar Orden";
                                                        $('#defaultForm').attr('action','../controllers/ControladorOrdenCompra.php?crear=true');
                                                    }
                                                }


                                            </script>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        <?php } ?>

                    </div>
                </div>

                <!-- /.box -->
            </div>

        </section>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="modal-register" tabindex="-1"
         role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header alert-info">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title text-center" id="modal-register-label">Añadir punto de entrega</h3>
                </div>
                <div class="modal-body">
                    <form role="form" id="formModal" action="../controllers/PuntosEntregaController.php?controlar=crear"
                          method="post" class="registration-form">
                        <div class="row">
                            <input type="text" name="return" id="return" class="form-control hidden" hidden
                                   value="crearOrden.php?idcliente=<?php echo $_GET['idcliente']; ?>"/>
                            <input type="text" name="nitEmpresaPuntoEntrega" id="nitEmpresaPuntoEntrega" class="form-control hidden" hidden
                                   value="<?php echo $cotizacion[0]['Nit'] ?>"/>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nombrePuntoEntrega" id="labelNit">Nombre del punto entrega (único)*</label>
                                    <input type="text" name="nombrePuntoEntrega" id="nombrePuntoEntrega" class="form-control"
                                           placeholder="Punto norte" autofocus required tabindex="11"/>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="direccionPuntoEntrega">Dirección*</label>
                                    <input type="text" name="direccionPuntoEntrega" id="direccionPuntoEntrega" class="form-control"
                                           placeholder="Calle 80 A No. 12 A - 21" required tabindex="12"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nombreContactoPuntoEntrega">Nombre contacto*</label>
                                    <input type="text" name="nombreContactoPuntoEntrega" id="nombreContactoPuntoEntrega" class="form-control"
                                           placeholder="Carlos Slimmer" required tabindex="13"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="correoPuntoEntrega">Correo electrónico*</label>
                                    <input type="email" name="correoPuntoEntrega" id="correoPuntoEntrega" class="form-control"
                                           placeholder="carlos@empresa.com" required tabindex="14"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="IdDepartamento">Departamento*</label><br>
                                    <select class="form-control select2 depto" name="IdDepartamento" id="IdDepartamento" style="width: 100%" required tabindex="15">
                                        <option value="0" disabled selected>Seleccione...</option>
                                        <?php
                                        include_once '../facades/DepartamentosFacade.php';
                                        $deptosFac = new DepartamentosFacade();
                                        $todos = $deptosFac->listarTodos();
                                        foreach ($todos as $depto) {
                                            ?>
                                            <option
                                                value="<?php echo $depto['idDepartamento'] ?>">
                                                <?php echo $depto['nombreDepartamento'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="idLugarPuntoEntrega">Lugar*</label><br>
                                    <select class="form-control select2" style="width: 100%;"  disabled name="idLugarPuntoEntrega" id="idLugarPuntoEntrega" style="width: 100%" required tabindex="16">
                                        <option value="0" disabled selected>Seleccione primero un departamento...</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telefonoPuntoEntrega">Teléfono fijo o celular*</label>
                                    <input type="text" name="telefonoPuntoEntrega" id="telefonoPuntoEntrega" class="form-control"
                                           placeholder="634342"
                                           required tabindex="17"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="observacionesPuntoEntrega">Observaciones (opcional)</label>
                                    <input type="text" name="observacionesPuntoEntrega" id="observacionesPuntoEntrega" class="form-control"
                                           placeholder="Horario de lunes a viernes de 8am a 5pm"  tabindex="18"/>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="guardar" class="btn bg-green-gradient" tabindex="19">
                            <i class="fa fa-refresh"></i>   Registrar punto y regresar</button>
                    </form>

                </div>

            </div>
        </div>
    </div>


    <!-- Main Footer -->
    <?php include_once 'footer.php'; ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <script>
        $('.click').on('click',function(){
            $('#modal-register').modal();
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        });
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#ordenCompra').hide();

            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    }
                }
            });

            $('#formModal').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    direccionPuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 100,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 100.'
                            }
                        }
                    },
                    nombrePuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 carácteres y máximo 50.'
                            }
                        }
                    },
                    telefonoPuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            }
                        }
                    },
                    correoPuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            }
                        }
                    },
                    IdDepartamento: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    idLugarPuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    Cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            }
                        }
                    },
                    nombreContactoPuntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Celular: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            }
                        }
                    }
                }
            });
        });

        $("#IdDepartamento").change(function () {
            $('#idLugarPuntoEntrega').removeAttr("disabled");
            $("#IdDepartamento option:selected").each(function () {
                idDepto = $(this).val();
                //$("#IdLugar").selectedIndex=0;
                //document.getElementById("#IdLugar").selectedIndex=0;
                $('#idLugarPuntoEntrega').val('0');
                $('#idLugarPuntoEntrega').change();
                $.post("lugaresDepto.php", { idDepto: idDepto }, function(data){
                    $("#idLugarPuntoEntrega").html(data);
                });
            });
        });
    </script>
    <script type="text/javascript">
        function showContent() {
            element = document.getElementById("oculto");
            check = document.getElementById("forzar");
            if (check.checked) {
                element.style.display='block';
            }
            else {
                element.style.display='none';
            }
        }
    </script>
</body>
</html>