<?php
include_once '../facades/FacadeCotizaciones.php';
$cotFaca = new FacadeCotizaciones();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Buscar cotizaciones</title>
    <link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/cirular-buttons.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Cotizaciones
                <small>Buscar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li>Cotizaciones</li>
                <li class="active">Buscar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">


                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>

                    <p class="text-info"><i class="fa fa-exclamation-circle"></i>  Recuerde que este formulario contiene campos obligatorios (*)</p>


                    <div class="box box-solid box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Opciones de búsqueda</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">


                            <form role="form" id="defaultForm" action="../controllers/controladorCotizacion.php?buscar=true" method="post">

                                <div class="form-group">
                                    <label for="criterio">Seleccione un criterio de búsqueda*</label>
                                    <select class="form-control select2" name="criterio" id="criterio"
                                            data-toggle="tooltip" title="Indique una categoría" required tabindex="1" autofocus>
                                        <option value="cotizaciones.EstadoCotizacion" selected>Estado Cotización</option>
                                        <option value="clientes.RazonSocial" >Razon social cliente</option>
                                        <option value="cotizaciones.IdCotizacion" >ID Cotización</option>
                                        <option value="cotizaciones.NitClienteCotizaciones" >Nit cliente</option>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                            <option value="cotizaciones.CedulaEmpleadoCotizaciones" >Cédula asesor</option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>

                                <label for="comobuscar" >¿Cómo desea consultar? y ¿Qué desea encontrar?*</label>
                                <div class="input-group input-group-sm margin">
                                    <div class="input-group-btn">
                                        <select name="comobuscar" id="comobuscar" class="btn bg-light-blue-gradient dropdown-toggle" data-toggle="dropdown" tabindex="2">
                                            <option value="1">Una búsqueda exacta de</option>
                                            <option selected value="2">Cualquier coincidencia de</option>
                                        </select>
                                    </div><!-- /btn-group -->
                                    <input type="text" name="busqueda" class="form-control" placeholder="Número Nit | Razón Social | Lugar" required
                                           data-toggle="tooltip" title="Indique lo que desea buscar" tabindex="3">
                    <span class="input-group-btn">
                      <button class="btn bg-aqua-gradient btn-flat" type="submit" tabindex="4" data-toggle="tooltip" title="Clic para consultar">
                          <i class="fa fa-search">     </i>     Buscar cotización(es)</button>
                    </span>
                                </div><!-- /input-group -->

                            </form>

                        </div>
                    </div>

                    <?php
                    if(isset($_GET['encontrados'])&&$_GET['encontrados']=='false') {
                        ?>


                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>

                            <p>No se han encontrado resultados para la consulta de:</p>
                            <p>Criterio: <?php echo $_GET['criterio'] ?></p>
                            <p>Búsqueda: <?php echo $_GET['busqueda'] ?></p>
                        </div>


                        <?php
                    }
                    if((isset($_GET['criterio'])&&isset($_GET['busqueda'])&&isset($_GET['comobuscar']))) {
                        $consulta = $cotFaca->buscarConCriterio($_GET['criterio'], $_GET['busqueda'], $_GET['comobuscar']);
                    }
                    if(isset($_GET['todos'])){
                        $consulta = $cotFaca->listarTodas();
                    }

                    if(isset($consulta)) {

                        if (count($consulta)>0) {

                            ?>

                            <div class="box box-primary box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Resultados de la búsqueda</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        Se han encontrado <span class="badge label-info"><?php echo count($consulta); ?></span>
                                        registros para esta consulta.
                                    </p>
                                    <br>
                                    <table id="example1" class="table table-striped table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>#Cot.</th>
                                            <th>#Nit</th>
                                            <th>Cliente</th>
                                            <?php
                                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                                ?>
                                                <th>#CC Asesor</th>
                                                <?php
                                            }
                                            ?>
                                            <th>Valor</th>
                                            <th>Estado</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $_SESSION['exportar'] = $consulta;
                                        foreach ($consulta as $respuesta){
                                        ?>
                                        <tr>
                                            <?php
                                            $now = time();
                                            $your_date = strtotime($respuesta['FechaCreacionCotizacion']);
                                            $datediff = $now - $your_date;
                                            $dueDate="";

                                            if(floor($datediff/(60*60*24))>=25 && $respuesta['EstadoCotizacion'] == 'Vigente'){
                                                $dueDate='style="background:#f39c12;color:#fff"';
                                            }?>
                                            <td <?php print $dueDate  ?> >

                                                <?php echo date("Y-m-d", strtotime($respuesta['FechaCreacionCotizacion']));
                                                if((30-floor($datediff/(60*60*24))>0)&& $respuesta['EstadoCotizacion'] == 'Vigente'){
                                                    print '<p></p><label class="alert-danger badge">dias a vencer ' . (30 - floor($datediff / (60 * 60 * 24))) . '</label>';
                                                }
                                                ?>
                                            </td>
                                            <td <?php print $dueDate  ?>>
                                                <?php echo $respuesta['IdCotizacion']; ?>
                                            </td>
                                            <td <?php print $dueDate  ?>>
                                                <?php echo $respuesta['NitClienteCotizaciones']; ?>
                                            </td>
                                            <td <?php print $dueDate  ?>>
                                                <?php echo $respuesta['RazonSocial']; ?>
                                            </td>
                                            <?php
                                            if ($_SESSION['datosLogin']['NombreRol'] == 'Administrador' || $_SESSION['datosLogin']['NombreRol'] == 'Coordinador') {
                                                ?>
                                                <td <?php print $dueDate  ?>>
                                                    <?php echo $respuesta['CedulaEmpleadoCotizaciones']; ?>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                            <td <?php print $dueDate  ?>>
                                                <h4>
                                                    <a class="label label-primary"
                                                       data-toggle="tooltip" title="Ver detalle" target="_blank"
                                                       href="ex2.php?coti=<?php print $respuesta['IdCotizacion'] ?>"
                                                       data-price="$<?php print number_format($respuesta['ValorTotalCotizacion']); ?>"
                                                       data-nit="<?php print $respuesta['NitClienteCotizaciones'];?>"
                                                       data-empresa="<?php print $respuesta['RazonSocial']; ?>">
                                                        <i class="fa fa-file-pdf-o">
                                                            $<?php echo number_format($respuesta['ValorTotalCotizacion']); ?>
                                                        </i>
                                                    </a>
                                                </h4>
                                            </td>
                                            <td <?php print $dueDate  ?>>
                                                <?php if ($respuesta['EstadoCotizacion'] == 'Vigente') { ?>
                                                    <div class="btn-group" data-toggle="tooltip" title="Cambiar estado">
                                                        <button type="button" class="btn btn-xs btn-success">
                                                            <?php echo $respuesta['EstadoCotizacion']; ?>
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-xs btn-success dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="crearOrden.php?idcliente=<?php echo $respuesta['IdCotizacion']; ?>">
                                                                    Generar pedido
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="click" data-name="<?php echo $respuesta['IdCotizacion']; ?> "
                                                                   data-price="$<?php echo number_format($respuesta['ValorTotalCotizacion']); ?>">
                                                                    Cancelar
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php } else { ?>
                                                    <span
                                                        class="label label-<?php if ($respuesta['EstadoCotizacion'] == 'Cancelada') {
                                                            echo 'warning';
                                                        } else if ($respuesta['EstadoCotizacion'] == 'Vigente') {
                                                            echo 'success';
                                                        } else if ($respuesta['EstadoCotizacion'] == 'Pedido') {
                                                            echo 'default';
                                                        }
                                                        ?>">
                                                    <?php echo $respuesta['EstadoCotizacion']; ?>
                                                        </span>
                                                <?php } ?>
                                                <?php if(strlen($respuesta['ObservacionesCotizacion'])>0){ ?>
                                                    <button class="btn btn-bitbucket pop" style="width: 30px;
                                                    height: 30px;
                                                    padding: 6px 0;
                                                    border-radius: 15px;
                                                    text-align: center;
                                                    font-size: 14px;
                                                    color: white;
                                                    margin-left: 3%;
                                                    line-height: 1.428571429;" data-toggle="popover"
                                                            data-placement="right"
                                                            title="Observación:"
                                                            tabindex="0" data-html="true"
                                                            data-content="
                                                        <?php
                                                            print '<p>' . $respuesta['ObservacionesCotizacion'] . '</p>';
                                                            ?>"><i class="fa fa-weixin"></i></button>
                                                <?php }?>
                                            </td>
                                            <?php

                                            }

                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>#Cot.</th>
                                            <th>#Nit</th>
                                            <th>Cliente</th>
                                            <?php
                                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                                ?>
                                                <th>#CC Asesor</th>
                                                <?php
                                            }
                                            ?>
                                            <th>Valor</th>
                                            <th>Estado</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    if (!isset($_GET['todos'])){ ?>
                        <!-- general form elements disabled -->
                        <div class="box box-success box-solid collapsed-box">                             <div class="box-header with-border">                                 <h3 class="box-title">Ver todos los registros</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <p>
                                    Si desea, puede ver todos los registros usando el botón "Ver todos"
                                    (esta opción puede tardar un poco).
                                </p>
                            </div>
                            <div class="box-footer">
                                <form action="../views/buscarCotizaciones.php?todos=todos" method="post">
                                    <button type="submit" class="btn bg-green-gradient pull-right" tabindex="14"
                                    > <i class="fa fa-plus-square-o"> </i>   Ver todos
                                    </button>
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div><!--/.col (right) -->
            </div>   <!-- /.row -->




        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 5px">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title" id="register-label">Motivo de cancelación</h3>
                <p>llene el siguiente campo con maximo 100 caracteres:</p>
            </div>

            <div class="modal-body">

                <form role="form" id="form-cancel" action="../controllers/controladorCotizacion.php" method="post" class="registration-form">
                    <div class="form-group">
                        <input id="cancelar" type="text" hidden name="cancelar">
                        <input id="cotizacionId" type="text" hidden name="id">
                        <label class="sr-only" for="form-about-yourself">About yourself</label>
	                        	<textarea rows="5"  placeholder="obsevaciones..."
                                          class="form-about-yourself form-control" name="comentario" required></textarea>
                    </div>
                    <button type="submit"  id="comentario" class="btn btn-success btn-block">Guardar comentario</button>
                </form>

            </div>

        </div>
    </div>
</div>

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                busqueda: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, 'asc' ]]
        });
    });
    $('.click').on('click',function(){
        $('#cotizacionId').val($(this).data("name"));
        $('#cancelar').val(true);
        $('#register-label').val('cliente'+$(this).data("nit"));
        $('#form-cancel').attr('action', '../controllers/controladorCotizacion.php?id='+$(this).data("name")+'&cancelar=true');
        $('#modal-register').modal();

    });
</script>
</html>
