<?php
include_once '../facades/ImportarFacade.php';
include_once '../utilities/installDto.php';
include_once '../utilities/config.php';
$tablasBase = new ImportarFacade();
$dataBase=new installDto();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exportar base de datos</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <?php include_once 'userPanel.php'; ?>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Exportar
                <small>base de datos</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 'true') {
                            echo 'alert-error';
                        } else {
                            echo 'alert-success';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="fa fa-<?php if($_GET['error']=='true'){echo 'warning';}else{echo 'check';};?>">   </i>    Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 'true') {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gestión de base de datos</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p>¿Qué desea hacer?</p>
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-primary">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                A. Descargar o guardar un backup actual
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>¿Guardar el backup o generar la descarga?</p>
                                                        <div class="col-lg-3">
                                                            <input type="radio" name="guardar" id="descargar" value="descargar" onclick="selectType()" checked>
                                                            <label for="descargar">Descargar</label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <input type="radio" name="guardar" id="guardar" value="guardar" onclick="noSelectType()">
                                                            <label for="guardar">Guardar .txt en el servidor</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="seleccionarTipo">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <p>Seleccione un formato para descargar el backup:</p>
                                                        <div class="col-sm-3">
                                                            <input class="sel" type="radio" name="tipo" id="txt" value="txt" checked>
                                                            <label for="txt">Sólo texto plano .txt</label>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input class="sel" type="radio" id="gz" name="tipo" value="gz">
                                                            <label for="gz">Txt comprimido en .gz</label>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input class="sel" type="radio" id="bz2" name="tipo" value="bz2">
                                                            <label for="bz2">Txt comprimido en .bz2</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn bg-aqua-gradient" id="boton" type="submit" href="">
                                                <i class="fa fa-database"></i> Generar backup de la base de datos
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel box box-danger">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                B. Descargar un backup almacenado
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <p>Seleccione un archivo para descargar:</p>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="form-control select2" name="archivo" id="archivo" style="width: 100%">
                                                                    <?php
                                                                    $arch=0;
                                                                    $ruta=("../documents/backsdb/");
                                                                    $directorio = opendir($ruta);
                                                                    while ($archivo = readdir($directorio)){
                                                                        if (!is_dir($archivo)&&$arch==0){ ?>
                                                                            <option value="" disabled selected>Seleccione un archivo</option>
                                                                            <?php $arch++; }; if(!is_dir($archivo)&&$arch>0){ ?>
                                                                            <option value="<?php echo $ruta.$archivo; ?>"><?php echo $archivo; ?></option>
                                                                        <?php }
                                                                    }if($arch==0){ ?>
                                                                        <option value="" disabled>No se han almacenado backups de la base de datos</option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn bg-light-blue-gradient disabled" id="btnDescarga" type="submit" href="">
                                                <i class="fa fa-exclamation-triangle"></i>  Primero debe seleccionar una opción
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->


            </div>   <!-- /.row -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php'; ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <script type="text/javascript">
        $("#archivo").change(function () {
            $('#btnDescarga').removeClass("disabled").html('<i class="fa fa-download"></i> Descargar copia seleccionada');
        });
        seleccionarTipo = document.getElementById("seleccionarTipo");
        function selectType() {
            seleccionarTipo.style.display='block';
        }
        function noSelectType() {
            seleccionarTipo.style.display='none';
        }
        $(document).ready(function()
        {
            $("#boton").click(function () {
                ruta1='../controllers/backupBaseController.php?tipo='+$('input:radio[name=tipo]:checked').val();
                ruta1+='&guardar='+$('input:radio[name=guardar]:checked').val();
                $("#boton").attr('href',ruta1);
            });
            $("#btnDescarga").click(function () {
                ruta2='../controllers/backupBaseController.php?descargar=descargar&archivo='+$( "#archivo option:selected" ).text();
                $("#btnDescarga").attr('href',ruta2);
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $(".select2").select2();
        });
    </script>
</body>
</html>