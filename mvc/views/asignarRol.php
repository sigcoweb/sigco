<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html>
  <head>
    <meta charset="UTF-8">
    <title>Asignar Rol</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

      <script type="text/javascript" src="../../plugins/select2/select2.js"></script>
      <link href="../../plugins/select2/select2.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="../../date/jquery-ui.css">
  <script src="../../date/jquery-ui.js"></script>
  <script src="../../date/jquery-ui.theme.css"></script>
<!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <?php include_once 'header.php'; ?>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar iterator panel (optional) -->
                <div class="user-panel">
                    <?php include_once 'userPanel.php'; ?>
                </div>
                <?php include_once 'menu.php' ?>
            </section>
            <!-- /.sidebar-menu -->
            <!-- /.sidebar -->
        </aside>
      <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
              <!-- Content Header (Page header) -->
              <section class="content-header">
                  <h1>
                      Formulario de asignación
                      <small>Roles</small>
                  </h1>
                  <ol class="breadcrumb">
                      <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                      <li><a href="#">Empleados</a></li>
                      <li class="active">Asignar rol</li>
                  </ol>
              </section>

              <!-- Main content -->
              <section class="content">
                  <div class="row">

                      <!-- right column -->
                      <div class="col-md-10">
                          <?php
                          if (isset($_GET['mensaje'])) {
                              ?>
                              <div class="alert
                      <?php if ($_GET['error'] == 1) {
                                  echo 'alert-error';
                              } else {
                                  echo 'alert-info';
                              } ?>
                      alert-dismissable">
                                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                  <h4>Resultado del proceso:</h4>
                                  <?php echo $mensaje = $_GET['mensaje'] ?>
                              </div>

                              <?php
                              if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                                  ?>

                                  <div class="box box-danger collapsed-box">
                                      <div class="box-header with-border">
                                          <h3 class="box-title">Ver detalle del error</h3>

                                          <div class="box-tools pull-right">
                                              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                              </button>
                                              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                              </button>
                                          </div>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                          <p>
                                              <?php echo $mensaje = $_GET['detalleerror'] ?>
                                          </p>
                                      </div>
                                      <!-- /.box-body -->
                                      <div class="box-footer">
                                          Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                      </div>
                                  </div><!-- /.box -->
                                  <?php
                              }
                          }
                          ?>
                    <form id="defaultForm" action="../controllers/ControladorRolesUsuarios.php" method="post">

                      <div class="box box-default">
                                  <div class="box-header with-border">
                                      <h3 class="box-title">Asignar Rol</h3>
                                  </div>
                                  <!-- /.box-header -->
                                  <div class="box-body">
                                      
                                          <div class="form-group">
                                              <p>
                                                  Por favor diligencie el siguiente formulario para Asignar el rol a un empleado.<br><br>
                                                  Recuerde que este formulario contiene campos obligatorios(*).
                                              </p>
                                          </div>
                                  </div>
                              </div>

                          <!-- general form elements disabled -->
                           

                          <div class="box box-default">
                              <div class="box-header with-border">
                                  <h3 class="box-title">Asignar Rol</h3>
                              </div>

                           <div class="box-body">

                               <div class="form-group">
                                   <label for="cedula">Seleccionar Cédula Empleado*</label>
                                   <select class="form-control select2" name="cedula" id="cedula" required>
                                       <option value="">Seleccionar</option>
                                       <?php
                                       include_once'../facades/FacadeEmpleado.php';

                                       $facade= new FacadeEmpleado();
                                       $lista=$facade->listarDocumentos();
                                       foreach ($lista as $recorrer ) { ?>
                                        <option value="<?php echo $recorrer['cc'] ?>"><?php echo $recorrer['cc']." | ".$recorrer['Nombres'].' '.$recorrer['Apellidos'] ?></option>
                                       <?php
                                       }

                                       ?>
                                   </select>
                               </div>

                               <div class="form-group">
                                   <label for="rol">Seleccionar Rol*</label>
                                   <select class="form-control select2" name="rol" id="rol" required>
                                       <option value="">Seleccionar</option>
                                       <?php
                                        $roles=$facade->listarRoles();foreach ($roles as $mostrar ) { ?>
                                           <option value="<?php echo $mostrar['IdRol'] ?>"><?php echo $mostrar['NombreRol'] ?></option>
                                           <?php
                                       }

                                       ?>
                                   </select>
                               </div>
                                    <div class="box-footer">
                                          <input type="button" class="btn bg-yellow-gradient" tabindex="15"
                                                 onclick="location.href='index.php'" value="Cancelar"/>
                                          <button type="submit" class="btn btn-success pull-right" tabindex="14"
                                                  value="guardar" name=" guardar" id="guardar">Asignar
                                          </button>
                                      </div>

                                                                   </div>

                                  </div>
                                                            </form>

                          </div>

                          <!-- /.box -->
                    

                      <!--/.col (right) -->
                  </div>
                  <!-- /.row -->
              </section>
              <!-- /.content -->
          </div><!-- /.content-wrapper -->


      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
       
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="index.php">SIGCO</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
  </body>
  <script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
         
                locale: 'es_ES',

        fields: {
            cedula: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es requerido'
                    }
                }
            },
            rol: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es requerido'
                    }
                }
            }
        }
    });
});
</script>

  <script>
      $(".select2").select2();
      var rol = $('#rol');
      var op = document.getElementById("rol").getElementsByTagName("option");
      $('#cedula').on('change', function () {
          reload = $(this).val();
          for (var i = 0; i < op.length; i++) {

                   op[i].disabled = false;
          }
          $(".select2").select2();
          $.post("../controllers/ControladorRolesUsuarios.php",

              {
                  getCurrentRol: reload

              },
              function (data) {

                  var json =JSON.parse(data);


                  for(var j=0;j<json.length;j++) {

                      for (var i = 0; i < op.length; i++) {
                          // lowercase comparison for case-insensitivity
                          (op[i].value.toLowerCase() == json[j].IdRol)
                              ? op[i].disabled = true
                              : console.log (json[j].IdRol);

                      }
                  }



              });

      });
  </script>

</html>
