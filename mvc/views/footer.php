<style type="text/css">
    .modal-body-licence{
        height: 250px;
        overflow-y: auto;
        padding: 2%;
    }

    @media (min-height: 500px) {
        .modal-body-licence { height: 400px; }
    }

    @media (min-height: 800px) {
        .modal-body-licence { height: 600px; }
    }
</style>
<footer class="main-footer">
    <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>SIGCO</a>.</strong>
    Licencia de uso individual (<a href="#myModal" data-toggle="modal">ver licencia</a>)
</footer>
<div class="modal fade modal-xlg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #3c8dbc;color: white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Licencia de uso SIGCO</h4>
            </div>
            <div class="modal-body-licence">
                <?php
                $licencia=fopen('../utilities/licence','r');
                while(!feof($licencia)){
                    echo fgets($licencia);
                }
                fclose($licencia);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-blue-gradient" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalPass" tabindex="-1"
     role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title text-center" id="modal-register-label">Cambiar contraseña</h3>
            </div>
            <div class="modal-body">
                <p><div id="rtaFinal">Valide la información para continuar</div></p>
                <div id="divTodo">
                    <form role="form" id="changeForm" action="../controllers/EmpleadoController.php"
                          method="post" class="registration-form">
                        <div class="row">
                            <div class="col-sm-5">
                                <label for="userChange">Usuario actual*</label>
                                <input type="text" name="userChange" id="userChange" placeholder="Usuario" class="form-control" required tabindex="1">
                            </div>
                            <div class="col-sm-5">
                                <label for="passChange">Contraseña actual*</label>
                                <input type="password" name="passChange" id="passChange" placeholder="Contraseña" class="form-control" required tabindex="2">
                            </div>
                            <div class="col-sm-2">
                                <label>Verificar</label>
                                <button class="btn bg-yellow-gradient btn-block" type="button" id="valida" onclick="javascript:verificarUser();">
                                    <i class="fa fa-refresh"></i> </button>
                            </div>
                        </div>
                        <div class="row" id="divi" style="display: none">
                            <div class="col-sm-12">
                                <p>
                                <div id="rta"></div>
                                </p>
                            </div>
                        </div>
                        <div class="row" id="nuevoPass" style="display: none">
                            <div class="col-sm-5">
                                <label for="newPass1">Nueva contraseña*</label>
                                <input type="password" name="newPass1" id="newPass1" placeholder="Nueva contraseña" onkeyup="javascript:validaAnterior();" class="form-control" required tabindex="5">
                            </div>
                            <div class="col-sm-5">
                                <label for="newPass2">Repita la contraseña*</label>
                                <input type="password" name="newPass2" id="newPass2" placeholder="Verifique la nueva contraseña" onkeyup="javascript:validaIgualPass();" disabled="disabled" class="form-control" required tabindex="6">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" id="divFoot" style="display: none">
                <div id="rtaPass" class="pull-left"></div>
                <button name="guardar" type="button" onclick="javascript:cambiarPass();"
                        id="btnCambiarPass" class="btn bg-green-gradient" disabled="disabled" tabindex="8">
                    <i class="fa fa-key"></i>  Cambiar contraseña</button>
            </div>
        </div>
    </div>
</div>

<script language="javascript">
    var change=0;
    function verificarUser(){
        var div=document.getElementById('divi');
        var nuevoPass=document.getElementById('nuevoPass');
        var divFoot=document.getElementById('divFoot');
        var user = document.getElementById('userChange').value;
        var pass = document.getElementById('passChange').value;
        if(user!=''&&pass!=''){
            $.ajax({
                type:'POST',
                url:'../controllers/EmpleadoController.php',
                data:('userChange='+user+'&passChange='+pass),
                success:function(respuesta){
                    if(respuesta=='1'){
                        document.getElementById('userChange').disabled=true;
                        document.getElementById('passChange').disabled=true;
                        document.getElementById('valida').disabled=true;
                        div.style.display='block';
                        $('#rta').html('<p class="alert alert-success"><i class="fa fa-check"></i> Datos correctos. Ahora puede cambiar la contraseña:</p>');
                        nuevoPass.style.display='block';
                        divFoot.style.display='block';
                    }else{
                        nuevoPass.style.display='none';
                        divFoot.style.display='none';
                        document.getElementById('userChange').value='';
                        document.getElementById('passChange').value='';
                        div.style.display='block';
                        $('#rta').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> '+respuesta+'</p>');
                    }
                }
            })
        }else{
            div.style.display='block';
            $('#rta').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Debe diligenciar los campos para continuar.</p>');
        }
    }
    function validaAnterior(){
        var pass0=document.getElementById('passChange').value;
        var pass1=document.getElementById('newPass1').value;
        if(pass1!=''){
            if(pass1==pass0){
                $('#rtaPass').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> La nueva contraseña debe ser diferente de la actual</p>');
                $('#btnCambiarPass').prop('disabled',true);
                $('#newPass2').prop('disabled', true);
                document.getElementById('newPass2').value='';
            }else{
                $('#rtaPass').html('');
                $('#newPass2').prop('disabled', false);
            }
        }else{
            $('#rtaPass').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Debe diligenciar los campos para continuar.</p>');
        }
    }
    function validaIgualPass(){
        var pass0=document.getElementById('passChange').value;
        var pass1=document.getElementById('newPass1').value;
        var pass2=document.getElementById('newPass2').value;
        if(pass1==pass2&&pass2!=pass0&&pass1!=''&&pass2!=''){
            $('#rtaPass').html('');
            $('#btnCambiarPass').prop('disabled',false);
        }else{
            $('#rtaPass').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> La contraseña debe coincidir.</p>');
            $('#btnCambiarPass').prop('disabled',true);
        }
    }
    function cambiarPass(){
        var newPass=document.getElementById('newPass1').value;
        var divTodo=document.getElementById('divTodo');
        var divFoot=document.getElementById('divFoot');
        $.ajax({
            type:'POST',
            url:'../controllers/EmpleadoController.php',
            data:('newPass='+newPass+'&cambia=cambia'),
            success:function(respuesta){
                borraTodo();
                if(respuesta=='1'||respuesta=="true"||respuesta==true){
                    divTodo.style.display='none';
                    change=1;
                    $('#rtaFinal').html('<p class="alert alert-success"><i class="fa fa-check"></i> La contraseña ha sido actualizada. Debe iniciar sesión nuevamente.</p>');
                    divFoot.style.display='block';
                    $('#btnCambiarPass').prop('disabled',false);
                    $('#btnCambiarPass').html('<i class="fa fa-sign-out"></i> Aceptar');
                    document.getElementById('btnCambiarPass').onclick=null;
                    $('#btnCambiarPass').click(function(){
                        $('#modalPass').modal('hide');
                    });
                }else{
                    $('#rtaFinal').html('<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> '+respuesta+'</p>');
                }
            }
        })
    }
    function borraTodo(){
        var nuevoPass=document.getElementById('nuevoPass');
        var divFoot=document.getElementById('divFoot');
        var div=document.getElementById('divi');
        document.getElementById('userChange').value='';
        document.getElementById('passChange').value='';
        document.getElementById('newPass1').value='';
        document.getElementById('newPass2').value='';
        $('#rta').html('');
        $('#rtaPass').html('');
        nuevoPass.style.display='none';
        divFoot.style.display='none';
        div.style.display='none';
        $('#userChange').prop('disabled', false);
        $('#passChange').prop('disabled', false);
        $('#newPass2').prop('disabled', true);
        $('#valida').prop('disabled', false);
    }
    $('#modalPass').on('hidden.bs.modal', function (e) {
        borraTodo();
        if(change==1){
            window.open('../../login.php?cerrar=true','_self');
        }
    })
</script>