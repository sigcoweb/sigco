<?php

class gestionCambiarPass
{
    public function enviarPas($pass, $mail){
        include_once 'mailgun-php-master/vendor/autoload.php';
        $mg = new \Mailgun\Mailgun('key-75d789aec486967c931a55c2c37b747b');
        $domain = "sandboxb79bd6f71f1f41f1801ea07bf828aeed.mailgun.org";
        $result = $mg->sendMessage($domain, array(
            'from' =>  'Admin SIGCO <admin@sigco.com>',
            'to' => $mail,
            'subject' => 'Solicitud de cambio de contraseña',
            'html' => '<div style="min-height:100%!important;width:100%!important;background:#eeeeee;margin:0;padding:0" bgcolor="#EEEEEE">

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
        <tbody><tr>
            <td align="center" valign="top">
                <table align="center" border="0" cellspacing="0" width="600" style="width:600px!important;min-width:600px!important;border-collapse:collapse!important">
                    <tbody><tr>
                        <td align="center" valign="top">

                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="text-align:center!important;background-image:url("")!important;background-repeat:no-repeat!important;background-position:center center!important">
                         <img src="http://s22.postimg.org/52ex74b1d/logo.png" width="172" height="50" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px auto 20px;border:0" class="CToWUd">
                        </td>
                    </tr>



                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="overflow:hidden;border-radius:4px;max-width:600px!important;min-width:600px!important;border-collapse:collapse!important;text-align:center;background:#ffffff" bgcolor="#FFFFFF">
                                <tbody><tr>
                                    <td valign="top" bgcolor="#00C2E0" style="max-width:100%!important;background:#a21300 url("http://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/9d93e995-abb6-4951-b345-1472b6fef71c.jpg") no-repeat top center;padding:20px 0 0">
                                    <h1 style="font-family:Helvetica;font-size:36px;font-style:normal;font-weight:bold;text-align:center;margin:55px auto 20px" align="center"><font color="#ffffff">Hola '.$mail.' ,</font></h1>
                                    <h2 style="font-size:28px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:0px auto 25px"><font color="#FFFFFF">SIGCO recibió su solicitud de restaurar contraseña </font></h2>
                                    </td>
                                </tr>


                                <tr>
                                    <td style="text-align:left" align="left">
                                        <img src="https://juandavidruiz.files.wordpress.com/2015/04/clave-de-seguridad1.jpg" align="left" width="150" height="150" style="display:inline-block;min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:40px;border:0;border-radius: 50%" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 228.5px; top: 536px;"><div id=":1a2" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                        <p style="text-align:left;font-size:22px;line-height:28px;margin:20 auto 10px;padding:20px 40px 0 0" align="left"><font color="#4d4d4d"> Su nueva contraseña es: </p></font>
                                        <table border="0" cellpadding="14" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:24px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:#61bd4f;margin:10px 0 20px" align="center" bgcolor="#61BD4F">
                                            <tbody><tr>
                                                <td align="center" valign="middle">
                                                    <label href="#" style="text-decoration:none;padding:10px" target="_blank"><font color="white">'.$pass.' </font></label>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#A86CC1">


                                        <h2 style="font-size:16px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:40px auto 0"><font color="#ffffff">Si usted no solicito una nueva contraseña, favor ponerse en contacto con: admin@sigco.com</font></h2>

                                        <table border="0" cellpadding="10" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:20px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:rgba(255,255,255,0.2);margin:0 auto 50px;border:2px solid #ffffff" align="center" bgcolor="rgba(255,255,255,0.2)">
                                            <tbody><tr>
                                                <td align="center" valign="middle">
                                                    <a href="http://mapcompanysas.com/" style="text-decoration:none;padding:10px" target="_blank"><font color="white">visite nuetra página web →</font></a>


                                                </td>
                                            </tr>
                                            </tbody></table>

                                    </td>
                                </tr>


                                </tbody></table>
                        </td>
                    </tr>



                    <tr>
                        <td align="center" valign="top">
                            <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" style="border-collapse:collapse!important;max-width:100%!important">
                                <tbody><tr>
                                    <td valign="top">


                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse:collapse!important">


                                            <tbody><tr>
                                                <td valign="top">
                                                    <br>
                                                    <div style="color:#707070;font-family:Arial;font-size:12px;line-height:125%;text-align:center;max-width:100%!important" align="center">

                                                        <em>Copyright © 2016 <span class="il">sigcoweb</span>  All rights reserved.</em>
                                                        <br>
                                                    </div>
                                                    <br>
                                                </td>

                                            </tr>
                                            </tbody></table>


                                    </td>
                                </tr>
                                </tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    &nbsp;

    <img src="https://ci6.googleusercontent.com/proxy/2uDvcVf13UcX3KBFMOGvsfy8Ej4tBzBZ-VCBUBK8y_IlIdj6tthi-ozaR4rAsdx8rDXRXjy1fLAl658hgy82lL0vhBQjC362MY7AqtWvKjvaz15PAi1cYsCSX3Au9yU0R6yQu6dL7avlyhMyMOF9Eb_dw8n1FdugnGMfPeEvQYSXSsSvg7TrbJC8DmVQRdXix5_i=s0-d-e1-ft#http://em.trello.com/e/o/eyJlbWFpbF9pZCI6Ik5ERXpOVEk2RndGWDJ3SmtBQUp6QUJjY2JRZ2FBVkpwVXZuTEZVVVlWckpjMWdGdE9qSTRNVFl5TUFBPSJ9" class="CToWUd"><div class="yj6qo"></div><div class="adL">
    </div></div>'));
        return $result;
    }

}