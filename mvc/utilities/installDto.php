<?php

class installDto
{
    public function existsConection($host, $baseName, $userName, $userPass){
        if($host==''||$baseName==''||$userName==''){
            $mensaje='No hay datos completos para la conexión.';
        }else{
            try{
                $conn = new PDO("mysql:host=".$host.";dbname=".$baseName."", $userName, $userPass);
                $conn->setAttribute(PDO:: ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $mensaje=1;
            }catch(Exception $ex){
                $mensaje=$ex->getMessage();
            }
        }
        $conn=null;
        return $mensaje;
    }

    public function countTables($host, $baseName, $userName, $userPass){
        if($host==''||$baseName==''||$userName==''){
            $mensaje='No hay datos completos para la conexión.';
        }else{
            try{
                $conn = new PDO("mysql:host=".$host.";dbname=".$baseName."", $userName, $userPass);
                $conn->setAttribute(PDO:: ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $mensaje=1;
            }catch(Exception $ex){
                $mensaje=$ex->getMessage();
            }
        }
        if($mensaje==1&&isset($conn)){
            try {
                $conn->beginTransaction();
                $query = $conn->prepare("SELECT COUNT(*) as cantidad
                                    FROM information_schema.tables
                                WHERE table_schema = '".$baseName."'");
                $query->execute();
                $conn->commit();
                $cantidad = $query->fetch();
                $mensaje = $cantidad['cantidad'];
            }catch(Exception $ex){
                $mensaje='Error: '.$ex->getMessage();
            }
        }else{
            $mensaje=0;
        }
        $conn=null;
        return $mensaje;
    }

    public function installIni($host, $baseName, $userBase, $userPass){
        try{
            $copia='';
            $archivo='../utilities/config.php';
            if(is_writable($archivo)){
                $file=fopen($archivo,'wb');
                fwrite($file, '<?php'.PHP_EOL);
                fwrite($file, '$installed=1;'.PHP_EOL);
                fwrite($file, '$host='.'"'.$host.'";'.PHP_EOL);
                fwrite($file, '$baseName='.'"'.$baseName.'";'.PHP_EOL);
                fwrite($file, '$userName='.'"'.$userBase.'";'.PHP_EOL);
                fwrite($file, '$userPass='.'"'.$userPass.'";'.PHP_EOL);
                fclose($file);
                $mensaje=1;
            }else{
                $mensaje=0;
            }
        }catch(Exception $ex){
            $mensaje.=$ex->getMessage();
        }
        return $mensaje;
    }

    public function installFail(){
        try{
            $original='../utilities/configRead';
            $copiaOriginal=file_get_contents($original,'rbt');
            $archivo='../utilities/config.php';
            if(is_readable($original)&&is_writable($archivo)){
                $file=fopen($archivo,'wb');
                fwrite($file, $copiaOriginal);
                fclose($file);
                $mensaje=1;
            }else{
                $mensaje=0;
            }
        }catch(Exception $ex){
            $mensaje.=$ex->getMessage();
        }
        return $mensaje;
    }

    public function getArrayDataConn(){
        $arrayDataConn=array();
        $archivo=fopen('../utilities/config.php','r');
        $c=0;
        while(!feof($archivo)){
            $linea=fgets($archivo);
            if($c==2){
                $arra=explode('"',$linea);
                $arrayDataConn['host']=$arra[1];
            }
            if($c==3){
                $arra=explode('"',$linea);
                $arrayDataConn['baseName']=$arra[1];
            }
            if($c==4){
                $arra=explode('"',$linea);
                $arrayDataConn['userName']=$arra[1];
            }
            if($c==5){
                $arra=explode('"',$linea);
                $arrayDataConn['userPass']=$arra[1];
            }
            $c++;
        }
        fclose($archivo);
        return $arrayDataConn;
    }


}
