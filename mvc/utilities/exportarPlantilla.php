<?php
include_once '../facades/ImportarFacade.php';
$impor = new ImportarFacade();
header("Content-type: application/vnd.ms-excel; name='excel'; charset=utf-8");
header("Content-Disposition: filename=plantilla-".$_GET['tabla'].'-'.time().".xls");
header("Pragma: no-cache");
header("Expires: 0");
$tab=$impor->listarCampos($_GET['tabla']);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo de la tabla <?php echo $_GET['tabla']; ?></title>
    </head>
<body>
<table border="1">
    <thead>
    <?php
    foreach($tab as $camp) {
        echo "<th>".$camp['Field']."</th>";
    };
    ?>
    </thead>
    <tbody>
    <tr>
        <?php
        foreach($tab as $camp){
            echo '<td>'.$impor->ejemploDato($camp['Type']).'</td>';
        };
        ?>
    </tr>
    </tbody>
</table>
</body>
</html>