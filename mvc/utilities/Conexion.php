<?php
include_once 'installDto.php';
class Conexion
{
    public static function getConexion(){
        $conn=null;
        $dtoInstall=new installDto();
        $arrayCon=$dtoInstall->getArrayDataConn();
        try {
            $conn = new PDO("mysql:host=".$arrayCon['host'].";dbname=".$arrayCon['baseName']."", $arrayCon['userName'], $arrayCon['userPass'], array(PDO::MYSQL_ATTR_LOCAL_INFILE => 'TRUE', PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $conn->setAttribute(PDO:: ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex  ){
            echo 'Error de conexión a la base de datos: '.$ex->getMessage();
        }
        return $conn;
    }
}