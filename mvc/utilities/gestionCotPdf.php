<?php
include_once '../facades/FacadeCotizaciones.php';
require 'fpdf/fpdf.php';

class gestionCotPdf
{
    public function guardarCot($numeroCot){
        $mensaje=array();
        try{
            $pdf = new FPDF("P");
            $facade = new FacadeCotizaciones();
            $resultados = $facade->buscarCotizacion($numeroCot);
            $pdf->AddPage();
            $pdf->SetFont('Arial', '', 12);
            switch ($resultados[0]['empresaCotiza']) {
                case "MAP":
                    $pdf->Image('../images/map.jpg', 0, 0, 218);
                    break;

                case "BIO":
                    $pdf->Image('../images/bio.jpg', 0, 0, 218);
                    break;

                case "MAT":
                    $pdf->Image('../images/mat.jpg', 0, 0, 218);
                    break;
            }
            $pdf->Image('../views/cotizacion.new.png', 0, 0, 218);
            $pdf->Ln(25);
            $pdf->Cell(70, 0, utf8_decode('SEÑOR(ES): ' . $resultados[0]['RazonSocial']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(115, 0, utf8_decode('DIRECCIÓN: ' . $resultados[0]['Direccion']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(130, 0, utf8_decode('TELÉFONO: ' . $resultados[0]['Telefono']), 0, 0, 'L');
            $pdf->SetFont('Arial', '', 16);
            $pdf->SetTextColor(220, 20, 60);
            $pdf->Cell(110, 0, utf8_decode('Cotización Nº: ' . $resultados[0]['IdCotizacion']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(156, 0, utf8_decode('E-MAIL: ' . $resultados[0]['EmailCliente']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(115, 0, utf8_decode('CIUDAD: ' . $resultados[0]['NombreLugar']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(60, 0, utf8_decode('NIT: ' . $resultados[0]['Nit']), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(60, 0, utf8_decode('ASESOR TÉCNICO COMERCIAL: ' . ($resultados[0]['Nombres'] . ' ' . $resultados[0]['Apellidos'])), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(60, 0, utf8_decode('EMAIl: ' . $resultados[0]['EmailPersona']), 0, 0, 'L');
            $pdf->Ln(25);
            $totalsub = 0;
            $totaliva = 0;
            $totalDescuento = 0;
            $pdf->Ln(15);
            $totalProductos = 0;
            $pdf->SetFont('Arial', '', 10);
            $prod = 0;
            $lin = 0;
            foreach ($resultados as $datos) {
                if ($lin > 13) {
                    $pdf->AddPage();
                    $pdf->SetFont('Arial', '', 12);
                    switch ($resultados[0]['empresaCotiza']) {
                        case "MAP":
                            $pdf->Image('../images/map.jpg', 0, 0, 218);
                            break;
                        case "BIO":
                            $pdf->Image('../images/bio.jpg', 0, 0, 218);
                            break;
                        case "MAT":
                            $pdf->Image('../images/mat.jpg', 0, 0, 218);
                            break;
                    }
                    $pdf->Image('cotizacion.new.png', 0, 0, 218);
                    $pdf->Ln(25);
                    $pdf->Cell(70, 0, utf8_decode('SEÑOR(ES): ' . $resultados[0]['RazonSocial']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(115, 0, utf8_decode('DIRECCIÓN: ' . $resultados[0]['Direccion']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(130, 0, utf8_decode('TELÉFONO: ' . $resultados[0]['Telefono']), 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 16);
                    $pdf->SetTextColor(220, 20, 60);
                    $pdf->Cell(110, 0, utf8_decode('Cotización Nº: ' . $resultados[0]['IdCotizacion']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->SetFont('Arial', '', 12);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->Cell(156, 0, utf8_decode('E-MAIL: ' . $resultados[0]['EmailCliente']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(115, 0, utf8_decode('CIUDAD: ' . $resultados[0]['NombreLugar']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(60, 0, utf8_decode('NIT: ' . $resultados[0]['Nit']), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(60, 0, utf8_decode('ASESOR TÉCNICO COMERCIAL: ' . ($resultados[0]['Nombres'] . ' ' . $resultados[0]['Apellidos'])), 0, 0, 'L');
                    $pdf->Ln(5);
                    $pdf->Cell(60, 0, utf8_decode('EMAIl: ' . $resultados[0]['EmailPersona']), 0, 0, 'L');
                    $pdf->Ln(25);
                    $pdf->Ln(15);
                    $pdf->SetFont('Arial', '', 10);
                    $prod = 0;
                    $lin = 0;
                }
                $totalsub = $totalsub + ($datos['TotalDetalle']);
                $pdf->Cell(26, 8, $datos['IdProducto'], 0, 0, 'L');
                $palabra = "";
                $palabra2 = "";
                if (strlen($datos['NombreProducto']) > 45) {
                    $test = str_split($datos['NombreProducto']);
                    for ($i = 0; $i < 45; $i++) {
                        $palabra = $palabra . $test[$i];
                    }
                    $pdf->Cell(85, 8, utf8_decode($palabra . '-'), 0, 0, 'L');
                    $pdf->Ln(5.5);
                    $restante = strlen($datos['NombreProducto']) - 45;
                    $restante = $restante + 45;
                    for ($i = 45; $i < $restante; $i++) {
                        $palabra2 = $palabra2 . $test[$i];
                    }
                    $pdf->Cell(26, 8, "", 0, 0, 'L');
                    $pdf->Cell(85, 8, utf8_decode($palabra2), 0, 0, 'L');
                    $totalProductos++;
                    $lin = $lin + 2;
                } else {
                    $pdf->Cell(85, 8, utf8_decode($datos['NombreProducto']), 0, 0, 'L');
                    $lin++;
                }
                $pdf->Cell(10, 8, $datos['CantidadProductos'], 0, 0, 'L');
                $pdf->Cell(20, 8, $datos['Abreviatura'], 0, 0, 'C');
                $pdf->Cell(26, 8, '$' . number_format($datos['TotalDetalle'] / $datos['CantidadProductos']), 0, 0, 'R');
                $pdf->Cell(30, 8, '$' . number_format($datos['TotalDetalle']), 0, 0, 'R');
                $pdf->Ln(5.5);
                $totalProductos++;
                $prod++;
            }
            if ($lin < 14) {
                $diferencia = 14 - $lin;
                for ($i = 1; $i < $diferencia - 1; $i++) {
                    $pdf->Cell(20, 8, "", 0, 0, 'L');
                    $pdf->Cell(107, 8, "", 0, 0, 'L');
                    $pdf->Cell(10, 8, "", 0, 0, 'L');
                    $pdf->Cell(18, 8, "", 0, 0, 'C');
                    $pdf->Cell(22, 8, "", 0, 0, 'L');
                    $pdf->Cell(60, 8, "", 0, 0, 'L');
                    $pdf->Ln(5.5);
                }
                $pdf->Ln(20);
            } else {
                $pdf->Ln(10);
            }
            $pdf->Cell(197, 0, "$" . number_format($resultados[0]['ValorTotalCotizacion']), 0, 0, 'R');
            date_default_timezone_set('America/Bogota');
            $mensaje['archivo'] = '../documents/cotNo.' . $numeroCot . 'de' . date("d-m-y-g-i-sa", time()) . '.pdf';
            $pdf->Output($mensaje['archivo']);
            $mensaje['resultado']=1;
        }catch(Exception $ex){
            $mensaje['archivo']=$ex->getMessage();
            $mensaje['resultado']=0;
        }
        return $mensaje;
    }

    public function enviarCot($archivo, $toMail, $fromName, $fromMail){
        include_once 'mailgun-php-master/vendor/autoload.php';
        $mg = new \Mailgun\Mailgun('key-75d789aec486967c931a55c2c37b747b');
        $domain = "sandboxb79bd6f71f1f41f1801ea07bf828aeed.mailgun.org";
        $result = $mg->sendMessage($domain, array(
            'from' =>  $fromName .'<' . $fromMail. '>',
            'to' => $toMail,
            'subject' => 'Nueva cotización generada, Grupo MAT',
            'html' => '
<div class="wrapper" style="background-color: #f0eee7;">
      <table style="border-collapse: collapse;table-layout: fixed;color: #b3b3b3;font-family: &quot;PT Sans&quot;,&quot;Trebuchet MS&quot;,sans-serif;" align="center">
        <tbody><tr>
          <td class="preheader__snippet" style="padding: 10px 0 5px 0;vertical-align: top;width: 300px;">

          </td>
          <td class="preheader__webversion" style="text-align: right;padding: 10px 0 5px 0;vertical-align: top;width: 300px;">

          </td>
        </tr>
      </tbody></table>
      <table class="header" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;" align="center">
        <tbody><tr>
          <td style="padding: 0;width: 600px;">
            <div class="header__logo emb-logo-margin-box" style="font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #41637e;font-family: Avenir,sans-serif;">
              <div class="logo-center" style="font-size:0px !important;line-height:0 !important;" align="center" id="emb-email-header"><img style="height: auto;width: 100%;border: 0;max-width: 212px;"
              src="http://sigco.corfedes.org/mvc/images/producbio.jpg" alt="" width="212" height="214"></div>
            </div>
          </td>
        </tr>
      </tbody></table>
      <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center">
        <tbody><tr>
          <td class="column" style="padding: 0;text-align: left;vertical-align: top;color: #61606c;font-size: 14px;line-height: 21px;font-family: &quot;PT Serif&quot;,Georgia,serif;width: 600px;">

        <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="display: block;border: 0;max-width: 900px;"
          src="http://www.stellartesthouse.com/wp-content/uploads/2015/07/qc-testing.jpg" alt="" width="600" height="400">
        </div>

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">
      <div style="line-height:20px;font-size:1px">&nbsp;</div>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;">

<h1 class="size-30" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #b59859;font-size: 30px;line-height: 38px;text-align: center;">
¡Tenemos muchos más productos para usted!
</h1><h2 class="size-24" style="Margin-top: 20px;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #555;font-size: 24px;line-height: 32px;font-family: &quot;PT Sans&quot;,&quot;Trebuchet MS&quot;,sans-serif;text-align: center;">
Respetados señores
</h2><p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
En calidad de su asesor técnico y comercial me permito adjuntar a ustedes la cotización de nuestra línea de productos según solicitud.
</p><p class="size-16" style="Margin-top: 20px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
Estaremos atentos ante cualquier
comentario.</p>
<p class="size-16" style="Margin-top: 20px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
Atentamente,
</p>
<p class="size-16" style="Margin-top: 20px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
'.strtoupper($fromName).'<br>
Servicio especializado <br>
GRUPO MAT
</p>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;">
      <div class="btn btn--flat" style="Margin-bottom: 20px;text-align: center;">
        <!--[if !mso]--><a style="border-radius: 4px;display: inline-block;font-weight: bold;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #cca95e;font-family: \'PT Serif\', Georgia, serif;font-size: 14px;line-height: 24px;padding: 12px 35px;" href="http://sigco.corfedes.org" data-width="92" target="_blank">Ir al sitio web</a><!--[endif]-->
      <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="http://as.createsend1.com/t/i-l-hrtujdt-l-r/" style="width:162px" arcsize="9%" fillcolor="#CCA95E" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,11px,0px,11px"><center style="font-size:14px;line-height:24px;color:#FFFFFF;font-family:Georgia,serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">Ir al sitio web</center></v:textbox></v:roundrect><![endif]--></div>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;">
      <div class="divider" style="display: block;font-size: 2px;line-height: 2px;width: 40px;background-color: #c9c2a8;Margin-left: 260px;Margin-right: 260px;Margin-bottom: 20px;">&nbsp;</div>
    </div>

            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
    </div>

          </td>
        </tr>
      </tbody></table>

      <div style="font-size: 20px;line-height: 20px;mso-line-height-rule: exactly;">&nbsp;</div>

      <table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto;border-spacing: 0;width: 600px;" align="center">
        <tbody><tr>
          <td style="padding: 0 0 40px 0;">
            <table class="footer__right" style="border-collapse: collapse;table-layout: auto;border-spacing: 0;" align="right">
              <tbody><tr>
                <td class="footer__inner" style="padding: 0;">
                </td>
              </tr>
            </tbody></table>
            <table class="footer__left" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b3b3b3;font-family: &quot;PT Sans&quot;,&quot;Trebuchet MS&quot;,sans-serif;width: 400px;">
              <tbody><tr>
                <td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;">

                  <div>
                    <div>Grupo MAT Colombia</div>
                  </div>
                  <div class="footer__permission" style="Margin-top: 18px;">
                    <div>Usted ha recibido esta cotización de un remitente seguro y de un sistema verificado previamente. Si usted no ha solicitado esta información por favor informenos.</div>
                  </div>
                </td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      </tbody></table>
    </div>
    '),
            array(
                'attachment' => array($archivo)
            ));
        return $result;
    }

    public function eliminarCot($archivo){
        try{
            unlink($archivo);
            $mensaje=1;
        }catch(Exception $ex){
            $mensaje='Error eliminando el archivo: '.$ex->getMessage();
        }
        return $mensaje;
    }

}