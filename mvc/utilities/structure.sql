SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `actividadesempresas`;
CREATE TABLE `actividadesempresas` (
  `IdActividad` int(11) NOT NULL,
  `NombreActividad` varchar(200) NOT NULL,
  `PagaIva` enum('Si','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `areascliente`;
CREATE TABLE `areascliente` (
  `nombreAreas` varchar(50) NOT NULL,
  `nitClienteAreas` bigint(20) NOT NULL,
  `nombreContactoAreas` varchar(45) NOT NULL,
  `telefonoArea` bigint(20) NOT NULL,
  `emailArea` varchar(45) NOT NULL,
  `observacionesArea` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aromas`;
CREATE TABLE `aromas` (
  `idAroma` int(11) NOT NULL,
  `aroma` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aromasproductos`;
CREATE TABLE `aromasproductos` (
  `idAromaAromasProd` int(11) NOT NULL,
  `idProductoAromasProd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `categoriasproductos`;
CREATE TABLE `categoriasproductos` (
  `IdCategoria` int(11) NOT NULL,
  `NombreCategoria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `clasificaciones`;
CREATE TABLE `clasificaciones` (
  `IdClasificacion` int(11) NOT NULL,
  `NombreClasificacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `IdCliente` int(11) NOT NULL,
  `CedulaCliente` bigint(20) NOT NULL,
  `CedulaEmpleadoClientes` bigint(20) NOT NULL,
  `Nit` bigint(20) NOT NULL,
  `RazonSocial` varchar(100) NOT NULL,
  `nombreComercial` varchar(50) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Telefono` bigint(20) NOT NULL,
  `EmailCliente` varchar(50) NOT NULL,
  `IdLugarCliente` int(11) NOT NULL,
  `IdTipoCliente` int(11) NOT NULL,
  `IdActividadCliente` int(11) NOT NULL,
  `IdClasificacionCliente` int(11) NOT NULL,
  `estadoCliente` enum('Activo','Inactivo') NOT NULL,
  `diaCierreFacturacion` int(2) NOT NULL DEFAULT '25',
  `plazoPago` int(3) NOT NULL DEFAULT '30'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `colores`;
CREATE TABLE `colores` (
  `idColor` int(11) NOT NULL,
  `color` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `coloresproductos`;
CREATE TABLE `coloresproductos` (
  `idColorColoresProd` int(11) NOT NULL,
  `idProductoColoresProd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `concentraciones`;
CREATE TABLE `concentraciones` (
  `idConcentracion` int(11) NOT NULL,
  `concentracion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `concentracionesproductos`;
CREATE TABLE `concentracionesproductos` (
  `idConcentracionConcProd` int(11) NOT NULL,
  `idProductoConcProd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cotizaciones`;
CREATE TABLE `cotizaciones` (
  `IdCotizacion` int(11) NOT NULL,
  `EstadoCotizacion` enum('Vigente','Cancelada','Pedido') NOT NULL,
  `FechaCreacionCotizacion` datetime NOT NULL,
  `ValorTotalCotizacion` bigint(20) NOT NULL,
  `ObservacionesCotizacion` varchar(200) NOT NULL,
  `ValorDescuento` bigint(20) NOT NULL,
  `CedulaEmpleadoCotizaciones` bigint(20) NOT NULL,
  `NitClienteCotizaciones` bigint(20) NOT NULL,
  `empresaCotiza` enum('MAT','MAP','BIO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE `departamentos` (
  `idDepartamento` int(11) NOT NULL,
  `nombreDepartamento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `detallescotizacion`;
CREATE TABLE `detallescotizacion` (
  `IdCotizacionDetallesCotizacion` int(11) NOT NULL,
  `IdProductoDetallesCotizacion` varchar(50) NOT NULL,
  `CantidadProductos` int(11) NOT NULL,
  `TotalDetalle` bigint(20) NOT NULL,
  `idPresentacionFacturacion` int(11) NOT NULL,
  `idAromaDetalles` int(11) NOT NULL,
  `idColorDetalles` int(11) NOT NULL,
  `idConcentracionDetalles` int(11) NOT NULL,
  `idPresentacionDetalles` int(11) NOT NULL,
  `ValorVentaProducto` bigint(20) NOT NULL,
  `fechaProduccion` datetime NOT NULL,
  `cantidadFacturacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `documentos`;
CREATE TABLE `documentos` (
  `idDocumento` int(11) NOT NULL,
  `rutaDocumento` varchar(45) NOT NULL,
  `TipoDocumento` enum('Certificado','FichaT','FichaS') NOT NULL,
  `idCategoriaDocumentos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `empleados`;
CREATE TABLE `empleados` (
  `CedulaEmpleado` bigint(20) NOT NULL,
  `Cargo` varchar(50) NOT NULL,
  `idLugarEmpleado` int(11) NOT NULL,
  `fechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `gestiones` (
  `IdGestion` int(11) NOT NULL,
  `CedulaEmpleadoGestiones` bigint(20) NOT NULL,
  `NitClienteGestiones` bigint(20) NOT NULL,
  `FechaProgramada` datetime NOT NULL,
  `EstadoGestiones` enum('Pendiente','Cancelada','Realizada') NOT NULL,
  `TipoGestiones` enum('Asesoría','Capacitación') NOT NULL,
  `Asistentes` int(11) NOT NULL,
  `ObservacionesGestiones` varchar(200) NOT NULL,
  `ResultadoGestiones` varchar(200) NOT NULL,
  `LugarGestiones` varchar(50) NOT NULL,
  `creado` timestamp NULL DEFAULT NULL,
  `Actualizado` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `impuestos`;
CREATE TABLE `impuestos` (
  `IdIva` int(11) NOT NULL,
  `PorcentajeIva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `lugares`;
CREATE TABLE `lugares` (
  `IdLugar` int(11) NOT NULL,
  `NombreLugar` varchar(50) NOT NULL,
  `idDepartamentoLugar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `metas`;
CREATE TABLE `metas` (
  `IdMeta` int(11) NOT NULL,
  `Tipo` enum('Ventas','Visitas') NOT NULL,
  `CantidadValor` bigint(20) NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaFin` date NOT NULL,
  `estadoMeta` enum('Vigente','Vencida') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `metasusuarios`;
CREATE TABLE `metasusuarios` (
  `CedulaMetasUsuarios` bigint(20) NOT NULL,
  `IdMetaMetasUsuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ordencompracliente`;
CREATE TABLE `ordencompracliente` (
  `idPedido` int(11) NOT NULL,
  `codigoOrdenCompraCliente` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `IdPedido` int(11) NOT NULL,
  `IdCotizacionPedidos` int(11) NOT NULL,
  `EstadoPedido` enum('Pendiente','Cancelado','Autorizado','Producción','Despachado','ProcesoDespacho') NOT NULL,
  `FechaElaboracionPedido` datetime NOT NULL,
  `ValorTotal` bigint(20) NOT NULL,
  `ObservacionesPedido` varchar(200) NOT NULL,
  `Remisionado` enum('Si','No') NOT NULL,
  `Facturado` enum('Si','No') NOT NULL,
  `Certificado` enum('Si','No') NOT NULL,
  `FichaTecnica` enum('Si','No') NOT NULL,
  `FichaSeguridad` enum('Si','No') NOT NULL,
  `nombrePuntoEntregaPedido` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos` (
  `IdPermiso` int(11) NOT NULL,
  `Url` varchar(100) NOT NULL,
  `NombrePagina` varchar(50) NOT NULL,
  `IdCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permisoscategorias`;
CREATE TABLE `permisoscategorias` (
  `IdCategoria` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Icono` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permisosroles`;
CREATE TABLE `permisosroles` (
  `IdRolPermisosRoles` int(11) NOT NULL,
  `IdPermisoPermisosRoles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `personas`;
CREATE TABLE `personas` (
  `IdPersona` int(11) NOT NULL,
  `CedulaPersona` bigint(20) NOT NULL,
  `Nombres` varchar(50) NOT NULL,
  `Apellidos` varchar(50) NOT NULL,
  `EmailPersona` varchar(50) NOT NULL,
  `EstadoPersona` enum('Activo','Inactivo') NOT NULL,
  `Contrasenia` varchar(50) NOT NULL,
  `RutaImagenPersona` varchar(100) NOT NULL,
  `CelularPersona` bigint(20) NOT NULL,
  `FechaCreacionPersona` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `presentaciones`;
CREATE TABLE `presentaciones` (
  `IdPresentacion` int(11) NOT NULL,
  `NombrePresentacion` varchar(50) NOT NULL,
  `Abreviatura` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `IdProducto` varchar(50) NOT NULL,
  `NombreProducto` varchar(100) NOT NULL,
  `DescripcionProducto` varchar(100) NOT NULL,
  `IdIvaProductos` int(11) NOT NULL,
  `IdCategoriaProductos` int(11) NOT NULL,
  `EstadoProductos` enum('Activo','Inactivo') NOT NULL,
  `rutaImagen` varchar(100) NOT NULL,
  `FechaCreacionProducto` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `puntosentrega`;
CREATE TABLE `puntosentrega` (
  `nombrePuntoEntrega` varchar(45) NOT NULL,
  `direccionPuntoEntrega` varchar(45) NOT NULL,
  `nombreContactoPuntoEntrega` varchar(45) NOT NULL,
  `correoPuntoEntrega` varchar(45) NOT NULL,
  `telefonoPuntoEntrega` bigint(20) NOT NULL,
  `nitEmpresaPuntoEntrega` bigint(20) NOT NULL,
  `idLugarPuntoEntrega` int(11) NOT NULL,
  `observacionesPuntoEntrega` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `IdRol` int(11) NOT NULL,
  `NombreRol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rolesusuarios`;
CREATE TABLE `rolesusuarios` (
  `IdRolRolesUsuarios` int(11) NOT NULL,
  `CedulaRolesUsuarios` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `temas`;
CREATE TABLE `temas` (
  `IdTema` int(11) NOT NULL,
  `NombreTema` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tiposempresas`;
CREATE TABLE `tiposempresas` (
  `IdTipo` int(11) NOT NULL,
  `NombreTipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `valorpresentacion`;
CREATE TABLE `valorpresentacion` (
  `idpresentacion` int(11) NOT NULL,
  `idProducto` varchar(50) NOT NULL,
  `valorPresentacion` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `actividadesempresas`
ADD PRIMARY KEY (`IdActividad`);

ALTER TABLE `areascliente`
ADD PRIMARY KEY (`nombreAreas`,`nitClienteAreas`),
ADD KEY `fk_depart_clientes_idx` (`nitClienteAreas`);

ALTER TABLE `aromas`
ADD PRIMARY KEY (`idAroma`);

ALTER TABLE `aromasproductos`
ADD PRIMARY KEY (`idAromaAromasProd`,`idProductoAromasProd`),
ADD KEY `fk_producto_aroma_idx` (`idProductoAromasProd`);

ALTER TABLE `categoriasproductos`
ADD PRIMARY KEY (`IdCategoria`);

ALTER TABLE `clasificaciones`
ADD PRIMARY KEY (`IdClasificacion`);

ALTER TABLE `clientes`
ADD PRIMARY KEY (`IdCliente`),
ADD UNIQUE KEY `Nit_UNIQUE` (`Nit`),
ADD KEY `fk_Clientes_TiposEmpresas1_idx` (`IdTipoCliente`),
ADD KEY `fk_Clientes_ActividadesEmpresas1_idx` (`IdActividadCliente`),
ADD KEY `fk_Clientes_Clasificaciones1_idx` (`IdClasificacionCliente`),
ADD KEY `fk_Clientes_Lugares1_idx` (`IdLugarCliente`),
ADD KEY `fk_Clientes_Personas_idx` (`CedulaCliente`),
ADD KEY `fk_Clientes_Empleados_idx` (`CedulaEmpleadoClientes`);

ALTER TABLE `colores`
ADD PRIMARY KEY (`idColor`);

ALTER TABLE `coloresproductos`
ADD PRIMARY KEY (`idColorColoresProd`,`idProductoColoresProd`),
ADD KEY `fk_prouctos_colores_idx` (`idProductoColoresProd`);

ALTER TABLE `concentraciones`
ADD PRIMARY KEY (`idConcentracion`);

ALTER TABLE `concentracionesproductos`
ADD PRIMARY KEY (`idConcentracionConcProd`,`idProductoConcProd`),
ADD KEY `fk_produc_concentr_idx` (`idProductoConcProd`);

ALTER TABLE `cotizaciones`
ADD PRIMARY KEY (`IdCotizacion`),
ADD KEY `fk_Cotizaciones_Usuarios1_idx` (`CedulaEmpleadoCotizaciones`),
ADD KEY `fk_Cotizaciones_Cliente1_idx` (`NitClienteCotizaciones`);

ALTER TABLE `departamentos`
ADD PRIMARY KEY (`idDepartamento`);

ALTER TABLE `detallescotizacion`
ADD PRIMARY KEY (`IdCotizacionDetallesCotizacion`,`IdProductoDetallesCotizacion`,`CantidadProductos`,`TotalDetalle`,`idPresentacionFacturacion`,`idAromaDetalles`,`idColorDetalles`,`idConcentracionDetalles`,`idPresentacionDetalles`,`ValorVentaProducto`),
ADD KEY `fk_CotizacionDetalles_Cotizaciones1_idx` (`IdCotizacionDetallesCotizacion`),
ADD KEY `fk_CotizacionDetalles_Productos1_idx` (`IdProductoDetallesCotizacion`);


ALTER TABLE `documentos`
ADD PRIMARY KEY (`idDocumento`),
ADD KEY `fk_documentos_categ_idx` (`idCategoriaDocumentos`);

ALTER TABLE `empleados`
ADD PRIMARY KEY (`CedulaEmpleado`);

ALTER TABLE `gestiones`
ADD PRIMARY KEY (`IdGestion`),
ADD KEY `fk_Asesorias_Empresas1_idx` (`NitClienteGestiones`),
ADD KEY `fk_Asesorias_Usuarios1_idx` (`CedulaEmpleadoGestiones`);

ALTER TABLE `impuestos`
ADD PRIMARY KEY (`IdIva`);

ALTER TABLE `lugares`
ADD PRIMARY KEY (`IdLugar`),
ADD KEY `fk_dept_idx` (`idDepartamentoLugar`);

ALTER TABLE `metas`
ADD PRIMARY KEY (`IdMeta`);

ALTER TABLE `metasusuarios`
ADD PRIMARY KEY (`CedulaMetasUsuarios`,`IdMetaMetasUsuarios`),
ADD KEY `fk_MetasUsuario_Usuarios1_idx` (`CedulaMetasUsuarios`),
ADD KEY `fk_MetasUsuario_Metas1_idx` (`IdMetaMetasUsuarios`);

ALTER TABLE `ordencompracliente`
ADD PRIMARY KEY (`idPedido`);

ALTER TABLE `pedidos`
ADD PRIMARY KEY (`IdPedido`),
ADD UNIQUE KEY `IdCotizacion_UNIQUE` (`IdCotizacionPedidos`),
ADD KEY `fk_OrdenDeCompra_Cotizaciones1_idx` (`IdCotizacionPedidos`);

ALTER TABLE `permisos`
ADD PRIMARY KEY (`IdPermiso`),
ADD KEY `fk_PermisosCategorias_Categorias` (`IdCategoria`);

ALTER TABLE `permisoscategorias`
ADD PRIMARY KEY (`IdCategoria`);

ALTER TABLE `permisosroles`
ADD PRIMARY KEY (`IdRolPermisosRoles`,`IdPermisoPermisosRoles`),
ADD KEY `fk_RolesPermisos_Roles1_idx` (`IdRolPermisosRoles`),
ADD KEY `fk_RolesPermisos_Permisos1_idx` (`IdPermisoPermisosRoles`);

ALTER TABLE `personas`
ADD PRIMARY KEY (`IdPersona`),
ADD UNIQUE KEY `Cedula_UNIQUE` (`CedulaPersona`);

ALTER TABLE `presentaciones`
ADD PRIMARY KEY (`IdPresentacion`);

ALTER TABLE `productos`
ADD PRIMARY KEY (`IdProducto`),
ADD KEY `fk_Productos_CategoriaProducto1_idx` (`IdCategoriaProductos`),
ADD KEY `fk_Productos_Impuestos_idx` (`IdIvaProductos`);

ALTER TABLE `puntosentrega`
ADD PRIMARY KEY (`nombrePuntoEntrega`,`nitEmpresaPuntoEntrega`),
ADD KEY `fk_punto_cliente_idx` (`nitEmpresaPuntoEntrega`),
ADD KEY `fk_punto_lugar_idx` (`idLugarPuntoEntrega`);

ALTER TABLE `roles`
ADD PRIMARY KEY (`IdRol`);

ALTER TABLE `rolesusuarios`
ADD PRIMARY KEY (`IdRolRolesUsuarios`,`CedulaRolesUsuarios`),
ADD KEY `fk_RolUsuario_Roles1_idx` (`IdRolRolesUsuarios`),
ADD KEY `fk_RolUsiario_Personas_idx` (`CedulaRolesUsuarios`);

ALTER TABLE `temas`
ADD PRIMARY KEY (`IdTema`);

ALTER TABLE `tiposempresas`
ADD PRIMARY KEY (`IdTipo`);

ALTER TABLE `valorpresentacion`
ADD PRIMARY KEY (`idpresentacion`,`idProducto`),
ADD KEY `fk_valor_prod_idx` (`idProducto`);


ALTER TABLE `actividadesempresas`
MODIFY `IdActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `aromas`
MODIFY `idAroma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `categoriasproductos`
MODIFY `IdCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `clasificaciones`
MODIFY `IdClasificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `clientes`
MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `colores`
MODIFY `idColor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `concentraciones`
MODIFY `idConcentracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `cotizaciones`
MODIFY `IdCotizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `documentos`
MODIFY `idDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `gestiones`
MODIFY `IdGestion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `impuestos`
MODIFY `IdIva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `lugares`
MODIFY `IdLugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `metas`
MODIFY `IdMeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `pedidos`
MODIFY `IdPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `permisos`
MODIFY `IdPermiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `permisoscategorias`
MODIFY `IdCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `personas`
MODIFY `IdPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `presentaciones`
MODIFY `IdPresentacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `roles`
MODIFY `IdRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `temas`
MODIFY `IdTema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `tiposempresas`
MODIFY `IdTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `areascliente`
ADD CONSTRAINT `fk_depart_clientes` FOREIGN KEY (`nitClienteAreas`) REFERENCES `clientes` (`Nit`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `aromasproductos`
ADD CONSTRAINT `fk_aroma_producto` FOREIGN KEY (`idAromaAromasProd`) REFERENCES `aromas` (`idAroma`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_producto_aroma` FOREIGN KEY (`idProductoAromasProd`) REFERENCES `productos` (`IdProducto`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `clientes`
ADD CONSTRAINT `fk_Clientes_ActividadesEmpresas1` FOREIGN KEY (`IdActividadCliente`) REFERENCES `actividadesempresas` (`IdActividad`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Clientes_Clasificaciones1` FOREIGN KEY (`IdClasificacionCliente`) REFERENCES `clasificaciones` (`IdClasificacion`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Clientes_Empleados1` FOREIGN KEY (`CedulaEmpleadoClientes`) REFERENCES `empleados` (`CedulaEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Clientes_Lugares1` FOREIGN KEY (`IdLugarCliente`) REFERENCES `lugares` (`IdLugar`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Clientes_Personas` FOREIGN KEY (`CedulaCliente`) REFERENCES `personas` (`CedulaPersona`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Clientes_TiposEmpresas1` FOREIGN KEY (`IdTipoCliente`) REFERENCES `tiposempresas` (`IdTipo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `coloresproductos`
ADD CONSTRAINT `fk_colores_productos` FOREIGN KEY (`idColorColoresProd`) REFERENCES `colores` (`idColor`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_prouctos_colores` FOREIGN KEY (`idProductoColoresProd`) REFERENCES `productos` (`IdProducto`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `concentracionesproductos`
ADD CONSTRAINT `fk_concentr_produ` FOREIGN KEY (`idConcentracionConcProd`) REFERENCES `concentraciones` (`idConcentracion`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_produc_concentr` FOREIGN KEY (`idProductoConcProd`) REFERENCES `productos` (`IdProducto`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cotizaciones`
ADD CONSTRAINT `fk_Cotizaciones_Cliente` FOREIGN KEY (`NitClienteCotizaciones`) REFERENCES `clientes` (`Nit`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Cotizaciones_Usuarios1` FOREIGN KEY (`CedulaEmpleadoCotizaciones`) REFERENCES `empleados` (`CedulaEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `detallescotizacion`
ADD CONSTRAINT `fk_CotizacionDetalles_Cotizaciones1` FOREIGN KEY (`IdCotizacionDetallesCotizacion`) REFERENCES `cotizaciones` (`IdCotizacion`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_CotizacionDetalles_Productos1` FOREIGN KEY (`IdProductoDetallesCotizacion`) REFERENCES `productos` (`IdProducto`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `documentos`
ADD CONSTRAINT `fk_documentos_categ` FOREIGN KEY (`idCategoriaDocumentos`) REFERENCES `categoriasproductos` (`IdCategoria`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `empleados`
ADD CONSTRAINT `fk_Empleados_Personas` FOREIGN KEY (`CedulaEmpleado`) REFERENCES `personas` (`CedulaPersona`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gestiones`
ADD CONSTRAINT `fk_Asesorias_Empresas1` FOREIGN KEY (`NitClienteGestiones`) REFERENCES `clientes` (`Nit`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Asesorias_Usuarios1` FOREIGN KEY (`CedulaEmpleadoGestiones`) REFERENCES `empleados` (`CedulaEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `lugares`
ADD CONSTRAINT `fk_lug_dept` FOREIGN KEY (`idDepartamentoLugar`) REFERENCES `departamentos` (`idDepartamento`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `metasusuarios`
ADD CONSTRAINT `fk_MetasUsuario_Metas1` FOREIGN KEY (`IdMetaMetasUsuarios`) REFERENCES `metas` (`IdMeta`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_MetasUsuario_Usuarios1` FOREIGN KEY (`CedulaMetasUsuarios`) REFERENCES `empleados` (`CedulaEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ordencompracliente`
ADD CONSTRAINT `fk_orden_pedido` FOREIGN KEY (`idPedido`) REFERENCES `pedidos` (`IdPedido`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pedidos`
ADD CONSTRAINT `fk_OrdenDeCompra_Cotizaciones1` FOREIGN KEY (`IdCotizacionPedidos`) REFERENCES `cotizaciones` (`IdCotizacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `permisos`
ADD CONSTRAINT `fk_PermisosCategorias_Categorias` FOREIGN KEY (`IdCategoria`) REFERENCES `permisoscategorias` (`IdCategoria`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `permisosroles`
ADD CONSTRAINT `fk_RolesPermisos_Permisos1` FOREIGN KEY (`IdPermisoPermisosRoles`) REFERENCES `permisos` (`IdPermiso`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_RolesPermisos_Roles1` FOREIGN KEY (`IdRolPermisosRoles`) REFERENCES `roles` (`IdRol`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `productos`
ADD CONSTRAINT `fk_Productos_CategoriaProducto1` FOREIGN KEY (`IdCategoriaProductos`) REFERENCES `categoriasproductos` (`IdCategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Productos_Impuestos` FOREIGN KEY (`IdIvaProductos`) REFERENCES `impuestos` (`IdIva`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `puntosentrega`
ADD CONSTRAINT `fk_punto_cliente` FOREIGN KEY (`nitEmpresaPuntoEntrega`) REFERENCES `clientes` (`Nit`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_punto_lugar` FOREIGN KEY (`idLugarPuntoEntrega`) REFERENCES `lugares` (`IdLugar`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `rolesusuarios`
ADD CONSTRAINT `fk_RolUsiario_Personas` FOREIGN KEY (`CedulaRolesUsuarios`) REFERENCES `personas` (`CedulaPersona`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_RolUsuario_Roles1` FOREIGN KEY (`IdRolRolesUsuarios`) REFERENCES `roles` (`IdRol`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `valorpresentacion`
ADD CONSTRAINT `fk_valor_pres` FOREIGN KEY (`idpresentacion`) REFERENCES `presentaciones` (`IdPresentacion`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_valor_prod` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`IdProducto`) ON DELETE CASCADE ON UPDATE CASCADE;



DROP TABLE IF EXISTS `comentariosGetiones`;
CREATE TABLE `comentariosGetiones` (
  `IdComentario` int(11) AUTO_INCREMENT NOT NULL,
  `Comentario` varchar(100) NOT NULL,
  `PersonaComenta` int(11) NOT NULL,
  `IdComentarioGestiones` int(11) NOT NULL,
  PRIMARY KEY (`IdComentario`),
  KEY `fk_comentario_gestiones` (`IdComentarioGestiones`),
  CONSTRAINT `fk_comentario_gestiones` FOREIGN KEY (`IdComentarioGestiones`) REFERENCES `gestiones` (`IdGestion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `detallesgestion`;
CREATE TABLE `detallesgestion` (
  `IdGestionesAsunto` int(11) AUTO_INCREMENT NOT NULL,
  `IdDetallesGestion` int(11) NOT NULL,
  `Asunto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdGestionesAsunto`),
  KEY `fk_detalles_gestion` (`IdDetallesGestion`),
  CONSTRAINT `fk_detalles_gestion` FOREIGN KEY (`IdDetallesGestion`) REFERENCES `gestiones` (`IdGestion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;