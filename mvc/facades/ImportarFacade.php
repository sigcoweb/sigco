<?php
include_once '../models/ImportarDao.php';
include_once '../utilities/Conexion.php';
class ImportarFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new ImportarDao();
    }

    public function importarDatos($archivo, $tabla, $separador, $encabezado){
        return $this->objDao->importarDatos($archivo, $tabla, $separador, $encabezado, $this->con);
    }

    public function listarTablas(){
        return $this->objDao->listarTablas($this->con);
    }

    public function listarCampos($nombreTabla){
        return $this->objDao->listarCampos($nombreTabla,$this->con);
    }

    public function ejemploDato($tipoDato){
        $arrayDato=explode('(',$tipoDato);
        $ejemplo=$arrayDato[0];
        if($arrayDato[0]=='datetime') {
            $time=getdate();
            $ejemplo=date('Y-m-d', time()).' '.$time['hours'].':'.$time['minutes'].':'.$time['seconds'];
        }
        if($arrayDato[0]=='date') {
            $ejemplo=date('Y-m-d', time());
        }
        if($arrayDato[0]=='int'||$arrayDato[0]=='bigint'){
            $ejemplo=1234567890;
        }
        if($arrayDato[0]=='char'||$arrayDato[0]=='varchar'){
            $ejemplo='Texto ejemplo';
        }
        if($arrayDato[0]=='time'){
            $time=getdate();
            $ejemplo=$time['hours'].':'.$time['minutes'].':'.$time['seconds'];
        }
        if($arrayDato[0]=='enum'){
            $arrayOpciones=explode("'",$arrayDato[1]);
            $ejemplo=$arrayOpciones[1];
        }
        return $ejemplo;
    }

}