<?php
include_once'../models/EmpleadoDao.php';
include_once'../utilities/Conexion.php';
Class FacadeEmpleado{
    private $con;
    private $objDao;

    public function __Construct(){

        $this->con=Conexion::getConexion();
        $this->objDao=new EmpleadoDao();
    }


    public function registrarEmpleado(EmpleadoDto $objeto,$rol){
        return $this->objDao->registrarEmpleado($objeto,$rol,$this->con);
    }

    public function obtenerUsuario($idEmpleado){
        return $this->objDao->buscarUsuario($idEmpleado,$this->con);
    }

    public function listarUsuarios(){
        return $this->objDao->listarUsuarios($this->con);
    }

    public function listarEmpleadosAsesores(){
        return $this->objDao->listarEmpleadosAsesores($this->con);
    }

    public function listarDocumentos(){
        return $this->objDao->listarDocumentos($this->con);
    }

    public function borrarUsuario($user){
        return $this->objDao->cancelarUsuario($user,$this->con);
    }

    public function modificarUsuario(EmpleadoDto $obj,$idPersona,$rol){
        return $this->objDao->modificarUsuario($obj,$idPersona,$rol,$this->con);
    }

    public function comprobarUsuario($user,$pass){
        $validar=$this->objDao->login($user,$pass,$this->con);
        if ($validar['existe']!=0){
            $validar=$this->objDao->datosLogin($user,$this->con);
        }else{
            $validar=0;
        }
        return $validar;
    }

    public function getUserData($user){
        return $this->objDao->datosLogin($user,$this->con);
    }

    public function unlock($pass){
        $validar=$this->objDao->login($_SESSION['datosLogin']['id'],$pass,$this->con);
        if ($validar['existe']==0){
            return false;
        }else{
            return true;
        }

    }

    public function generarClave()
    {
        return $this->objDao->generar_clave();
    }

    public function verificarExistencia($user){
        return $this->objDao->verificar($user,$this->con);
    }

    public function obtenerMenu($rol){
        return $this->objDao->obtenerTitulos($rol,$this->con);
    }

    public function obtenerSubMenu($id,$rol){
        return $this->objDao->obtenerSubTitulos($id,$rol,$this->con);
    }

    public function listarRoles(){
        return $this->objDao->listarRoles($this->con);
    }

    public function listarMetas(){
        return $this->objDao->listarMetas($this->con);
    }


    public function buscarCriterio($criterio,$busqueda,$comobuscar){
        return $this->objDao->buscarEmpleadoCriterio($criterio,$busqueda,$comobuscar,$this->con);

    }

    public function cambiarEstado($user,$estado){
        return $this->objDao->cambiarEstado($user,$estado,$this->con);
    }

    public function cambiarClave($clave,$user){
        return $this->objDao->cambiarClave($user,$clave,$this->con);
    }
    public function totalVentasEmpleados(){
        return $this->objDao->totalVentasEmpleados($this->con);
    }
    public function obtenerRoles($user){
        return $this->objDao->obtenerRoles($user,$this->con);
    }

    public function validaUserPass($user,$pass){
        return $this->objDao->validaUserPass($user,$pass,$this->con);
    }

    public function validaUserMail($user,$mail){
        return $this->objDao->validaUserMail($user,$mail,$this->con);
    }




}