<?php
include_once '../utilities/Conexion.php';
include_once '../models/installDao.php';
class installFacade
{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new installDao();
    }

    public function installScript($script){
        return $this->objDao->installScript($script, $this->con);
    }

    public function createAdmin($cedula, $email, $pass){
        return $this->objDao->createAdmin($cedula, $email, $pass, $this->con);
    }

    public function cantidadTablas($base){
        return $this->objDao->cantidadTablas($base, $this->con);
    }

    public function verifyAdmin($admin,$email,$pass){
        return $this->objDao->verifyAdmin($admin,$email,$pass, $this->con);
    }
}