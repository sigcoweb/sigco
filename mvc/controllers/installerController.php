<?php
include_once '../utilities/installDto.php';
$dataBase=new installDto();

if(isset($_GET['makeFile'])){
    $cop=$dataBase->installIni($_POST['host'],$_POST['baseName'],$_POST['userName'],$_POST['userPass']);
    for($co=0;$co<2;$co++){
        if($cop){
            break(1);
        }else{
            $cop=$dataBase->installIni($_POST['host'],$_POST['baseName'],$_POST['userName'],$_POST['userPass']);
            sleep(1);
        }
    }
    if($cop){
        for($con=0;$con<2;$con++){
            if(!isset($userPass)||$userPass!=$_POST['userPass']){
                include_once '../utilities/config.php';
            }else{
                sleep(1);
            }
            if($_POST['userPass']==$userPass){
                for($a=0;$a<2;$a++){
                    if(!isset($userPass)||$userPass!=$_POST['userPass']){
                        include_once '../utilities/config.php';
                    }
                    $try=$dataBase->existsConection($host, $baseName, $userName, $userPass);
                    if($try==1){
                        include_once '../facades/installFacade.php';
                        $installFaca=new installFacade();
                        if($installFaca->cantidadTablas($baseName)==0){
                            header('Location: ../views/install.php?lock=true');
                        }else{
                            header("location: ../views/install.php?error=true&mensaje=La base de datos ya contiene tablas, debe utilizar una base en blanco (la base tiene ".$installFaca->cantidadTablas($baseName)." tablas)");
                        }
                    }else{
                        sleep(1);
                        if($a=1){
                            header('Location: ../views/install.php?error=true&mensaje=El usuario y la base de datos no han sido encontrados en el servidor. Verifique los requisitos.'.$try);
                        }
                    }
                }
            }else{
                sleep(1);
            }
        }
    }else{
        header('Location: ../views/install.php?error=true&mensaje=No ha sido posible escribir en los archivos necesarios. Verifique los permisos en el servidor.'.$cop);
    }
}

if(isset($_GET['installer'])){
    include_once '../facades/installFacade.php';
    if(!isset($baseName)){
        include_once '../utilities/config.php';
    }
    $installFaca=new installFacade();
    $archivo='../utilities/structure.sql';
    if(is_readable($archivo)){
        $try=$dataBase->existsConection($host, $baseName, $userName, $userPass);
        if($try==1){
            $script=file_get_contents($archivo,'r');
            if(isset($script)){
                $res=$installFaca->installScript($script);
                if($res==1){
                    header('Location: ../views/config.php');
                }else{
                    $dataBase->installFail();
                    header('Location: ../views/install.php?error=true&mensaje='.$res);
                }
            }else{
                $dataBase->installFail();
                header('Location: ../views/install.php?error=true&mensaje=No se ha encontrado la informacion necesaria en el archivo para inicializar la base de datos. Debería volver a copiar los archivos y reintentar.');
            }
        }else{
            header('Location: ../views/install.php?error=true&mensaje=No se ha establecido la conexión con la base de datos y el usuario indicados.');
        }
    }else{
        header('Location: ../views/install.php?error=true&mensaje=No ha sido posible leer el archivo necesario para inicializar la base de datos. Compruebe los persmisos de lectura en el servidor.');
    }
}

if(isset($_GET['config'])){
    include_once '../facades/installFacade.php';
    $installFaca=new installFacade();
    $archivo='../utilities/data.sql';
    if(is_readable($archivo)){
        $script=file_get_contents($archivo,'r');
        if(isset($script)){
            $res=$installFaca->installScript($script);
            if($res){
                $user=$installFaca->createAdmin($_POST['adminName'],$_POST['adminEmail'],$_POST['adminPass']);
                $verifiUser=$installFaca->verifyAdmin($_POST['adminName'],$_POST['adminEmail'],$_POST['adminPass']);
                if($user){
                    header('Location: ../views/finish.php?user='.$_POST['adminName'].'&pass='.substr($_POST['adminPass'],(strlen($_POST['adminPass'])-3),3).'&email='.$_POST['adminEmail']);
                }else{
                    $dataBase->installFail();
                    header('Location: ../views/config.php?error=true&mensaje='.$user);
                }
            }else{
                $dataBase->installFail();
                header('Location: ../views/config.php?error=true&mensaje='.$res);
            }
        }else{
            $dataBase->installFail();
            header('Location: ../views/install.php?error=true&mensaje=No se ha encontrado la informacion necesaria en el archivo para inicializar la base de datos. Debería volver a copiar los archivos y reintentar.');
        }
    }else{
        $dataBase->installFail();
        header('Location: ../views/install.php?error=true&mensaje=No ha sido posible leer el archivo necesario para inicializar la base de datos. Compruebe los persmisos de lectura en el servidor.');
    }
}

if(isset($_POST['forzar'])&&isset($_POST['userName'])&&isset($_POST['userPass'])&&$_POST['forzar']=='on'){
    include_once '../facades/FacadeEmpleado.php';
    session_start();
    $fachada = new FacadeEmpleado();
    unset($_SESSION['forzar']);
    $user=$_POST['userName'];
    $pass=$_POST['userPass'];
    $_SESSION['forzar']=$fachada->comprobarUsuario($user,$pass);
    if ($_SESSION['forzar']==0){
        unset($_SESSION['forzar']);
        header('location: ../views/install.php?error=true&mensaje=Los datos no son correctos.');
    }else{
        if($_SESSION['forzar']['NombreRol']=='Administrador'){
            header('location: ../views/install.php');
        }else{
            unset($_SESSION['forzar']);
            header('location: ../views/install.php?error=true&mensaje=El usuario no tiene los privilegios suficientes para realizar el proceso.');
        }
    }
}

