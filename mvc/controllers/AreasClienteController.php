<?php
session_start();
include_once '../models/AreasClienteDto.php';
include_once '../facades/AreasClienteFacade.php';
$fachada = new AreasClienteFacade();
if(isset($_GET['controlar'])) {
    $accion = $_GET['controlar'];
    switch ($accion) {
        case 'crear':
            $areaDto=new AreasClienteDto($_POST['nombreArea'],$_POST['nit'],$_POST['nombreContacto'],$_POST['telefonoArea'],$_POST['correoArea'],$_POST['observacionesArea']);
            $mensaje=$fachada->registrarArea($areaDto);
            header("Location: ../views/buscarAreasEmpresas.php?mensaje=".$mensaje);
            break;
        case 'modificar':
            $areaDto=new AreasClienteDto($_POST['nombreArea'],$_POST['nit'],$_POST['nombreContacto'],$_POST['telefonoArea'],$_POST['correoArea'],$_POST['observacionesArea']);
            $areaDto->setNombreAntiguo($_POST['nombreAntiguo']);
            $areaDto->setNitAntiguo($_POST['nitAntiguo']);
            $mensaje=$fachada->modificarArea($areaDto);
            header("Location: ../views/buscarAreasEmpresas.php?mensaje=".$mensaje);
            break;
        case 'buscar':
            $criterio = $_POST['criterio'];
            $busqueda = $_POST['busqueda'];
            $comobuscar = $_POST['comobuscar'];
            $mensaje = $fachada->buscarArea($criterio, $busqueda, $comobuscar);
            if ($mensaje == null) {
                header("Location: ../views/buscarAreasEmpresas.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            } else {
                header("Location: ../views/buscarAreasEmpresas.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            }
            break;
        case 'todos':
            $mensaje = $fachada->listarTodos();
            if ($mensaje == null) {
                header("Location: ../views/buscarAreasEmpresas.php?encontrados=false");
            } else {
                header("Location: ../views/buscarAreasEmpresas.php?encontrados=true&todos=todos");
            }
            break;
        default:
            echo 'Valor incorrecto enviado por el método get a la variable controlar';
    }
}



