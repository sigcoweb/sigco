<?php
include_once '../facades/FacadeEmpleado.php';
if(!isset($_SESSION)){
    session_start();
}
$facaEmpleado=new FacadeEmpleado();

if (isset ($_POST['userChange'])&&isset($_POST['passChange'])){
    $respuesta='La información ingresada no es correcta. Intente nuevamente';
    if($_POST['userChange']==$_SESSION['datosLogin']['CedulaPersona']&&md5($_POST['passChange'])==$_SESSION['datosLogin']['Contrasenia']){
        $respuesta = $facaEmpleado->validaUserPass($_POST['userChange'],$_POST['passChange']);
    }
    echo $respuesta;
}

if(isset($_POST['cambia'])&&isset($_POST['newPass'])){
    $respuesta=$facaEmpleado->cambiarClave($_POST['newPass'],$_SESSION['datosLogin']['CedulaPersona']);
    echo $respuesta;
}