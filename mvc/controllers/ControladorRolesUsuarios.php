<?php
/**
 * Created by PhpStorm.
 * User: iStam
 * Date: 2/09/15
 * Time: 8:02 PM
 */
include_once '../models/RolesUsuariosDto.php';
include_once '../facades/FacadeRolesUsuarios.php';
include_once '../facades/FacadeEmpleado.php';
$fachada = new FacadeRolesUsuarios();
$facadeEmpleado = new FacadeEmpleado();
if(isset($_POST['guardar'])) {

    $dto = new RolesUsuariosDto();
    $dto->setCedula($_POST['cedula']);
    $dto->setRol($_POST['rol']);
    $mensaje = $fachada->registrarRolUsuario($dto);
    header('location: ../views/asignarRol.php?mensaje=' . $mensaje);
}
if(isset($_POST['getCurrentRol'])){
    print json_encode($facadeEmpleado->obtenerRoles($_POST['getCurrentRol']));
}