<?php
include_once '../utilities/config.php';

if(isset($_GET['guardar'])){
//aca los parametros de conexion, si tienes aparte la conexión , solo incluyuela
    $usuario=$userName;
    $passwd=$userPass;
    $bd=$baseName;
    date_default_timezone_set('America/Bogota');
    $nombre="backDB-".date('Y-m-d-g-i-sa', time()).".txt"; //Este es el nombre del archivo a generar
    /* Determina si la tabla será vaciada (si existe) cuando  restauremos la tabla. */
    $drop = false;
    $tablas = false; //tablas de la bd
// Tipo de compresion.
// Puede ser "gz", "bz2", o false (sin comprimir)
    $compresion = false;
    if($_GET['tipo']!='txt'){
        $compresion=$_GET['tipo'];
    }

    /* Conexion */
    $conexion = mysqli_connect($host, $usuario, $passwd)
    or die("No se puede conectar con el servidor MySQL: ".mysqli_error($conexion));
    mysqli_select_db($conexion, $bd)
    or die("No se pudo seleccionar la Base de Datos: ". mysqli_error($conexion));
    /* Se busca las tablas en la base de datos */
    if ( empty($tablas) ) {
        $consulta = "SHOW TABLES FROM $bd;";
        $respuesta = mysqli_query($conexion, $consulta)
        or die("No se pudo ejecutar la consulta: ".mysqli_error($conexion));
        while ($fila = mysqli_fetch_array($respuesta)) {
            $tablas[] = $fila[0];
        }
    }
    /* Se crea la cabecera del archivo */
    $info['dumpversion'] = "1.1b";
    $info['fecha'] = date("d-m-Y");
    $info['hora'] = date("h:m:s A");
    $info['mysqlver'] = mysqli_get_server_info($conexion);
    $info['phpver'] = phpversion();
    ob_start();
    print_r($tablas);
    $representacion = ob_get_contents();
    ob_end_clean ();
    preg_match_all('/(\[\d+\] => .*)\n/', $representacion, $matches);
    $info['tablas'] = implode(";  ", $matches[1]);
    $dump = <<<EOT
# +===================================================================
# |
# | Generado el {$info['fecha']} a las {$info['hora']}
# | Servidor: {$_SERVER['HTTP_HOST']}
# | MySQL Version: {$info['mysqlver']}
# | PHP Version: {$info['phpver']}
# | Base de datos: '$bd'
# | Tablas: {$info['tablas']}
# |
# +-------------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
EOT;
    foreach ($tablas as $tabla) {

        $drop_table_query = "";
        $create_table_query = "";
        $insert_into_query = "";

        /* Se halla el query que será capaz vaciar la tabla. */
        if ($drop) {
            $drop_table_query = "DROP TABLE IF EXISTS `$tabla`;";
        } else {
            $drop_table_query = "# No especificado.";
        }

        /* Se halla el query que será capaz de recrear la estructura de la tabla. */
        $create_table_query = "";
        $consulta = "SHOW CREATE TABLE $tabla;";
        $respuesta = mysqli_query($conexion,$consulta)
        or die("No se pudo ejecutar la consulta: ".mysqli_error($conexion));
        while ($fila = mysqli_fetch_array($respuesta)) {
            $create_table_query = $fila[1].";";
        }

        /* Se halla el query que será capaz de insertar los datos. */
        $insert_into_query = "";
        $consulta = "SELECT * FROM $tabla;";
        $respuesta = mysqli_query($conexion,$consulta)
        or die("No se pudo ejecutar la consulta: ".mysqli_error($conexion));
        while ($fila = mysqli_fetch_array($respuesta)) {
            $columnas = array_keys($fila);
            foreach ($columnas as $columna) {
                if ( gettype($fila[$columna]) == "NULL" ) {
                    $values[] = "NULL";
                } else {
                    $values[] = "'".$fila[$columna]."'";
                }
            }
            $insert_into_query .= "INSERT INTO `$tabla` VALUES (".implode(", ", $values).");\n";
            unset($values);
        }

        $dump .= <<<EOT
SET FOREIGN_KEY_CHECKS=1
EOT;
    }

    if($_GET['guardar']=='guardar'){
        $archivo = '../documents/backsdb/'.$nombre;
        if(is_writable('../documents/backsdb')){
            $fp=fopen($archivo,"xb");
            fputs($fp,$dump);
            fclose($fp) ;
            header('Location: ../views/crearBackup.php?mensaje=Se ha almacenado el backup con el nombre '.$nombre);
        }else{
            header('Location: ../views/crearBackup.php?error=true&mensaje=No se ha guardado el archivo. No hay permisos de escritura en la carpeta.');
        }
    }else{
        if ( !headers_sent() ) {
            header("Pragma: no-cache");
            header("Expires: 0");
            header("Content-Transfer-Encoding: binary");
            switch ($compresion) {
                case "gz":
                    header("Content-Disposition: attachment; filename=$nombre.gz");
                    header("Content-type: application/x-gzip");
                    echo gzencode($dump, 9);
                    break;
                case "bz2":
                    header("Content-Disposition: attachment; filename=$nombre.bz2");
                    header("Content-type: application/x-bzip2");
                    echo bzcompress($dump, 9);
                    break;
                default:
                    header("Content-Disposition: attachment; filename=$nombre");
                    header("Content-type: application/force-download");
                    echo $dump;
            }
        } else {
            header('Location: ../views/crearBackup.php?error=true&mensaje=Ha ocurrido un error al generar el archivo: '.$dump);
        }
    }
}


if(isset($_GET['descargar'])){

    $archivo=$_GET['archivo'];
    $ruta = '../documents/backsdb/'.$_GET['archivo'];

    if (is_file($ruta))
    {
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename='.$archivo);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($ruta));
        readfile($ruta);
        header('Location: ../views/crearBackup.php?mensaje=Se ha iniciado la descarga del archivo '.$archivo);
    }
    else{
        header('Location: ../views/crearBackup.php?error=true&mensaje=El archivo seleccionado no es válido para la descarga.');
    }
}