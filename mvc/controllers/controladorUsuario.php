<?php

include_once '../models/EmpleadoDto.php';
include_once '../facades/FacadeEmpleado.php';
include_once '../utilities/mailgun-php-master/vendor/autoload.php';
session_start();

$fachada = new FacadeEmpleado();
$dto= new EmpleadoDto();


if (isset($_POST['documento'])) {
    $clave;
    if($_POST['changepass']==1){
        $dto->setContrasenia(md5($_POST['pass1']));
        $clave=$_POST['pass1'];
    }else{
        $dto->setContrasenia(md5($_POST['documento']));
        $clave=$_POST['documento'];
    }
    $dto->setIdUsuario($_POST['documento']);
    $dto->setNombres($_POST['nombres']);
    $dto->setApellidos($_POST['apellidos']);
    $dto->setEmpleo($_POST['cargo']);
    $dto->setEmail($_POST['email']);

    $dto->setEstado($_POST['estadoEmpleado']);

    if($_FILES['imagen']['size']>0){
        $file = $_FILES['imagen'];
        $name = $file['name'];
        $path = "../images/" . basename($name);
        move_uploaded_file($file['tmp_name'], $path );
        $dto->setRutaimagen($name);
    }else{
        $dto->setRutaimagen('sinImagen.jpg');
    }


    $dto->setCelular($_POST['celular']);
    $dto->setFechaNacimiento($_POST['fechaNacimiento']);
    $dto->setIdLugar($_POST['IdLugar']);


    $mensaje= $fachada->registrarEmpleado($dto,$_POST['rol']);
    if($mensaje=="Empleado registrado exitosamente"){
        $mg = new \Mailgun\Mailgun('key-5a31f6e6575d5274a958973433fbaa00');
        $domain = "sandbox688a73d786dc48aea3ee45e1e3ec35f8.mailgun.org";

        $result = $mg->sendMessage($domain, array(
            'from' => 'Notificaciones Mapcompany <notificaciones@mapcompany.com>',
            'to' => $dto->getNombres() . '<' . $dto->getEmail(). '>',
            'subject' => 'Bienvenido a MAP Company',
            'html' => '<div style="min-height:100%!important;width:100%!important;background:#eeeeee;margin:0;padding:0" bgcolor="#EEEEEE">

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
        <tbody><tr>
            <td align="center" valign="top">
                <table align="center" border="0" cellspacing="0" width="600" style="width:600px!important;min-width:600px!important;border-collapse:collapse!important">
                    <tbody><tr>
                        <td align="center" valign="top">

                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="text-align:center!important;background-image:url("")!important;background-repeat:no-repeat!important;background-position:center center!important">
                         <img src="http://s22.postimg.org/52ex74b1d/logo.png" width="172" height="50" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px auto 20px;border:0" class="CToWUd">
                        </td>
                    </tr>



                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="overflow:hidden;border-radius:4px;max-width:600px!important;min-width:600px!important;border-collapse:collapse!important;text-align:center;background:#ffffff" bgcolor="#FFFFFF">
                                <tbody><tr>
                                    <td valign="top" bgcolor="#00C2E0" style="max-width:100%!important;background:#a21300 url("http://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/9d93e995-abb6-4951-b345-1472b6fef71c.jpg") no-repeat top center;padding:20px 0 0">
                                    <h1 style="font-family:Helvetica;font-size:36px;font-style:normal;font-weight:bold;text-align:center;margin:55px auto 20px" align="center"><font color="#ffffff">Bienvenido '.$dto->getNombres().' ,</font></h1>
                                    <h2 style="font-size:24px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:0px auto 25px"><font color="#FFFFFF">Nos alegra poder contar con su apoyo</font></h2>
                                    </td>
                                </tr>


                                <tr>
                                    <td style="text-align:left" align="left">
                                        <img src="http://www.datosgratis.net/wp-content/uploads/2010/12/fiesta-15-anos.jpg" align="left" width="150" height="150" style="display:inline-block;min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:40px;border:0;border-radius: 50%" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 228.5px; top: 536px;"><div id=":1a2" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                        <p style="text-align:left;font-size:22px;line-height:28px;margin:20 auto 10px;padding:20px 40px 0 0" align="left"><font color="#4d4d4d">Sus datos de acceso para SIGCO son:<br>Usuario:'.$dto->getIdUsuario().'<br>Contraseña:'.$clave.' </p></font>
                                        <table border="0" cellpadding="14" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:24px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:#61bd4f;margin:10px 0 20px" align="center" bgcolor="#61BD4F">
                                            </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#A86CC1">


                                        <h2 style="font-size:16px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:40px auto 0"><font color="#ffffff">En caso de tener alguna duda sobre cualquier tema, puede hacerla presente en nuestro departamento administrativo</font></h2><br>

                                        <table border="0" cellpadding="10" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:20px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:rgba(255,255,255,0.2);margin:0 auto 50px;border:2px solid #ffffff" align="center" bgcolor="rgba(255,255,255,0.2)">
                                            <tbody><tr>
                                                <td align="center" valign="middle">
                                                    <a href="http://mapcompanysas.com/" style="text-decoration:none;padding:10px" target="_blank"><font color="white">visite nuetra página web →</font></a>


                                                </td>
                                            </tr>
                                            </tbody></table>

                                    </td>
                                </tr>


                                </tbody></table>
                        </td>
                    </tr>



                    <tr>
                        <td align="center" valign="top">
                            <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" style="border-collapse:collapse!important;max-width:100%!important">
                                <tbody><tr>
                                    <td valign="top">


                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse:collapse!important">


                                            <tbody><tr>
                                                <td valign="top">
                                                    <br>
                                                    <div style="color:#707070;font-family:Arial;font-size:12px;line-height:125%;text-align:center;max-width:100%!important" align="center">

                                                        <em>Copyright © 2016 <span class="il">sigcoweb</span>  All rights reserved.</em>
                                                        <br>
                                                    </div>
                                                    <br>
                                                </td>

                                            </tr>
                                            </tbody></table>


                                    </td>
                                </tr>
                                </tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    &nbsp;

    <img src="https://ci6.googleusercontent.com/proxy/2uDvcVf13UcX3KBFMOGvsfy8Ej4tBzBZ-VCBUBK8y_IlIdj6tthi-ozaR4rAsdx8rDXRXjy1fLAl658hgy82lL0vhBQjC362MY7AqtWvKjvaz15PAi1cYsCSX3Au9yU0R6yQu6dL7avlyhMyMOF9Eb_dw8n1FdugnGMfPeEvQYSXSsSvg7TrbJC8DmVQRdXix5_i=s0-d-e1-ft#http://em.trello.com/e/o/eyJlbWFpbF9pZCI6Ik5ERXpOVEk2RndGWDJ3SmtBQUp6QUJjY2JRZ2FBVkpwVXZuTEZVVVlWckpjMWdGdE9qSTRNVFl5TUFBPSJ9" class="CToWUd"><div class="yj6qo"></div><div class="adL">
    </div></div>'));


        header('location: ../views/RegistrarEmpleado.php?mensaje='.$mensaje);



    }
    header('location: ../views/RegistrarEmpleado.php?mensaje='.$mensaje);



}

if (isset($_GET['buscar'])) {
    unset($_SESSION['consulta']);
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $fachada->buscarCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarEmpleado.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarEmpleado.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['controlar'])) {
    $resul = $fachada->listarUsuarios();
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarEmpleado.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarEmpleado.php?encontrados=true&todos=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['modificar'])) {

    if(isset($_POST['changepass'])&&$_POST['changepass']==1){
        $dto->setContrasenia(md5($_POST['pass1']));
    }else{
        include_once '../facades/FacadeEmpleado.php';
        $facaEmpl=new FacadeEmpleado();
        $empl=$facaEmpl->obtenerUsuario($_GET['idPersona']);
        $dto->setContrasenia($empl['Contrasenia']);
    };

    if(!isset($_POST['imagen'])){
        $dto->setRutaimagen('sinImagen.jpg');
    }else{
        $dto->setRutaimagen($_POST['imagen']);
    };

    $dto->setIdUsuario($_POST['cc']);
    $dto->setNombres($_POST['nombres']);
    $dto->setApellidos($_POST['apellidos']);
    $dto->setCelular($_POST['celular']);
    $dto->setEmpleo($_POST['cargo']);
    $dto->setEmail($_POST['email']);
    $dto->setEstado($_POST['estadoEmpleado']);
    $dto->setFechaNacimiento($_POST['fechaNacimiento']);
    $dto->setIdLugar($_POST['IdLugar']);

    $mensaje=$fachada->modificarUsuario($dto,$_GET['idPersona'],$_POST['rol']);
    header("Location: ../views/buscarEmpleado.php?mensaje=" . $mensaje);

}

if (isset($_GET['cambiarEstado'])) {
    $estado=$_GET['estado'];

    if ($estado=="Activo"){
        $estado="Inactivo";
    }else{
        $estado="Activo";
    }
    $mensaje=$fachada->cambiarEstado($_GET['id'],$estado);
    header("Location: ../views/buscarEmpleado.php?mensaje=" . $mensaje);
}



