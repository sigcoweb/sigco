<?php
include_once '../models/CotizacionesDTO.php';
include_once'../models/DetallesCotizacionDTO.php';
include_once '../facades/FacadeCotizaciones.php';
include_once '../facades/FacadeProducto.php';
include_once'../models/ProductosCotizados.php';
include_once'../utilities/gestionCotPdf.php';
include_once '../facades/ClienteFacade.php';
session_start();
$facadeCliente=new ClienteFacade();
$gestionCot=new gestionCotPdf();
$dto=new CotizacionesDTO();
$facade=new FacadeCotizaciones();
$facadeProducto=new FacadeProducto();
$detalles=new DetallesCotizacionDTO();

if (isset($_GET['buscar'])) {
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $facade->buscarConCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarCotizaciones.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarCotizaciones.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['listar'])) {
    unset($_SESSION['consulta']);
    $resul = $facade->listarCotizaciones();
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarCotizaciones.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarCotizaciones.php?encontrados=true&todos=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['agregar'])) {
    $select=$_POST['idproducto'];
    $cantidad=$_POST['cantidad'];
    $cliente=$_POST['idcliente'];
    $explode=explode(",",$select);
    $idProducto=$_POST['idproducto'];
    $idPresentacion=$_POST['idpresentacion'];
    $producto=$facadeProducto->obtenerProductoPresentacion($idProducto,$idPresentacion);
    $dto=new ProductosCotizados();
    $dto->setId($idProducto);
    $dto->setNombre($producto['NombreProducto']);
    $dto->setValorBase($producto['valorPresentacion']);
    $dto->setCantidad($cantidad);
    $dto->setIva("0");
    $dto->setSubtotal($cantidad*$dto->getValorBase());
    $dto->setPresentacion($_POST['idpresentacion']);
    $dto->setAroma($_POST['idaroma']);
    $dto->setColor($_POST['idcolor']);
    $dto->setConcentracion($_POST['idconcentracion']);
    $nomas='';
    if (!isset($_SESSION['productoDatos']) ){
        $_SESSION['productoDatos']=array();
        array_push($_SESSION['productoDatos'],$dto);
    }else{
        $rep=0;
        foreach($_SESSION['productoDatos'] as $product){
            if($_POST['idproducto']==$product->getId()&&$_POST['idpresentacion']==$product->getPresentacion()&&$_POST['idaroma']==$product->getAroma()&&$_POST['idcolor']==$product->getColor()&&$_POST['idconcentracion']==$product->getConcentracion()){
                $rep++;
            }
        }
        if($rep==0){
            array_push($_SESSION['productoDatos'],$dto);
        }else{
            $nomas='&error=true&detalleerror=El producto NO ha sido añadido porque ya se encuentra en la cotización';
        }
    }
    header('location: ../views/crearCotizacion2.php?idcliente='.$cliente.'&empresaCotiza='.$_GET['empresaCotiza'].$nomas);
}


if (isset($_GET['finalizar'])) {
    $mensaje="La cotización no ha sido generada&error=true";
    $dto->setObservaciones($_POST['observaciones']);
    $dto->setEstado("Vigente");
    $dto->setIdCliente($_GET['idcliente']);
    $dto->setEmpresaCotiza($_GET['cotiza']);
    if (isset($_POST['asesor'])){
        $dto->setIdUsuario($_POST['asesor']);
    }else{
        $dto->setIdUsuario($_SESSION['datosLogin']['id']);
    }
    $dto->setValorDescuento("0");
    $idCotizacion=$facade->registrarCotizaciones($dto)['idc'];
    $i=0;
    foreach ($_SESSION['productoDatos'] as $productos ) {
        $detalles->setIdCotizacion($idCotizacion);
        $detalles->setCantidad($productos->getCantidad());
        $detalles->setIdProducto($productos->getId());
        $detalles->setCantidadDespacho($_POST['cantidadDespacho'.$i.'']);
        $valorProducto=$_POST['descuento'.$i.''];
        $total=$valorProducto*$detalles->getCantidadDespacho();
        $detalles->setTotal($total);
        $detalles->setPresentacion($productos->getPresentacion());
        $detalles->setAroma($productos->getAroma());
        $detalles->setColor($productos->getColor());
        $detalles->setConcentracion($productos->getConcentracion());
        $detalles->setPresentacionDetalles($productos->getPresentacion());
        $detalles->setDescuento($_POST['descuento'.$i.'']);
        $detalles->setPresentacionDespacho($_POST['presentacionDespacho'.$i.'']);
        $mensaje=$facade->agregarProducto($detalles);
        print_r($mensaje);
        $i++;
    }
    unset($_SESSION['productoDatos']);
    if($_POST['enviar']){
        $enviado=0;
        $nombresDesde=$_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'];
        $correoDesde=$_SESSION['datosLogin']['EmailPersona'];
        if(isset($_POST['asesor'])){
            $nombresDesde=$facadeCliente->obtenerDatosPersona($_POST['asesor'])['Nombres'].' '.$facadeCliente->obtenerDatosPersona($_POST['asesor'])['Apellidos'];
            $correoDesde=$facadeCliente->obtenerDatosPersona($_POST['asesor'])['EmailPersona'];
        }
        $res=$gestionCot->guardarCot($idCotizacion);
        $correosDestino='';
        foreach($_POST['correo'] as $key=>$correo){
            $correosDestino.=$correo;
            if(isset($_POST['correo'][$key+1])){
                $correosDestino.=", ";
            }
        }
        if($res['resultado']==1){
            try{
                $send=$gestionCot->enviarCot($res['archivo'],$correosDestino,$nombresDesde,$correoDesde);
                $enviado=1;
            }catch(Exception $ex){
                $enviado=$ex->getMessage();
            }
            if($enviado==1){
                $mensaje.='. El mensaje fue enviado. ';
            }else{
                $mensaje.='. El correo no fue enviado. '.$enviado;
            }
            $gestionCot->eliminarCot($res['archivo']);
        }else{
            $mensaje.='. El pdf no ha sido generado, el correo no fue enviado. '.$res;
        }
    }
    if(isset($idCotizacion)){
        $mensaje.='&criterio=cotizaciones.IdCotizacion&comobuscar=1&busqueda='.$idCotizacion;
    }
    header("Location: ../views/buscarCotizaciones.php?mensaje=" . $mensaje);
}



if (isset($_POST['cancelar'])){
    $id=$_POST['id'];
    $comentario=$_POST['comentario'];
    $mensaje=$facade->cancelarCoti($id,$comentario);
    header("Location: ../views/buscarCotizaciones.php?mensaje=" . $mensaje);
}




