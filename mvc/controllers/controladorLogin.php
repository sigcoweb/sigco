<?php
include_once '../facades/FacadeEmpleado.php';
include_once '../utilities/gestionCambiarPass.php';
session_start();
$gesPass=new gestionCambiarPass();
$fachada = new FacadeEmpleado();

if (isset($_GET['login'])){
    unset($_SESSION['datosLogin']);
    $user=$_POST['email'];
    $pass=$_POST['clave'];
    $_SESSION['datosLogin']=$fachada->comprobarUsuario($user,$pass);
    if ($_SESSION['datosLogin']==0){
        unset($_SESSION['hora']);
        unset($_SESSION['datosLogin']);
        header('location: ../../login.php?login=false');
    }else{
        $_SESSION['hora']=time();
        header('location: ../views/index.php');
    }
}

if(isset($_POST['usuario'])&&isset($_POST['email'])){
    $respuesta=$fachada->validaUserMail($_POST['usuario'],$_POST['email']);
    if($respuesta==1){
        $nuevoP=$fachada->generarClave();
        if ($fachada->cambiarClave($nuevoP,$_POST['usuario'])){
            try{
                $gesPass->enviarPas($nuevoP, $_POST['email']);
                $respuesta=1;
            }catch(Exception $ex){
                $respuesta='Error en el envío del correo con la nueva contraseña. Detalle: '.$ex->getMessage();
            }
        }else{
            $respuesta='<p class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Los datos han sido validados pero no ha sido posible generar el cambio, contacte al administrador.';
        }
    }else{
        $respuesta='<p class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> El usuario y correo no son válidos para restablecer la contraseña.</p>';
    }
    echo $respuesta;
}

if(isset($_GET['cambiarol'])){
    $_SESSION['datosLogin']['IdRol']=$_POST['IdRol'];
    $_SESSION['datosLogin']['NombreRol']=$_POST['NombreRol'];
    header('location: ../views/'.$_SESSION['pagina']);
}

if (isset($_GET['forget'])){
    $user=$_POST['usuario'];
    $existe=$fachada->verificarExistencia($user);
    if ($existe==false){
        header('location: ../../login.php?correo=false');
    }else{
        $_SESSION['correo']=$existe;
        $_SESSION['pass']=$fachada->generarClave();
        if ($fachada->cambiarClave($_SESSION['pass'],$user)){
            header('location: ../utilities/resetpassMailgun.php?sent=true');
        }else{
            header('location: ../../login.php?fail=true');
        }
    }
}

if (isset($_GET['unlock'])){
    $login=$fachada->unlock($_POST['pass']);
    if ($login==false){
        header('location: ../../lock_screen.php?login=false');
    }else{
        $_SESSION['hora']=time();
        header('location: ../views/'.$_SESSION['pagina']);
    }
}



if (isset($_GET['account'])){
    unset($_SESSION['datosLogin']);
    unset($_SESSION['hora']);
    session_destroy();
    session_unset();
    header('location: ../../login.php?cerrar=true');
}