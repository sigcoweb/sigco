<?php
/**
 * Created by PhpStorm.
 * User: iStam
 * Date: 3/09/15
 * Time: 7:53 PM
 */
include_once '../models/MetaUsuarioDto.php';
include_once '../facades/FacadeMetaUsuario.php';
include_once '../utilities/mailgun-php-master/vendor/autoload.php';
include_once '../facades/FacadeEmpleado.php';

session_start();
$facade = new FacadeMetaUsuario();
$empleado = new FacadeEmpleado();
$dto = new MetaUsuarioDto();

if (isset($_POST['cedula'])) {
    $dto->setEmpleado($_POST['cedula']);
    $dto->setMeta($_POST['meta']);
    $postMeta = $_POST['meta'];
    $mg = new \Mailgun\Mailgun('key-fec32986bb0db91006bbdd5e6ecbf305');
    $domain = "sandbox15819fc8307a4e94bcd2034c57385a01.mailgun.org";
    $mensaje = $facade->asignarMeta($dto);

    if ($_POST['enviar'] && $mensaje == 'Meta asignada exitosamente') {
        try {
            foreach ($_POST['cedula'] as $user) {

                $metaUsuario = $facade->buscarConCriterio('metas.IdMeta', $postMeta, 1);
                for ($i = 0; $i < count($_POST['cedula']); $i++) {
                    # Make the call to the client.
                    $tipo = $metaUsuario[$i]['CantidadValor'];
                    $dir = dirname($_SERVER['PHP_SELF']);
                    $dirs = explode('/', $dir);
                    if ($metaUsuario[$i]['Tipo'] == 'Ventas') {
                        $tipo = '$' . number_format($metaUsuario[$i]['CantidadValor']);

                    }

                    $body = 'Le ha sido asignada la meta: ' . $metaUsuario[$i]['Tipo'] . '<br> por Valor de: ' . $tipo;
                    $name = $metaUsuario[$i]['fullname'];
                    $datediff = strtotime($metaUsuario[$i]['FechaFin']) - strtotime($metaUsuario[$i]['FechaInicio']);
                    $formatDate = floor($datediff / (60 * 60 * 24));
                    $result = $mg->sendMessage($domain, array(
                        'from' => 'Notificaciones Mapcompany <notificaciones@mapcompany.com>',
                        'to' => $metaUsuario[$i]['fullname'] . '<' . $metaUsuario[$i]['EmailPersona'] . '>',
                        'subject' => 'Asignación de Meta',
                        'html' => '<div style="min-height:100%!important;width:100%!important;background:#eeeeee;margin:0;padding:0" bgcolor="#EEEEEE">

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
        <tbody><tr>
            <td align="center" valign="top">
                <table align="center" border="0" cellspacing="0" width="600" style="width:600px!important;min-width:600px!important;border-collapse:collapse!important">
                    <tbody><tr>
                        <td align="center" valign="top">

                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="text-align:center!important;background-image:url("")!important;background-repeat:no-repeat!important;background-position:center center!important">
                         <img src="http://s22.postimg.org/52ex74b1d/logo.png" width="172" height="50" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px auto 20px;border:0" class="CToWUd">
                        </td>
                    </tr>



                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="overflow:hidden;border-radius:4px;max-width:600px!important;min-width:600px!important;border-collapse:collapse!important;text-align:center;background:#ffffff" bgcolor="#FFFFFF">
                                <tbody><tr>
                                    <td valign="top" bgcolor="#00C2E0" style="max-width:100%!important;background:#00c2e0 url("http://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/9d93e995-abb6-4951-b345-1472b6fef71c.jpg") no-repeat top center;padding:20px 0 0">


                                    <h1 style="font-family:Helvetica;font-size:36px;font-style:normal;font-weight:bold;text-align:center;margin:55px auto 20px" align="center"><font color="#ffffff">Hola! ' . $name . '</font></h1>
                                    <h2 style="font-size:28px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:0px auto 25px"><font color="#FFFFFF">Cumple con tus metas.<br>este año! </font></h2>



                                    </td>
                                </tr>


                                <tr>
                                    <td style="text-align:left" align="left">
                                        <img src="http://4.bp.blogspot.com/-hYSBa6qL80U/VckGSGC-k6I/AAAAAAAAVO0/foqRbdHnfyg/s1600/ser-emprendedor-exito.jpg" align="left" width="150" height="150" style="display:inline-block;min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:40px;border:0;border-radius: 50%" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 228.5px; top: 536px;"><div id=":1a2" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                        <p style="text-align:left;font-size:22px;line-height:28px;margin:20 auto 10px;padding:20px 40px 0 0" align="left"><font color="#4d4d4d">' . $body . '<br>Fecha Inicio: ' . $metaUsuario[$i]['FechaInicio'] . '<br>Fecha Fin: ' . $metaUsuario[$i]['FechaFin'] . '</p></font>
                                        <table border="0" cellpadding="14" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:24px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:#61bd4f;margin:10px 0 20px" align="center" bgcolor="#61BD4F">
                                            <tbody><tr>
                                                <td align="center" valign="middle">
                                                    <a href="' .$_SERVER['SERVER_NAME'] .'/sigcofinal/mvc/views/buscarMetas.php' . '" style="text-decoration:none;padding:10px" target="_blank"><font color="white">Duración ' . $formatDate . ' días</font></a>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>

                                <tr>
                                    <td bgcolor="#A86CC1">


                                        <h2 style="font-size:28px;font-style:normal;font-weight:bold;color:#ffffff!important;margin:40px auto 0"><font color="#ffffff">La disiplina es el puente entre las metas y tus logros</font></h2>

                                        <p style="font-size:22px;line-height:28px;margin:20 auto 10px;padding:0px 40px"><font color="#FFF">
                                                Aumenta tu productividad<em></em>
                                            </font></p>

                                        <table border="0" cellpadding="10" cellspacing="0" style="display:inline-block;font-weight:bold;font-size:20px;text-align:center;color:#ffffff;border-radius:6px;border-collapse:collapse!important;background:rgba(255,255,255,0.2);margin:0 auto 50px;border:2px solid #ffffff" align="center" bgcolor="rgba(255,255,255,0.2)">
                                            <tbody><tr>
                                                <td align="center" valign="middle">
                                                    <a href="http://mapcompanysas.com/" style="text-decoration:none;padding:10px" target="_blank"><font color="white">visita nuetra página web →</font></a>


                                                </td>
                                            </tr>
                                            </tbody></table>

                                    </td>
                                </tr>


                                </tbody></table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p style="text-align:center;color:#aaaaaa;font-weight:bold;font-size:22px;line-height:28px;margin:20px auto 0" align="center">Connect with us:</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center!important" align="center !important">
                            <a href="https://bitbucket.org/sigcoweb/profile/members" style="text-decoration:none;width:40px" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/H9LcRehWW0rx1cI1VOleYXL__7VyHBaaiXzwYJxJZBruL1Q0xXM6oVypiJ8ZLqzNKzCunJxCutSQ6nhCwKyHU-RNE5ifkeQRpGm6m38GmYUTkeKkeNRr_wzkH0GOBl-rNRexnlGOihBAI0u1S3IJpVhlKO0GnEjEK39Itis=s0-d-e1-ft#https://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/e1edb4e7-5b33-4c3d-a010-e15b628929af.png" width="40" height="40" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px 2px;border:0" class="CToWUd"></a>
                            <a href="https://bitbucket.org/sigcoweb/profile/members" style="text-decoration:none;width:40px" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/uvw6MePWBRM3O2RDeP6fd3ZWVJxkLndV19f07e3zTrN0LjuP5H0d2xm5Qdvpk8IpIpD28vlhwl43l9Nl1PS89zGoquT4JFH_cIiR1rLTOcL14a8pUvr-SOtBGKBOcIdnK9gs-_xG4AVfzbkBEBKIWYT0sujPyp_w1eMnhaE=s0-d-e1-ft#https://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/24847ca6-6a1d-4069-88b5-80e353cebbac.png" width="40" height="40" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px 2px;border:0" class="CToWUd"></a>
                            <a href="https://bitbucket.org/sigcoweb/profile/members" style="text-decoration:none;width:40px" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/SP5KmIAa2w6kjiaja4hpdEq-Hsj7EaELBeIlg6IA4_XMbLTe8y5B5_g77GI6-JRdDxQTitr_ZO6B_xQGvgNntbPSDVGjvRUZILPd7cEfs0kHPVbuqjY4ZGsGvvxxnjDisreaaKYxMl17jZzQBFtT6ErrglNQ6cUmTVo5n2A=s0-d-e1-ft#https://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/ab734e10-e4d9-4829-99c7-421e4daa175f.png" width="40" height="40" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px 2px;border:0" class="CToWUd"></a>
                            <a href="https://bitbucket.org/sigcoweb/profile/members" style="text-decoration:none;width:80px!important" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/NLQvurPBSbZWbDUA7TK32iZt4QxEuuNP025mt05kM1BjMOu4fmzGU3IEgqjZ0ylesg1D0AyEKbKhXqTQPQ8zOaPf6CuTCkehNbMTrvoxgi7jbecleIVtMpx3_IYmg8d1OWSIrV-LJ7zyIISsu48vTOyyDxM2S3WSE4Pklzo=s0-d-e1-ft#https://gallery.mailchimp.com/5cb712992a906406c5eae28a7/images/f470fd65-3f90-477b-802e-8d84567280b7.png" width="80" height="40" style="min-height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;margin:10px 2px;border:0" class="CToWUd"></a>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" style="border-collapse:collapse!important;max-width:100%!important">
                                <tbody><tr>
                                    <td valign="top">


                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="border-collapse:collapse!important">


                                            <tbody><tr>
                                                <td valign="top">
                                                    <br>
                                                    <div style="color:#707070;font-family:Arial;font-size:12px;line-height:125%;text-align:center;max-width:100%!important" align="center">

                                                        <em>Copyright © 2016 <span class="il">sigcoweb</span>  All rights reserved.</em>
                                                        <br>
                                                    </div>
                                                    <br>
                                                </td>

                                            </tr>
                                            </tbody></table>


                                    </td>
                                </tr>
                                </tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    &nbsp;

    <img src="https://ci6.googleusercontent.com/proxy/2uDvcVf13UcX3KBFMOGvsfy8Ej4tBzBZ-VCBUBK8y_IlIdj6tthi-ozaR4rAsdx8rDXRXjy1fLAl658hgy82lL0vhBQjC362MY7AqtWvKjvaz15PAi1cYsCSX3Au9yU0R6yQu6dL7avlyhMyMOF9Eb_dw8n1FdugnGMfPeEvQYSXSsSvg7TrbJC8DmVQRdXix5_i=s0-d-e1-ft#http://em.trello.com/e/o/eyJlbWFpbF9pZCI6Ik5ERXpOVEk2RndGWDJ3SmtBQUp6QUJjY2JRZ2FBVkpwVXZuTEZVVVlWckpjMWdGdE9qSTRNVFl5TUFBPSJ9" class="CToWUd"><div class="yj6qo"></div><div class="adL">
    </div></div>'));
                }
            }
            header('location: ../views/buscarMetas.php?mensaje=' . $mensaje);
        } catch (Exception $ex) {

            header('location: ../views/buscarMetas.php?detalleerror=' . $ex->getMessage() . '&error=1&mensaje=error al enviar los correos');

        }
    } else {
        header('location: ../views/buscarMetas.php?mensaje=' . $mensaje);
    }


}

if (isset($_GET['buscar'])) {
    unset($_SESSION['consulta']);
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $facade->buscarConCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta'] = $resul;
    if ($resul == null) {
        header("Location: ../views/buscarMetas.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/buscarMetas.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }
}

if (isset($_GET['listar'])) {
    unset($_SESSION['consulta']);
    $resul = $facade->listarMetas();
    if( !($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador')) {
        $resul = $facade->buscarConCriterio('CedulaEmpleado', $_SESSION['datosLogin']['id'], 1);
    }

    $_SESSION['consulta'] = $resul;
    if ($resul == null) {
        header("Location: ../views/buscarMetas.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/buscarMetas.php?encontrados=true&todos=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }
}

