<?php
class ClientesDao
{
    private $mensaje="";
    public function registrarCliente(ClientesDto $clienteDto, PDO $cnn){
        try{
            $cnn->beginTransaction();
            $query = $cnn->prepare("INSERT INTO personas (CedulaPersona, Nombres,
                                    Apellidos, EmailPersona, EstadoPersona, Contrasenia,
                                    RutaImagenPersona, CelularPersona, FechaCreacionPersona)
                                    VALUES(?,?,?,?,'Activo',MD5(?),'sinImagen.jpg',?, CURRENT_TIMESTAMP )");
            $query->bindParam(1, $clienteDto->getCedula());
            $query->bindParam(2, $clienteDto->getNombres());
            $query->bindParam(3, $clienteDto->getApellidos());
            $query->bindParam(4, $clienteDto->getEmail1());
            $query->bindParam(5, $clienteDto->getCedula());
            $query->bindParam(6, $clienteDto->getCelular());
            $query->execute();
            $cnn->commit();
            $cnn->beginTransaction();
            $query2 = $cnn->prepare("INSERT INTO clientes (Nit, RazonSocial, Direccion, Telefono,
                                    EmailCliente, IdTipoCliente, IdActividadCliente,
                                    IdClasificacionCliente, IdLugarCliente, CedulaCliente, CedulaEmpleadoClientes,
                                    nombreComercial,estadoCliente, diaCierreFacturacion, plazoPago)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $query2->bindParam(1, $clienteDto->getNit());
            $query2->bindParam(2, $clienteDto->getRazonSocial());
            $query2->bindParam(3, $clienteDto->getDireccion());
            $query2->bindParam(4, $clienteDto->getTelefono());
            $query2->bindParam(5, $clienteDto->getEmail2());
            $query2->bindParam(6, $clienteDto->getIdTipo());
            $query2->bindParam(7, $clienteDto->getIdActividad());
            $query2->bindParam(8, $clienteDto->getIdClasificacion());
            $query2->bindParam(9, $clienteDto->getIdLugar());
            $query2->bindParam(10, $clienteDto->getCedula());
            $query2->bindParam(11, $clienteDto->getCedulaEmpleado());
            $query2->bindParam(12, $clienteDto->getNombreComercial());
            $query2->bindParam(13, $clienteDto->getEstado());
            $query2->bindParam(14, $clienteDto->getDiaCierre());
            $query2->bindParam(15, $clienteDto->getPlazoPago());
            $query2->execute();
            $cnn->commit();
            $cnn->beginTransaction();
            $query3 = $cnn->prepare("INSERT INTO rolesusuarios VALUES ('2', ?)");
            $query3->bindParam(1, $clienteDto->getCedula());
            $query3->execute();
            $cnn->commit();
            $nit=$clienteDto->getNit();
            if($clienteDto->getEstado()=="Activo"){
                $activo='El cliente ha sido activado.&activo=true';
            }else{
                $activo='Recuerde que el cliente está inactivo y debe ser activado por un coordinador.&activo=false';
            }
            $this->mensaje="Cliente registrado con éxito en la base de datos. ".$activo."&criterio=clientes.Nit&busqueda=".$nit."&comobuscar=1&error=false&nit=".$nit;
        } catch (Exception $ex){
            $cnn->rollBack();
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=El cliente NO ha sido registrado en la base de datos.';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function registrarSoloEmpresa(ClientesDto $clienteDto, PDO $cnn){
        try{
            $cnn->beginTransaction();
            $query2 = $cnn->prepare("INSERT INTO clientes (Nit, RazonSocial, Direccion, Telefono,
                                    EmailCliente, IdTipoCliente, IdActividadCliente,
                                    IdClasificacionCliente, IdLugarCliente, CedulaCliente, CedulaEmpleadoClientes,
                                    nombreComercial,estadoCliente, diaCierreFacturacion, plazoPago)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $query2->bindParam(1, $clienteDto->getNit());
            $query2->bindParam(2, $clienteDto->getRazonSocial());
            $query2->bindParam(3, $clienteDto->getDireccion());
            $query2->bindParam(4, $clienteDto->getTelefono());
            $query2->bindParam(5, $clienteDto->getEmail2());
            $query2->bindParam(6, $clienteDto->getIdTipo());
            $query2->bindParam(7, $clienteDto->getIdActividad());
            $query2->bindParam(8, $clienteDto->getIdClasificacion());
            $query2->bindParam(9, $clienteDto->getIdLugar());
            $query2->bindParam(10, $clienteDto->getCedula());
            $query2->bindParam(11, $clienteDto->getCedulaEmpleado());
            $query2->bindParam(12, $clienteDto->getNombreComercial());
            $query2->bindParam(13, $clienteDto->getEstado());
            $query2->bindParam(14, $clienteDto->getDiaCierre());
            $query2->bindParam(15, $clienteDto->getPlazoPago());
            $query2->execute();
            $cnn->commit();
            $nit=$clienteDto->getNit();
            if($clienteDto->getEstado()=="Activo"){
                $activo='El cliente ha sido activado.&activo=true';
            }else{
                $activo='Recuerde que el cliente está inactivo y debe ser activado por un coordinador.&activo=false';
            }
            $this->mensaje="Cliente registrado con éxito en la base de datos. ".$activo."&criterio=clientes.Nit&busqueda=".$nit."&comobuscar=1&error=false&nit=".$nit;
        } catch (Exception $ex){
            $cnn->rollBack();
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=El cliente NO ha sido registrado en la base de datos.';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarTodos(PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
            try {
                $listarClientes = 'SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                join departamentos on departamentos.idDepartamento=lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                ORDER BY personas.FechaCreacionPersona DESC';
                $query = $cnn->prepare($listarClientes);
                $query->execute();
                $this->mensaje=$query->fetchAll();
            } catch
            (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage();
            }
            return $this->mensaje;
        }elseif($_SESSION['datosLogin']['NombreRol']=='Asesor') {
            try {
                $listarClientes = 'SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*, concat(personaempleado.Nombres," ",
                                personaempleado.Apellidos) as Empleado, personaempleado.EmailPersona as EmailEmpleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.CelularPersona as CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.CedulaEmpleadoClientes = '.$_SESSION['datosLogin']['id'].'
                                ORDER BY personas.FechaCreacionPersona DESC';
                $query = $cnn->prepare($listarClientes);
                $query->execute();
                //unset($_SESSION['conteo']);
                //$_SESSION['conteo'] = $query->rowCount();
                //return $query->fetchAll();
                $this->mensaje=$query->fetchAll();
            } catch
            (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage();
            }
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerPersona($cedulaPersona, PDO $cnn){
        try {
            $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND personas.CedulaPersona = ?');
            $query->bindParam(1, $cedulaPersona);
            $query->execute();
            $this->mensaje = $query->fetch();
        } catch (Exception $ex) {
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&mensaje=Error buscando el usuario en la base de datos.&error=true';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerDatosPersona($cedulaPersona, PDO $cnn){
        try {
            $query = $cnn->prepare('select personas.*, roles.* FROM personas
join rolesusuarios on personas.CedulaPersona=rolesusuarios.CedulaRolesUsuarios
join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios and personas.CedulaPersona=?
GROUP by personas.CedulaPersona');
            $query->bindParam(1, $cedulaPersona);
            $query->execute();
            $this->mensaje = $query->fetch();
        } catch (Exception $ex) {
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&mensaje=Error buscando el usuario en la base de datos.&error=true';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function listarCorreosNit($nit, PDO $cnn){
        try {
            $query = $cnn->prepare('select clientes.EmailCliente as emails, clientes.RazonSocial as detalle from clientes WHERE clientes.Nit=?
                                  UNION
                                  SELECT personas.EmailPersona, concat(personas.Nombres,\' \',personas.Apellidos) FROM personas JOIN
                                  clientes on personas.CedulaPersona=clientes.CedulaCliente and
                                  clientes.Nit=?
                                  UNION
                                  SELECT puntosentrega.correoPuntoEntrega, concat(puntosentrega.nombrePuntoEntrega,\' \', puntosentrega.nombreContactoPuntoEntrega)
                                  from puntosentrega JOIN clientes on clientes.Nit=puntosentrega.nitEmpresaPuntoEntrega
                                  AND clientes.Nit=?
                                  UNION
                                  SELECT areascliente.emailArea, concat(areascliente.nombreAreas,\' \',areascliente.nombreContactoAreas)
                                  FROM areascliente JOIN clientes on clientes.Nit=areascliente.nitClienteAreas
                                  AND clientes.Nit=?');
            $query->bindParam(1, $nit);
            $query->bindParam(2, $nit);
            $query->bindParam(3, $nit);
            $query->bindParam(4, $nit);
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&mensaje=Error buscando el usuario en la base de datos.&error=true';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerPersonaById($idPersona, PDO $cnn){
        try {
            $query = $cnn->prepare('SELECT personas.*, roles.*, rolesusuarios.*
                                    from personas join rolesusuarios on personas.CedulaPersona=rolesusuarios.CedulaRolesUsuarios
                                    join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios
                                    AND personas.IdPersona = ?');
            $query->bindParam(1, $idPersona);
            $query->execute();
            $this->mensaje = $query->fetch();
        } catch (Exception $ex) {
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&mensaje=Error buscando el usuario en la base de datos.&error=true';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerCliente($IdCliente, PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
            try {
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.IdCliente = ?');
                $query->bindParam(1, $IdCliente);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando el cliente en la base de datos.&error=true';;
            }
        } else {
            try{
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.IdCliente = ?
                                AND clientes.CedulaEmpleadoClientes = ' . $_SESSION['datosLogin']['id']);
                $query->bindParam(1, $IdCliente);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando el cliente en la base de datos.&error=true';;
            }
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerClienteIdPersona($idPersona, PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
            try {
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND personas.IdPersona = ?');
                $query->bindParam(1, $idPersona);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando el cliente en la base de datos.&error=true';;
            }
        } else {
            try{
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND personas.IdPersona = ?
                                AND clientes.CedulaEmpleadoClientes = ' . $_SESSION['datosLogin']['id']);
                $query->bindParam(1, $idPersona);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando el cliente en la base de datos.&error=true';;
            }
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function listarPersonasConRol(PDO $cnn){
        try{
            $query = $cnn->prepare('SELECT personas.*, roles.*, rolesusuarios.*
                                    FROM personas JOIN rolesusuarios ON
                                    personas.CedulaPersona = rolesusuarios.CedulaRolesUsuarios
                                    JOIN roles ON rolesusuarios.IdRolRolesUsuarios = roles.IdRol
                                    ORDER BY personas.FechaCreacionPersona DESC ');
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex){
            $this->mensaje='&detalleerror='.$ex->getMessage().'&encontrados=false&error=true';
        };
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarCedulaPersona($cedulaPersona, PDO $cnn){
        try{
            $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                    concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                    tiposempresas.*, actividadesempresas.*, clasificaciones.*
                                    FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                    join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                    JOIN departamentos on departamentos.idDepartamento = lugares.idDepartamentoLugar
                                    JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                    JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                    JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                    AND personas.CedulaPersona = "'.$cedulaPersona.'"
                                    ORDER BY clientes.Nit ASC');
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex){
            $this->mensaje='&detalleerror='.$ex->getMessage().'&encontrados=false&error=true';
        };
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarCliente($criterio, $busqueda, $comobuscar, PDO $cnn){
        switch ($comobuscar) {
            case 1:
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
                    try{
                        $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                join departamentos on lugares.idDepartamentoLugar = departamentos.idDepartamento
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND '.$criterio.' = "'.$busqueda.'"
                                ORDER BY personas.FechaCreacionPersona DESC');
                        $query->execute();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex){
                        $this->mensaje ='&detalleerror='.$ex->getMessage().'&encontrados=0';
                    };
                }else{
                    try{
                        $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                join departamentos on lugares.idDepartamentoLugar = departamentos.idDepartamento
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND '.$criterio.' = "'.$busqueda.'"
                                AND clientes.CedulaEmpleadoClientes = '.$_SESSION['datosLogin']['id'].'
                                ORDER BY personas.FechaCreacionPersona DESC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        //return $query->fetchAll();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex){
                        $this->mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                    };
                }
                break;
            case 2;
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                join departamentos on lugares.idDepartamentoLugar = departamentos.idDepartamento
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                            AND ' . $criterio . ' like "%' . $busqueda . '%"
                                            ORDER BY personas.FechaCreacionPersona DESC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje='&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                }else{
                    try {
                        $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.*, departamentos.*,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.Nombres as nombresEmpleado, personaempleado.Apellidos as apellidosEmpleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                join departamentos on lugares.idDepartamentoLugar = departamentos.idDepartamento
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                            AND ' . $criterio . ' like "%' . $busqueda . '%"
                                            AND clientes.CedulaEmpleadoClientes = '.$_SESSION['datosLogin']['id'].'
                                            ORDER BY personas.FechaCreacionPersona DESC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje='&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                }
                break;
            default:
                $this->mensaje='Opción inválida para Cómo búscar';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function modificarCliente(ClientesDto $clienteDto, PDO $cnn){

        try{
            $query = $cnn->prepare("UPDATE personas set CedulaPersona = ?, Nombres = ?,
                                    Apellidos = ?, EmailPersona = ?,
                                    CelularPersona = ? WHERE personas.IdPersona = ?");
            $query->bindParam(1, $clienteDto->getCedula());
            $query->bindParam(2, $clienteDto->getNombres());
            $query->bindParam(3, $clienteDto->getApellidos());
            $query->bindParam(4, $clienteDto->getEmail1());
            $query->bindParam(5, $clienteDto->getCelular());
            $query->bindParam(6, $clienteDto->getIdPersona());
            $query->execute();
            $query = $cnn->prepare("UPDATE clientes set Nit = ?, RazonSocial = ?, Direccion = ?, Telefono = ?,
                                    EmailCliente = ?, IdTipoCliente = ?, IdActividadCliente = ?,
                                    IdClasificacionCliente = ?, IdLugarCliente = ?, CedulaCliente = ?,
                                    CedulaEmpleadoClientes=?, estadoCliente = ?, nombreComercial = ?,
                                    diaCierreFacturacion=?, plazoPago=? where clientes.IdCliente = ? ");
            $query->bindParam(1, $clienteDto->getNit());
            $query->bindParam(2, $clienteDto->getRazonSocial());
            $query->bindParam(3, $clienteDto->getDireccion());
            $query->bindParam(4, $clienteDto->getTelefono());
            $query->bindParam(5, $clienteDto->getEmail2());
            $query->bindParam(6, $clienteDto->getIdTipo());
            $query->bindParam(7, $clienteDto->getIdActividad());
            $query->bindParam(8, $clienteDto->getIdClasificacion());
            $query->bindParam(9, $clienteDto->getIdLugar());
            $query->bindParam(10, $clienteDto->getCedula());
            $query->bindParam(11, $clienteDto->getCedulaEmpleado());
            $query->bindParam(12, $clienteDto->getEstado());
            $query->bindParam(13, $clienteDto->getNombreComercial());
            $query->bindParam(14, $clienteDto->getDiaCierre());
            $query->bindParam(15, $clienteDto->getPlazoPago());
            $query->bindParam(16, $clienteDto->getIdCliente());
            $query->execute();
            if($clienteDto->getEstado()=="Activo"){
                $activo='El cliente ha sido activado.&activo=true';
            }else{
                $activo='Recuerde que el cliente está inactivo y solo puede ser activado por un coordinador.&activo=false';
            }
            $this->mensaje="Cliente actualizado con éxito en la base de datos. ".$activo."&criterio=clientes.Nit&busqueda=".$clienteDto->getNit()."&comobuscar=1&error=false&nit=".$clienteDto->getNit();
        } catch (Exception $ex){
            $cnn->rollBack();
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=El cliente NO ha sido actualizado en la base de datos.';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function cambiarEstado($nit, PDO $cnn){
        try{
            $cnn->beginTransaction();
            $query = $cnn->prepare("select clientes.estadoCliente from clientes WHERE clientes.Nit = ?");
            $query->bindParam(1, $nit);
            $query->execute();
            $estado=$query->fetch();
            if($estado['estadoCliente']=='Activo'){
                $query = $cnn->prepare("UPDATE clientes SET clientes.estadoCliente = 'Inactivo' WHERE clientes.Nit = ?");
                $query->bindParam(1, $nit);
                $query->execute();
                $this->mensaje="&mensaje=Cliente actualizado en la base de datos.&encontrados=true&criterio=clientes.Nit&busqueda=".$nit."&comobuscar=1";
            }elseif($estado['estadoCliente']=='Inactivo'){
                $query = $cnn->prepare("UPDATE clientes SET clientes.estadoCliente = 'Activo' WHERE clientes.Nit = ?");
                $query->bindParam(1, $nit);
                $query->execute();
                $this->mensaje="&mensaje=Cliente actualizado en la base de datos.&encontrados=true&criterio=clientes.Nit&busqueda=".$nit."&comobuscar=1";
            }else{
                $this->mensaje='No se envió el valor correcto de la cédula';
            };
            $cnn->commit();
        } catch (Exception $ex){
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=El cliente NO ha sido actualizado en la base de datos.';
        }
        $cnn = null;
        return json_encode($this->mensaje);
    }

    public function reestablecerContrasenia($idPersona, PDO $cnn){

        try{
            $query = $cnn->prepare("UPDATE personas SET personas.Contrasenia = MD5(personas.CedulaPersona) WHERE personas.IdPersona = ?");
            $query->bindParam(1, $idPersona);
            $query->execute();
            $this->mensaje="Se ha reestablecido la contrase&ntilde;a del cliente a su estado inicial (c&eacute;dula)";
            //$this->mensaje=header("Location: ../views/buscarClientes.php?mensaje=Se ha reestablecido la contraseña del cliente a su estado inicial (cédula).&error=false");
        } catch (Exception $ex){
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=El cliente NO ha sido actualizado en la base de datos.';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function existeNit($nit, PDO $cnn){
        $this->mensaje = "";
        try{
            $query = $cnn->prepare("SELECT COUNT(*) as existente from clientes where clientes.Nit = ?");
            $query->bindParam(1, $nit);
            $query->execute();
            $this->mensaje = $query->fetch();
        }catch (Exception $ex){
            $this->mensaje='&detalleerror='.$ex->getMessage().'$error=true&mensaje=Error en la consulta';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function existeCedula($cedula, PDO $cnn){
        $this->mensaje = "";
        try{
            $query = $cnn->prepare("SELECT COUNT(*) as existente from personas where personas.CedulaPersona = ?");
            $query->bindParam(1, $cedula);
            $query->execute();
            $this->mensaje = $query->fetch();
        }catch (Exception $ex){
            $this->mensaje='&detalleerror='.$ex->getMessage().'$error=true&mensaje=Error en la consulta';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function contarClientesInactivos(PDO $cnn){
        try{
            $query = $cnn->prepare("select COUNT(*) as inactivos from clientes join personas on
                                    clientes.CedulaCliente = personas.CedulaPersona and clientes.estadoCliente = 'Inactivo'");
            $query->execute();
            $this->mensaje = $query->fetch();
        }catch (Exception $ex){
            $this->mensaje='&detalleerror='.$ex->getMessage().'$error=true&mensaje=Error en la consulta';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerClientesActivos(PDO $cnn)
    {
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
            try {
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.NombreLugar,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.estadoCliente = "Activo"');
                $query->execute();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        } else {
            try{
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.NombreLugar,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.estadoCliente = "Activo"
                                AND clientes.CedulaEmpleadoClientes = ' . $_SESSION['datosLogin']['id']);
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }
            catch(Exception $ex) {
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function obtenerClientesInactivos(PDO $cnn)
    {
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
            try {
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.NombreLugar,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.estadoCliente = "Inactivo"');
                $query->execute();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        } else {
            try{
                $query = $cnn->prepare('SELECT personas.*, clientes.*, lugares.NombreLugar,
                                concat(personas.Nombres," ",personas.Apellidos) as Contacto,
                                tiposempresas.*, actividadesempresas.*, clasificaciones.*, empleados.*,
                                concat(personaempleado.Nombres," ",personaempleado.Apellidos) as Empleado,
                                personaempleado.EmailPersona as EmailEmpleado, personaempleado.CelularPersona as
                                CelularEmpleado, personaempleado.EstadoPersona as EstadoEmpleado
                                FROM personas JOIN clientes on personas.CedulaPersona = clientes.CedulaCliente
                                JOIN empleados on empleados.CedulaEmpleado = clientes.CedulaEmpleadoClientes
                                join personas as personaempleado on personaempleado.CedulaPersona = empleados.CedulaEmpleado
                                join lugares on clientes.IdLugarCliente = lugares.IdLugar
                                JOIN tiposempresas on tiposempresas.IdTipo = clientes.IdTipoCliente
                                JOIN actividadesempresas on actividadesempresas.IdActividad = clientes.IdActividadCliente
                                JOIN clasificaciones on clasificaciones.IdClasificacion = clientes.IdClasificacionCliente
                                AND clientes.estadoCliente = "Inactivo"
                                AND clientes.CedulaEmpleadoClientes = ' . $_SESSION['datosLogin']['id']);
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }
            catch(Exception $ex) {
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function totalComprasClientes(PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
            try{
                $query=$cnn->prepare("select sum(cotizaciones.ValorTotalCotizacion) as comprado,
                              clientes.Nit, clientes.nombreComercial
                              from cotizaciones JOIN clientes on
                              cotizaciones.NitClienteCotizaciones=clientes.Nit
                               join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                               and pedidos.EstadoPedido='Autorizado'
                               and
                              cotizaciones.EstadoCotizacion='Pedido' GROUP by clientes.Nit ORDER by comprado desc;");
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }catch(Exception $ex){
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        }elseif($_SESSION['datosLogin']['NombreRol']=='Asesor'){
            try{
                /*
                 select sum(cotizaciones.ValorTotalCotizacion) as comprado, clientes.Nit, clientes.nombreComercial from cotizaciones JOIN clientes on cotizaciones.NitClienteCotizaciones=clientes.Nit and cotizaciones.EstadoCotizacion='Pedido' AND cotizaciones.CedulaEmpleadoCotizaciones=234234 GROUP by cotizaciones.NitClienteCotizaciones
                 */
                $query=$cnn->prepare("select sum(cotizaciones.ValorTotalCotizacion) as comprado,
                              clientes.Nit, clientes.nombreComercial
                              from cotizaciones JOIN clientes on
                              cotizaciones.NitClienteCotizaciones=clientes.Nit
                              join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                               and pedidos.EstadoPedido='Autorizado'
                              and
                              cotizaciones.EstadoCotizacion='Pedido'
                              AND cotizaciones.CedulaEmpleadoCotizaciones=?
                              GROUP by clientes.Nit ORDER by comprado desc;");
                $query->bindParam(1, $_SESSION['datosLogin']['id']);
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }catch(Exception $ex){
                $this->mensaje='&detalleerror=' . $ex->getMessage() . '&mensaje=Error buscando los clientes en la base de datos.&error=true';;
            }
        }
        $cnn=null;
        return $this->mensaje;
    }



}