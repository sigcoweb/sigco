<?php
class installDao
{
    private $mensaje;
    public function installScript($script, PDO $cnn){
        try{
            $cnn->beginTransaction();
            $query = $cnn->prepare($script);
            $query->execute();
            $cnn->commit();
            $this->mensaje=1;
        }catch(Exception $ex){
            $cnn->rollBack();
            $this->mensaje="Error: ".$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function createAdmin($cedula, $email, $pass, PDO $cnn){
        try{
            $cnn->beginTransaction();
            $script="INSERT INTO personas (IdPersona, CedulaPersona, Nombres, Apellidos, EmailPersona, EstadoPersona,
 Contrasenia, RutaImagenPersona, CelularPersona, FechaCreacionPersona) VALUES
(1, ".$cedula.", 'Admin', 'System', '".$email."', 'Activo', MD5(".$pass."), 'admon.png', ".$cedula.", CURRENT_TIMESTAMP)";
            $query = $cnn->prepare($script);
            $query->execute();
            $script="INSERT INTO empleados (CedulaEmpleado, Cargo, idLugarEmpleado, fechaNacimiento) VALUES
(".$cedula.", 'Administrador', 149, CURRENT_TIMESTAMP)";
            $query = $cnn->prepare($script);
            $query->execute();
            $script="INSERT INTO rolesusuarios (IdRolRolesUsuarios, CedulaRolesUsuarios) VALUES
(1, ".$cedula.")";
            $query = $cnn->prepare($script);
            $query->execute();
            $cnn->commit();
            $this->mensaje=1;
        }catch(Exception $ex){
            $cnn->rollBack();
            $this->mensaje="Error: ".$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function cantidadTablas($base, PDO $cnn){
        try {
            $cnn->beginTransaction();
            $query = $cnn->prepare("SELECT COUNT(*) as cantidad
                                    FROM information_schema.tables
                                WHERE table_schema = '".$base."'");
            $query->execute();
            $cnn->commit();
            $cantidad = $query->fetch();
            $this->mensaje = $cantidad['cantidad'];
        }catch(Exception $ex){
            $this->mensaje='Error: '.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function verifyAdmin($admin,$email,$pass, PDO $cnn){
        try {
            $cnn->beginTransaction();
            $query = $cnn->prepare('SELECT COUNT(*) as cantidad
                                    FROM personas join empleados on personas.CedulaPersona = empleados.CedulaEmpleado
                                    join rolesusuarios on personas.CedulaPersona = rolesusuarios.CedulaRolesUsuarios
                                    JOIN roles on rolesusuarios.IdRolRolesUsuarios = roles.IdRol
                                    AND personas.CedulaPersona= ? and personas.EmailPersona=? and personas.Contrasenia=?');
            $query->bindParam(1, $admin);
            $query->bindParam(2, $email);
            $query->bindParam(3, $pass);
            $query->execute();
            $cnn->commit();
            $cantidad = $query->fetch();
            $this->mensaje = $cantidad['cantidad'];
        }catch(Exception $ex){
            $this->mensaje='Error: '.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

}