<?php
include_once '../utilities/installDto.php';
class ImportarDao
{

    public function importarDatos($archivo, $tabla, $separador, $encabezado, PDO $cnn){
        $mensaje = "";
        try{
            $query=$cnn->prepare("LOAD DATA INFILE ? INTO TABLE ".$tabla." FIELDS TERMINATED BY '".$separador."' LINES TERMINATED BY '\n' IGNORE ".$encabezado." ROWS");
            $query->bindParam(1, $archivo);
            $query->execute();
            $mensaje="Información importada exitosamente en la tabla ".$tabla;
        }catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true';
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarTablas(PDO $cnn){
        try{
            $query = $cnn->prepare("show tables");
            $query->execute();
            $mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function listarCampos($nombreTabla,PDO $cnn){
        include_once '../utilities/config.php';
        $baseTabla=$baseName.'.'.$nombreTabla;
        try{
            $query = $cnn->prepare('show COLUMNS FROM '.$baseTabla);
            $query->execute();
            $mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }


}