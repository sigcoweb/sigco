<?php

/**
 * Created by PhpStorm.
 * User: iStam
 * Date: 2/09/15
 * Time: 7:59 PM
 */
class RolesUsuariosDao
{

    public function registrarRolUsuario(RolesUsuariosDto $dto,PDO $cnn) {
        try {
            $query2= $cnn->prepare("INSERT INTO rolesusuarios (IdRolRolesUsuarios,CedulaRolesUsuarios) VALUES (?,?)");
            $query2->bindParam(1,$dto->getRol());
            $query2->bindParam(2,$dto->getCedula());
            $query2->execute();
            $mensaje="Se ha realizado la asignación de rol exitosamente";
        } catch (Exception $ex) {
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=1&mensaje=La asignación de rol NO se ha podido realizar';
        }
        $cnn =null;
        return $mensaje;
    }

    public function listarMisRoles($cedula,PDO $cnn) {
        try {
            $query2= $cnn->prepare("select roles.*
              from personas join rolesusuarios on personas.CedulaPersona = rolesusuarios.CedulaRolesUsuarios
              join roles on rolesusuarios.IdRolRolesUsuarios = roles.IdRol AND personas.CedulaPersona = ? ORDER BY roles.IdRol ASC ");
            $query2->bindParam(1, $cedula);
            $query2->execute();
            $mensaje=$query2->fetchAll();
        } catch (Exception $ex) {
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=1&mensaje=La asignación de rol NO se ha podido realizar';
        }
        $cnn =null;
        return $mensaje;
    }

}