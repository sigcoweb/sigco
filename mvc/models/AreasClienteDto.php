<?php

class AreasClienteDto
{
private $nombreArea;
private $nitCliente;
private $nombreContactoArea;
private $telefonoArea;
private $emailArea;
private $observacionesArea;
private $nombreAntiguo;
private $nitAntiguo;

    /**
     * @return mixed
     */
    public function getNitAntiguo()
    {
        return $this->nitAntiguo;
    }

    /**
     * @param mixed $nitAntiguo
     */
    public function setNitAntiguo($nitAntiguo)
    {
        $this->nitAntiguo = $nitAntiguo;
    }

    /**
     * @return mixed
     */
    public function getNombreAntiguo()
    {
        return $this->nombreAntiguo;
    }

    /**
     * @param mixed $nombreAntiguo
     */
    public function setNombreAntiguo($nombreAntiguo)
    {
        $this->nombreAntiguo = $nombreAntiguo;
    }


    /**
     * AreasClienteDto constructor.
     * @param $nombreArea
     * @param $nitCliente
     * @param $nombreContactoArea
     * @param $telefonoArea
     * @param $emailArea
     * @param $observacionesArea
     */
    public function __construct($nombreArea, $nitCliente, $nombreContactoArea, $telefonoArea, $emailArea, $observacionesArea)
    {
        $this->nombreArea = $nombreArea;
        $this->nitCliente = $nitCliente;
        $this->nombreContactoArea = $nombreContactoArea;
        $this->telefonoArea = $telefonoArea;
        $this->emailArea = $emailArea;
        $this->observacionesArea = $observacionesArea;
    }

    /**
     * @return mixed
     */
    public function getNombreArea()
    {
        return $this->nombreArea;
    }

    /**
     * @param mixed $nombreArea
     */
    public function setNombreArea($nombreArea)
    {
        $this->nombreArea = $nombreArea;
    }

    /**
     * @return mixed
     */
    public function getNitCliente()
    {
        return $this->nitCliente;
    }

    /**
     * @param mixed $nitCliente
     */
    public function setNitCliente($nitCliente)
    {
        $this->nitCliente = $nitCliente;
    }

    /**
     * @return mixed
     */
    public function getNombreContactoArea()
    {
        return $this->nombreContactoArea;
    }

    /**
     * @param mixed $nombreContactoArea
     */
    public function setNombreContactoArea($nombreContactoArea)
    {
        $this->nombreContactoArea = $nombreContactoArea;
    }

    /**
     * @return mixed
     */
    public function getTelefonoArea()
    {
        return $this->telefonoArea;
    }

    /**
     * @param mixed $telefonoArea
     */
    public function setTelefonoArea($telefonoArea)
    {
        $this->telefonoArea = $telefonoArea;
    }

    /**
     * @return mixed
     */
    public function getEmailArea()
    {
        return $this->emailArea;
    }

    /**
     * @param mixed $emailArea
     */
    public function setEmailArea($emailArea)
    {
        $this->emailArea = $emailArea;
    }

    /**
     * @return mixed
     */
    public function getObservacionesArea()
    {
        return $this->observacionesArea;
    }

    /**
     * @param mixed $observacionesArea
     */
    public function setObservacionesArea($observacionesArea)
    {
        $this->observacionesArea = $observacionesArea;
    }

}