<?php

class EmpleadoDao
{
    private $mensaje;
    public function registrarEmpleado(EmpleadoDto $dto,$rol, PDO $cnn)
    {
        try {
            $query = $cnn->prepare("INSERT INTO personas
VALUES(DEFAULT ,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)");
            $query->bindParam(1, $dto->getIdUsuario());
            $query->bindParam(2, $dto->getNombres());
            $query->bindParam(3, $dto->getApellidos());
            $query->bindParam(4, $dto->getEmail());
            $query->bindParam(5, $dto->getEstado());
            $query->bindParam(6, $dto->getContrasenia());
            $query->bindParam(7, $dto->getRutaimagen());
            $query->bindParam(8, $dto->getCelular());
            $query->execute();
            $query2 = $cnn->prepare("INSERT INTO empleados VALUES (?,?,?,?)");
            $query2->bindParam(1, $dto->getIdUsuario());
            $query2->bindParam(2, $dto->getEmpleo());
            $query2->bindParam(3, $dto->getIdLugar());
            $query2->bindParam(4, $dto->getFechaNacimiento());
            $query2->execute();
            $query3 = $cnn->prepare("INSERT INTO rolesusuarios VALUES (?,?)");
            $query3->bindParam(1, $rol);
            $query3->bindParam(2, $dto->getIdUsuario());
            $query3->execute();
            $this->mensaje="Empleado registrado exitosamente";
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror=' . $ex->getMessage() . '&error=1&mensaje=El empleado NO ha sido registrado en la base de datos.';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarDocumentos(PDO $cnn)
    {
        try {
            $query = $cnn->prepare("select empleados.*, empleados.CedulaEmpleado as 'cc', personas.* from empleados JOIN
                                    personas on empleados.CedulaEmpleado = personas.CedulaPersona ORDER BY empleados.CedulaEmpleado");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function listarRoles(PDO $cnn)
    {
        try {
            $query = $cnn->prepare("Select * from roles");
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarMetas(PDO $cnn)
    {
        try {
            $query = $cnn->prepare("Select * from metas order by IdMeta desc");
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }


    public function modificarUsuario(EmpleadoDto $obj, $idPersona, $rol, PDO $cnn)
    {
        include_once '../facades/FacadeEmpleado.php';
        $empleadofaca=new FacadeEmpleado();
        $empleado=$empleadofaca->obtenerUsuario($idPersona);
        $originalCc=$empleado['CedulaPersona'];
        $originalCargo=$empleado['Cargo'];
        $originalRol=$empleado['IdRol'];
        try {
            $query = $cnn->prepare("UPDATE personas SET Nombres=?,Apellidos=?,EmailPersona=?,
                            EstadoPersona=?,Contrasenia=?,RutaImagenPersona=?,CelularPersona=?,
                            CedulaPersona=? where IdPersona=?");
            $query->bindParam(1, $obj->getNombres());
            $query->bindParam(2, $obj->getApellidos());
            $query->bindParam(3, $obj->getEmail());
            $query->bindParam(4, $obj->getEstado());
            $query->bindParam(5, $obj->getContrasenia());
            $query->bindParam(6, $obj->getRutaimagen());
            $query->bindParam(7, $obj->getCelular());
            $query->bindParam(8, $obj->getIdUsuario());
            $query->bindParam(9, $idPersona);
            $query->execute();
            $query2 = $cnn->prepare("Update empleados set Cargo=?, CedulaEmpleado=?, fechaNacimiento=?,
                                      idLugarEmpleado=? where Cargo=? and CedulaEmpleado=?");
            $query2->bindParam(1, $obj->getEmpleo());
            $query2->bindParam(2, $obj->getIdUsuario());
            $query2->bindParam(3, $obj->getFechaNacimiento());
            $query2->bindParam(4, $obj->getIdLugar());
            $query2->bindParam(5, $originalCargo);
            $query2->bindParam(6, $originalCc);
            $query2->execute();
            $query3 = $cnn->prepare("Update rolesusuarios set IdRolRolesUsuarios=?, CedulaRolesUsuarios=? where IdRolRolesUsuarios=? and CedulaRolesUsuarios=?");
            $query3->bindParam(1, $rol);
            $query3->bindParam(2, $obj->getIdUsuario());
            $query3->bindParam(3, $originalRol);
            $query3->bindParam(4, $obj->getIdUsuario());
            $query3->execute();
            $this->mensaje= "Empleado actualizado exitosamente";
        } catch (Exception $ex) {
            $this->mensaje='No se ha actualizado la info.&error=true&detalleerror='.$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function buscarUsuario($idEmpleado, PDO $cnn)
    {

        try {

            $query = $cnn->prepare('SELECT personas.*, empleados.*, rolesusuarios.*, roles.*, lugares.*, departamentos.*
                      FROM personas join empleados
                      on personas.CedulaPersona=empleados.CedulaEmpleado
                      JOIN rolesusuarios ON personas.CedulaPersona=rolesusuarios.CedulaRolesUsuarios
                      JOIN roles on rolesusuarios.IdRolRolesUsuarios=roles.IdRol JOIN
                      lugares on lugares.IdLugar=empleados.idLugarEmpleado JOIN
                      departamentos ON departamentos.idDepartamento=lugares.idDepartamentoLugar
                      AND personas.IdPersona=?');
            $query->bindParam(1, $idEmpleado);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarUsuarios(PDO $cnn)
    {
        try {

            $query = $cnn->prepare("Select empleados.*, personas.*, roles.*, rolesusuarios.* from personas
                                    join empleados on personas.CedulaPersona = empleados.CedulaEmpleado join
                                    rolesusuarios on rolesusuarios.CedulaRolesUsuarios=empleados.CedulaEmpleado
                                    join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios
");

            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function listarEmpleadosAsesores(PDO $cnn)
    {

        try {
            $query = $cnn->prepare("Select empleados.*, personas.*, roles.*, rolesusuarios.* from personas
                                    join empleados on personas.CedulaPersona = empleados.CedulaEmpleado join
                                    rolesusuarios on rolesusuarios.CedulaRolesUsuarios=empleados.CedulaEmpleado
                                    join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios and rolesusuarios.IdRolRolesUsuarios=3");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje= 'Error' . $ex->getMessage();
        }
        return $this->mensaje;
    }

    public function generar_clave(){
        $an = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM.,:;";
        $su = strlen($an) - 1;
        return substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1) .
        substr($an, rand(0, $su), 1);
    }

    public function verificar($user, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT count(*) as "existe" FROM personas WHERE CedulaPersona=?');
            $query->bindParam(1, $user);
            $query->execute();
            $resul = $query->fetch();
            if ($resul['existe'] == 0) {
                $this->mensaje=false;
            } else {
                $correo = $cnn->prepare("Select Nombres,Apellidos,EmailPersona as 'mail' from personas where CedulaPersona=?");
                $correo->bindParam(1, $user);
                $correo->execute();
                $this->mensaje=$correo->fetch();
            }
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function login($user, $pass, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT count(*) as "existe" FROM personas WHERE CedulaPersona=? AND Contrasenia=?');
            $query->bindParam(1, $user);
            $query->bindParam(2, md5($pass));
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function rol($user, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select IdRolRolesUsuarios as "rol" from rolesusuarios where CedulaRolesUsuarios=?');
            $query->bindParam(1, $user);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function datosLogin($user, PDO $cnn)
    {
        try {
            /*
             * $query = $cnn->prepare("SELECT CedulaEmpleado as 'id',Nombres,Apellidos,EmailPersona,EstadoPersona,RutaImagenPersona,empleados.CedulaEmpleado
                                    as 'idempleado',roles.NombreRol FROM personas
                                    JOIN empleados on empleados.CedulaEmpleado=personas.CedulaPersona
                                    JOIN rolesusuarios on rolesusuarios.CedulaRolesUsuarios=personas.CedulaPersona
                                    JOIN roles on rolesusuarios.IdRolRolesUsuarios=roles.IdRol WHERE personas.CedulaPersona=?");
             */
            $query = $cnn->prepare("
select personas.CedulaPersona as 'id', personas.CedulaPersona as 'idempleado', personas.*, roles.*, rolesusuarios.*
from personas join rolesusuarios ON personas.CedulaPersona=rolesusuarios.CedulaRolesUsuarios
 join roles on rolesusuarios.IdRolRolesUsuarios = roles.IdRol
 and personas.CedulaPersona=? ORDER BY roles.IdRol ASC
");
            $query->bindParam(1, $user);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function obtenerTitulos($rol, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from permisoscategorias
                                    join permisos on permisos.IdCategoria=permisoscategorias.IdCategoria
                                    join permisosroles on permisosroles.IdPermisoPermisosRoles=permisos.IdPermiso
                                    where permisosroles.IdRolPermisosRoles=? group by permisoscategorias.IdCategoria');
            $query->bindParam(1, $rol);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function obtenerSubTitulos($id, $rol, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from permisos join permisosroles on permisosroles.IdPermisoPermisosRoles=permisos.IdPermiso
                                    where permisos.IdCategoria=? AND permisosroles.IdRolPermisosRoles=? group by permisos.NombrePagina');
            $query->bindParam(1, $id);
            $query->bindParam(2, $rol);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }


    public function buscarEmpleadoCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {

        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare('Select empleados.*, personas.*, roles.*, rolesusuarios.* from personas
                                            join empleados on personas.CedulaPersona = empleados.CedulaEmpleado
                                            join rolesusuarios on rolesusuarios.CedulaRolesUsuarios=empleados.CedulaEmpleado
                                            join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios
                                            WHERE '.$criterio.' = "'.$busqueda.'"');
                    $query->execute();
                    $_SESSION['conteo'] = $query->rowCount();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            case 2:
                try {
                    $query = $cnn->prepare('Select empleados.*, personas.*, roles.*, rolesusuarios.* from personas
                                            join empleados on personas.CedulaPersona = empleados.CedulaEmpleado
                                            join rolesusuarios on rolesusuarios.CedulaRolesUsuarios=empleados.CedulaEmpleado
                                            join roles on roles.IdRol=rolesusuarios.IdRolRolesUsuarios
                                            WHERE ' . $criterio . ' like "%' . $busqueda . '%"');
                    $query->execute();
                    $_SESSION['conteo'] = $query->rowCount();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function cambiarEstado($user, $estado, PDO $cnn)
    {

        try {
            $query2 = $cnn->prepare("Update personas set EstadoPersona=? where CedulaPersona=?");
            $query2->bindParam(1, $estado);
            $query2->bindParam(2, $user);
            $query2->execute();

            $this->mensaje="El empleado ahora se encuentra " . $estado;
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function cambiarClave($user, $pass, PDO $cnn)
    {
        try {
            $query2 = $cnn->prepare("Update personas set Contrasenia=? where CedulaPersona=?");
            $query2->bindParam(1, md5($pass));
            $query2->bindParam(2, $user);
            $query2->execute();
            $this->mensaje=true;
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function totalVentasEmpleados(PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
            try{
                $query=$cnn->prepare("select sum(cotizaciones.ValorTotalCotizacion) as vendido, empleados.CedulaEmpleado, concat(personas.Nombres,' ', personas.Apellidos) as nombres from cotizaciones JOIN
empleados on cotizaciones.CedulaEmpleadoCotizaciones=empleados.CedulaEmpleado join personas on personas.CedulaPersona=empleados.CedulaEmpleado
join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                               and pedidos.EstadoPedido='Autorizado'
and cotizaciones.EstadoCotizacion='Pedido' GROUP by empleados.CedulaEmpleado ORDER by vendido desc");
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }catch(Exception $ex){
                $this->mensaje=$ex->getMessage();
            }
        }elseif($_SESSION['datosLogin']['NombreRol']=='Asesor'){
            try{
                $query=$cnn->prepare("select sum(cotizaciones.ValorTotalCotizacion) as vendido, empleados.CedulaEmpleado, concat(personas.Nombres,' ', personas.Apellidos) as nombres from cotizaciones JOIN
empleados on cotizaciones.CedulaEmpleadoCotizaciones=empleados.CedulaEmpleado join personas on personas.CedulaPersona=empleados.CedulaEmpleado
join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                               and pedidos.EstadoPedido='Autorizado'
and cotizaciones.EstadoCotizacion='Pedido' AND empleados.CedulaEmpleado=? GROUP by empleados.CedulaEmpleado ORDER by vendido desc");
                $query->bindParam(1, $_SESSION['datosLogin']['id']);
                $query->execute();
                $this->mensaje=$query->fetchAll();
            }catch(Exception $ex){
                $this->mensaje=$ex->getMessage();
            }
        }
        $cnn = null;
        return $this->mensaje;
    }
    public function obtenerRoles($user, PDO $cnn)
    {
        try {

            $query = $cnn->prepare("select personas.CedulaPersona as 'id', personas.CedulaPersona as 'idempleado', personas.*, roles.*, rolesusuarios.*
                                    from personas join rolesusuarios ON personas.CedulaPersona=rolesusuarios.CedulaRolesUsuarios
                                    join roles on rolesusuarios.IdRolRolesUsuarios = roles.IdRol
                                    and personas.CedulaPersona=? ORDER BY roles.IdRol ASC");
            $query->bindParam(1, $user);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function validaUserPass($user, $pass, PDO $cnn)
    {
        try {
            $query=$cnn->prepare("select count(personas.CedulaPersona) as existe
from personas where personas.CedulaPersona=? and personas.Contrasenia='".md5($pass)."'");
            $query->bindParam(1, $user);
            $query->execute();
            $array=$query->fetch();
            //$this->mensaje=$query->fetch();
            $this->mensaje=$array['existe'];
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function validaUserMail($user, $mail, PDO $cnn)
    {
        try {
            $query=$cnn->prepare("select count(personas.CedulaPersona) as existe
from personas where personas.CedulaPersona=? and personas.EmailPersona=?");
            $query->bindParam(1, $user);
            $query->bindParam(2, $mail);
            $query->execute();
            $array=$query->fetch();
            $this->mensaje=$array['existe'];
        } catch (Exception $ex) {
            $this->mensaje='Error' . $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

}