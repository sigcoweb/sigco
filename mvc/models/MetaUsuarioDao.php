<?php

/**
 * Created by PhpStorm.
 * User: iStam
 * Date: 3/09/15
 * Time: 7:54 PM
 */
class MetaUsuarioDao
{

    public function asignarMeta(MetaUsuarioDto $dto, PDO $cnn)
    {
        $valor = $dto->getMeta();
        try {
            foreach ($dto->getEmpleado() as $meta) {
                $query = $cnn->prepare("INSERT INTO metasusuarios VALUES (?,?)");
                $query->bindParam(1, $meta);
                $query->bindParam(2, $valor);
                $query->execute();
            }
            return "Meta asignada exitosamente";
        } catch (Exception $ex) {
            $mensaje = '&detalleerror=' . $ex->getMessage() . '&error=1&mensaje=La meta NO se ha podido asignar';
        }
        $cnn = null;
        return $mensaje;
    }


    public function progreso($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare("select sum(cotizaciones.ValorTotalCotizacion) as vendido, empleados.CedulaEmpleado,
                                          concat(personas.Nombres,' ', personas.Apellidos) as nombres from cotizaciones
                                          JOIN empleados on cotizaciones.CedulaEmpleadoCotizaciones=empleados.CedulaEmpleado
                                          join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                          join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                                          and pedidos.EstadoPedido='Autorizado'
                                                          and cotizaciones.EstadoCotizacion='Pedido'
                                                          and FechaElaboracionPedido
                                                          between  ? AND ?
                                        GROUP by empleados.CedulaEmpleado ORDER by vendido desc");
                    $query->execute();
                    $_SESSION['conteo'] = $query->rowCount();
                    return $query->fetchAll();
                } catch (Exception $ex) {
                    echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;

            case 2:
                try {
                    $query = $cnn->prepare("Select * from metas
                                            where $criterio like '%$busqueda%' ");
                    $query->execute();
                    $_SESSION['conteo'] = $query->rowCount();
                    return $query->fetchAll();
                } catch (Exception $ex) {
                    echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                };

                break;
        }

    }


    public function buscarMeta($user, PDO $cnn)
    {
        try {
            $query = $cnn->prepare("Select * from metas where IdMeta=?");
            $query->bindParam(1, $user);
            $query->execute();
            $_SESSION['conteo'] = $query->rowCount();
            return $query->fetch();
        } catch (Exception $ex) {
            return $mensaje = $ex->getMessage();
        }
    }

    public function listarMeta(PDO $cnn)
    {
        try {
            $query = $cnn->prepare("Select * from metas ORDER BY datediff(metas.FechaFin,metas.FechaInicio) ASC ");
            $query->execute();
            $_SESSION['conteo'] = $query->rowCount();
            return $query->fetchAll();
        } catch (Exception $ex) {
            return $mensaje = $ex->getMessage();
        }
    }

    public function goalVisitProgress($startDate,$endDate, PDO $cnn)
    {


                if ($_SESSION['datosLogin']['NombreRol'] == 'Administrador' || $_SESSION['datosLogin']['NombreRol'] == 'Coordinador') {
                    try {
                        $query = $cnn->prepare('select count(gestiones.EstadoGestiones) as vendido, empleados.CedulaEmpleado,
                                                concat(personas.Nombres," ", personas.Apellidos) as nombres from gestiones
                                                JOIN empleados on gestiones.CedulaEmpleadoGestiones=empleados.CedulaEmpleado
                                                join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                                and gestiones.EstadoGestiones="Realizada"
                                                and gestiones.FechaProgramada
                                                between  ? AND ?
                                                GROUP by empleados.CedulaEmpleado ORDER by vendido desc');
                        $query->bindParam(1,$startDate);
                        $query->bindParam(2,$endDate);
                        $query->execute();
                        $this->mensaje = $query->fetch();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                } else {
                    try {
                        $query = $cnn->prepare('select count(gestiones.EstadoGestiones) as vendido, empleados.CedulaEmpleado,
                                                concat(personas.Nombres," ", personas.Apellidos) as nombres from gestiones
                                                JOIN empleados on gestiones.CedulaEmpleadoGestiones=empleados.CedulaEmpleado
                                                join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                                and gestiones.EstadoGestiones="Realizada"
                                                and gestiones.FechaProgramada
                                                between  ? AND ?
                                                AND empleados.CedulaEmpleado= ?
                                                GROUP by empleados.CedulaEmpleado ORDER by vendido desc');
                        $query->bindParam(1,$startDate);
                        $query->bindParam(2,$endDate);
                        $query->bindParam(3,$_SESSION['datosLogin']['id']);
                        $query->execute();
                        $this->mensaje = $query->fetch();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                }
                $cnn = null;
                return $this->mensaje;
    }

    public function goalProgress($startDate,$endDate, PDO $cnn)
    {


        if ($_SESSION['datosLogin']['NombreRol'] == 'Administrador' || $_SESSION['datosLogin']['NombreRol'] == 'Coordinador') {
            try {
                $query = $cnn->prepare('select sum(cotizaciones.ValorTotalCotizacion) as vendido, empleados.CedulaEmpleado,
                                          concat(personas.Nombres," ", personas.Apellidos) as nombres from cotizaciones
                                          JOIN empleados on cotizaciones.CedulaEmpleadoCotizaciones=empleados.CedulaEmpleado
                                          join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                          join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                          and pedidos.EstadoPedido="Autorizado"
                                          and cotizaciones.EstadoCotizacion="Pedido"
                                          and FechaElaboracionPedido
                                          between  ? AND ?
                                          GROUP by empleados.CedulaEmpleado ORDER by vendido desc');
                $query->bindParam(1,$startDate);
                $query->bindParam(2,$endDate);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
            };
        } else {
            try {
                $query = $cnn->prepare('select sum(cotizaciones.ValorTotalCotizacion) as vendido, empleados.CedulaEmpleado,
                                          concat(personas.Nombres," ", personas.Apellidos) as nombres from cotizaciones
                                          JOIN empleados on cotizaciones.CedulaEmpleadoCotizaciones=empleados.CedulaEmpleado
                                          join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                          join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                           and pedidos.EstadoPedido="Autorizado"
                                           and cotizaciones.EstadoCotizacion="Pedido"
                                           and FechaElaboracionPedido
                                           between  ? AND ?
                                           AND empleados.CedulaEmpleado= ?
                                           GROUP by empleados.CedulaEmpleado ORDER by vendido desc');
                $query->bindParam(1,$startDate);
                $query->bindParam(2,$endDate);
                $query->bindParam(3,$_SESSION['datosLogin']['id']);
                $query->execute();
                $this->mensaje = $query->fetch();
            } catch (Exception $ex) {
                $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
            };
        }
        $cnn = null;
        return $this->mensaje;
    }


    public function buscarMetaCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                if ($_SESSION['datosLogin']['NombreRol'] == 'Administrador' || $_SESSION['datosLogin']['NombreRol'] == 'Coordinador') {
                    try {
                        $query = $cnn->prepare('SELECT personas.*,empleados.*,concat(personas.Nombres," ",personas.Apellidos) as fullname,
                                             metas.*
                                FROM personas JOIN empleados on empleados.CedulaEmpleado = personas.CedulaPersona
                                join  metasusuarios on empleados.CedulaEmpleado = metasusuarios.CedulaMetasUsuarios
                                JOIN  metas  ON metas.IdMeta = metasusuarios.IdMetaMetasUsuarios
                                AND ' . $criterio . ' = "' . $busqueda . '"
                                ORDER BY datediff(metas.FechaFin,metas.FechaInicio) ASC');
                        $query->execute();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                } else {
                    try {
                        $query = $cnn->prepare('SELECT personas.*,empleados.*,concat(personas.Nombres," ",personas.Apellidos) as fullname,
                                             metas.*
                                FROM personas JOIN empleados on empleados.CedulaEmpleado = personas.CedulaPersona
                                join  metasusuarios on empleados.CedulaEmpleado = metasusuarios.CedulaMetasUsuarios
                                JOIN  metas  ON metas.IdMeta = metasusuarios.IdMetaMetasUsuarios
                                AND ' . $criterio . ' = "' . $busqueda . '"
                                AND empleados.CedulaEmpleado = ' . $_SESSION['datosLogin']['id'] . '
                                ORDER BY datediff(metas.FechaFin,metas.FechaInicio) ASC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        //return $query->fetchAll();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                }
                break;
            case 2;
                if ($_SESSION['datosLogin']['NombreRol'] == 'Administrador' || $_SESSION['datosLogin']['NombreRol'] == 'Coordinador') {
                    try {
                        $query = $cnn->prepare('SELECT personas.*,empleados.*,concat(personas.Nombres," ",personas.Apellidos) as fullname,
                                             metas.*
                                FROM personas JOIN empleados on empleados.CedulaEmpleado = personas.CedulaPersona
                                join  metasusuarios on empleados.CedulaEmpleado = metasusuarios.CedulaMetasUsuarios
                                JOIN  metas  ON metas.IdMeta = metasusuarios.IdMetaMetasUsuarios
                                AND ' . $criterio . ' like "%' . $busqueda . '%"
                                ORDER BY datediff(metas.FechaFin,metas.FechaInicio) ASC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    };
                } else {
                    try {
                        $query = $cnn->prepare('SELECT personas.*,empleados.*,concat(personas.Nombres," ",personas.Apellidos) as fullname,
                                             metas.*
                                FROM personas JOIN empleados on empleados.CedulaEmpleado = personas.CedulaPersona
                                join  metasusuarios on empleados.CedulaEmpleado = metasusuarios.CedulaMetasUsuarios
                                JOIN  metas  ON metas.IdMeta = metasusuarios.IdMetaMetasUsuarios
                                AND ' . $criterio . ' like "%' . $busqueda . '%"
                                AND clientes.CedulaEmpleadoClientes = ' . $_SESSION['datosLogin']['id'] . '
                                ORDER BY datediff(metas.FechaFin,metas.FechaInicio) ASC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&encontrados=0';
                    }
                }
                break;
            default:
                $this->mensaje = 'Opción inválida para búscar';
        }
        $cnn = null;
        return $this->mensaje;
    }

 }
